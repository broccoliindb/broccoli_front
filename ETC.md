# 기타 정보정리

## 정적사이트 vs SSR
- 사용자가 요청을 하기전에 페이지를 보여줄수 있다면 정적사이트로 하는게 낫다.
- 사용자가 요청이 많거나 데이터의 변경이 자주 일어난다면 SSR이 낫다.

## 2가지 방식의 Pre-render
nextjs는 두가지 방식의 pre-render가 있는데 하나는 static-generation, 그리고 ssr이다.

- static generation: html을 빌드할 때 생성한다. 그리고 이렇게 pre-render된 html은 매 요청시마다 재사용된다.
- server-side-render: 매 요청때 html을 생성한다. 

next.js는 개발자로 하여금 두가지 방식의 pre-render를 매 페이지마다 선택할수 있게 해준다!!!

## Client-Side-Rendering
Link를 사용한 페이지 trasition은 전통적인 방식의 anchor태그를 이용한 것보다 훨씬 빠르다. 왜냐면 full refresh를 하지 않기 때문인데
테스트 해보려면 개발자도구에 html에 css스타일을 background: yellow로 해보면 알수있다 full refresh를 하게되면 노란색배경이 유지되지 않지만 Link를 사용하면 노란색 배경이 유지된다. 

외부페이지를 방문할때는 anchor태그를 이용하면 된다.

## Code Splitting and prefetching

넥스트는 자동으로 코드스필릿팅을 하는데 그래서 매 페이지는 필요로하는 것만 로드한다. 즉 루트페이지가 렌더된다고 할때 다른 페이지들은 초기에 렌더되지 않는다. 따라서 수백개의 페이지가 있다고 할때 렌더가 필요한 페이지만 빨리 로드될 수 있다.

요청한 페이지만 로드된다는 것은 그 페이지만 독립적이라는 의미이고 만약 어떤 특정 페이지가 에러가 난다고 해서 다른 app은 여전히 동작함을 의미한다.

그리고 운영빌드상태에서의 넥스트는 Link가 viewport에 있을 때마다 해당 링크된 페이지의 코드를 자동적으로 백그라운드로 prefetching 해온다. 따라서 사용자가 Link를 클릭할 때 목적하는 페이지를 위한 코드는 백그라운드로 이미 로드되고 페이지 transition은 곧 이루어진다.

## Auto Optimization
넥스트는 app을 위해 자동적으로 코드스플리팅, 클라이언트사이트 navigation, prefeching(이건 운영환경에서만)을 동작한다.
파일시스템기반의 routing을 지원하기 때문에 routing 라이브러리가 필요가 없다.  

## Assets, Metadata, and css

넥스트는 에셋파일들을 top 레벨 경로에 public 아래에 보관한다. 이 경로에 있는 assets들은 pages들처럼 브라우저에서 파일시스템적인 경로로 접근가능한다.

이 아래에는 robots.txt(웹크롤링제한가능) 파일도 사용된다. 

## Image Component and Image Optimization

### 전통적인 방식

전통적인 방식으로 아래와 같이 사용할 경우 여러가지 수동적으로 신경써줘야하는 것들이 있다.

```
<img src="/images/profile.jpg" alt="Your Name" />
```
- 이미지가 다른 사이즈의 디바이스에서 responsive하게 동작하도록 하는것
- 3rd library를 통해 이미지 최적화하기
- 이미지는 viewport에 있을때만 로드된다. 등등

하지만 넥스트는 Image라는 컴포넌트를 제공하는 데 이문제들을 대신 다뤄준다.

Image 컴포넌트는 img태그를 확장 진화시킨 것이다.

이것은 이미지 최적화를 자동으로 지원하여 리사이징, 최적화, WebP 지원(애니메이션지원, 용량 30퍼센트 더 작음 png보다)를 해준다.

따라서 작은 디바이스에 불필요한 큰이미지로드를 피하고 브라우저에 더 적합한 이미지포맷을 제공한다.

## Using the Image Component
빌드시에 이미지 최적화를 진행하지 않는다. 따라서 이미지가 많던 적던 정적사이트생성기처럼 빌드시간을 증가시키지 않는다. 

- 사용자의 요구에 따라 이미지 최적화진행한다.
- 이미지는 디폴트로 lazy load된다. 따라서 viewport에 들어오면 로드된다.

Image Component는 구글이 서치랭킹에 사용하는 Core Web Vitals 이나 CLS를 피하는 방식으로 항상 렌더된다.

ℹ️ CLS: Cumulative Layout Shift
경고없이 문자가 이동하고 터치하거나 클릭하려고 할때 엉뚱한 링크나 버튼이 클릭되는 현상 그리고 엉뚱한 장소로 이동되는.....그지같은 상황

ℹ️ Core Web Vitals
어떤 웹의 모든 페이지들을 사용자가 사용할 때 얼마나 편한가 혹은 안편한가? 를 지표로 나타내는 방식으로서 지표들은 시간이 지남에 따라 변경가능하며 2020년에는 loading, interativity, visual stability에 중점을 두고 있다. 각 지표는 시간이 짧을 수록 좋다. 
- LCP: Largest Contentful Paint: 2.5초 이내
- FID: First Input Delay: 0.1ms 이내
- CLS: Cumulative Layout Shift: 0.1보다 낮을수록 좋다.

## Metadata

### Head
from 'next/head'

<head> 대신 사용할 수 있다. 타이틀변경가능
이것은 모든페이지에 common하게 사용되는 head를 사용할때 유용

## pages/_app.js
페이지 초기화를 제어할수 있다.
- 레이아웃 유지
- navigating 페이지의 상태값 유지
- Custom error handling using componentDidCatch
- Inject additional data into pages
- Add global CSS

```
// import App from 'next/app'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async (appContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);
//
//   return { ...appProps }
// }

export default MyApp
```

여기서 Component는 현재 액티브한 페이지를 의미한다. 따라서 어떤 프롭스를 보낼때 해당 액티브 페이지는 프롭스를 받을 수 있다. 

## pages/_document.js

- Document를 오버라이드한 것이다.
- 이것은 datafetching 을 할수없다. - 이벤트핸들러동작안된다.
- Document는 서버에서만 렌더된다. 
- Main은 브라우저에 의해서 초기화 안된다.
- 여기에는 어떤 app logic이나 custom-css(styled-jsx같은)가 추가되선 안된다.
- 만약 모든 페이지에서 공통으로 사용될 컴포넌트가 필요하다면 _app.js를 활용해라.

```
import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
```

getInitialProps or render 함수는 변경할게없다면 제거해된다. 

하지만

<Html>, <Head />, <Main /> and <NextScript /> 는 필수적이다. 

위에서 언급한 4개는 next/document라는 곳에서 import한것이다. 

## render를 커스텀할때(?)

It should be noted that the only reason you should be customizing renderPage is for usage with css-in-js libraries that need to wrap the application to properly work with server-side rendering.

It takes as argument an options object for further customization:

```
import Document from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const originalRenderPage = ctx.renderPage

    ctx.renderPage = () =>
      originalRenderPage({
        // useful for wrapping the whole react tree
        enhanceApp: (App) => App,
        // useful for wrapping in a per-page basis
        enhanceComponent: (Component) => Component,
      })

    // Run the parent `getInitialProps`, it now includes the custom `renderPage`
    const initialProps = await Document.getInitialProps(ctx)

    return initialProps
  }
}

export default MyDocument
```

## Writing and Importing css
.css, .scss를 임포트할 수 있다.

## Layout Component
Layout을 표현하는 컴포넌트로서 내부의 컴포넌트들을 감싼 부분을 wrapping한다.

## Pre-rendering and Data Fetching
넥스트 js는 디폴트로 매 페이지를 pre render한다. 이말은 넥스트는 미리 매 페이지의 html을 생성한다는 의미이다. 클라이언트사이드 자바스크립트의 의해 되는 것 대신!!!

pre-rendering은 퍼포먼스와 SEO에서 더 좋은 결과를 불러온다.

ℹ️ SEO: Search engine optimization
검색엔진에서 나의 사이트나 페이지가 자주언급되고 결과로 나오는것.

매 생성된 html은 최소한의 필수적인 자바스크립트 코드와 함께 관련되는데 브라우저가 페이지를 로드할때 그 자바스크립트 코드는 실행되고 페이지의 모든 상호작용을 만들어낸다.
이런 과정을 hydration이라고 한다.

dehydration: 동적 -> 정적
hydration: 정적 -> 동적

pre-rendering에는 두가지 과정이 있다.
1. initial load
2. hydration

initial load는 js 동작이 없는 html을 먼저 화면에 보여주고 js가 로드되지 않으므로 interactive한 컴포넌트가 동작하지 않는다.

hydration 은 최소한의 필요한 js파일을 서버로부터 받아와 html과 연결과하는 과정이다.