import { useState, useRef, useEffect } from 'react'

const useIntersect = (
  {
    root = null,
    rootMargin,
    threshold = 0,
    updatedAt,
    lastId,
    likeCount,
    durationType,
    hashtag
  },
  handler
) => {
  const [node, setNode] = useState(null)
  const observer = useRef(null)
  useEffect(() => {
    if (observer.current) observer.current.disconnect()

    observer.current = new IntersectionObserver(
      ([entryInfo]) => {
        if (entryInfo.isIntersecting) {
          handler(entryInfo)
        }
      },
      { root, rootMargin, threshold }
    )
    const { current: currentObserver } = observer
    if (node) {
      currentObserver.observe(node)
    }
    return () => currentObserver.disconnect()
  }, [node, updatedAt, lastId, likeCount, durationType, hashtag])

  return [setNode]
}

export default useIntersect
