import { all, call, put, takeLatest, fork } from 'redux-saga/effects'
import axios from 'axios'
import { TYPES } from '../reducers/rootReducer'

const searchTermApi = (data) => {
  const { q, username, userId } = data
  return axios.post('/search', { q, username, userId })
}
const getSearchTermTokensApi = (data) => {
  return axios.get(`/search/terms?term=${encodeURIComponent(data)}`)
}
/**
REQUESTS
*/
function* searchTermRequest(action) {
  try {
    const searchResult = yield call(searchTermApi, action.data)
    yield put({
      type: TYPES.SEARCH_TERM_FULFILLED,
      payload: searchResult.data
    })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.SEARCH_TERM_REJECTED, error })
    }
  }
}
function* getSearchTermTokensRequest(action) {
  try {
    const searchResult = yield call(getSearchTermTokensApi, action.data)
    yield put({
      type: TYPES.GET_SEARCH_TERM_TOKENS_FULFILLED,
      payload: searchResult.data
    })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.GET_SEARCH_TERM_TOKENS_REJECTED, error })
    }
  }
}

/**
WATCHERS
*/
function* watchSearchTerm() {
  yield takeLatest(TYPES.SEARCH_TERM_REQUEST, searchTermRequest)
}
function* watchGetSearchTermTokens() {
  yield takeLatest(
    TYPES.GET_SEARCH_TERM_TOKENS_REQUEST,
    getSearchTermTokensRequest
  )
}
export default function* searchSagas() {
  yield all([fork(watchSearchTerm), fork(watchGetSearchTermTokens)])
}
