import { all, call, put, takeLatest, fork } from 'redux-saga/effects'
import axios from 'axios'
import { TYPES } from '../reducers/rootReducer'

/**
APIS
*/

const addSeriesApi = (data) => {
  return axios.post('/series', data)
}
const getSeriesApi = () => {
  return axios.get('/series')
}
/**
REQUESTS
*/

function* addSeriesRequest(action) {
  try {
    const series = yield call(addSeriesApi, action.data)
    yield put({ type: TYPES.ADD_SERIES_FULFILLED, payload: series.data })
  } catch (error) {
    if (error) {
      console.error('🥊🥊🥊', error)
      yield put({ type: TYPES.ADD_SERIES_REJECTED, error })
    }
  }
}
function* getSeriesRequest() {
  try {
    const series = yield call(getSeriesApi)
    yield put({ type: TYPES.GET_SERIES_FULFILLED, payload: series.data })
  } catch (error) {
    if (error) {
      console.error('🥊🥊🥊', error)
      yield put({ type: TYPES.GET_SERIES_REJECTED, error })
    }
  }
}

/**
WATCHERS
*/
function* watchAddSeries() {
  yield takeLatest(TYPES.ADD_SERIES_REQUEST, addSeriesRequest)
}
function* watchGetSeries() {
  yield takeLatest(TYPES.GET_SERIES_REQUEST, getSeriesRequest)
}

export default function* seriesSagas() {
  yield all([fork(watchAddSeries), fork(watchGetSeries)])
}
