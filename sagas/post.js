import { all, call, put, takeLatest, fork } from 'redux-saga/effects'
import axios from 'axios'
import { TYPES } from '../reducers/rootReducer'

/**
APIS
*/

const addPostApi = (data) => {
  return axios.post('/post', data)
}
const getPostsApi = ({ nickname, loadType, userId, updatedAt = 0 }) => {
  if (!nickname) {
    return axios.get(`/posts/${loadType}?updatedAt=${updatedAt}`)
  }
  if (userId) {
    return axios.get(
      `/posts/${loadType}?updatedAt=${updatedAt}&authorId=${userId}&nickname=${encodeURIComponent(
        nickname
      )}`
    )
  }
  return axios.get(
    `/posts/${loadType}?updatedAt=${updatedAt}&nickname=${encodeURIComponent(
      nickname
    )}`
  )
}
const getPostApi = (data) => {
  return axios
    .get(`/post/${data.postKey}`)
    .then((response) => response)
    .catch((error) => error)
}
const getTempPostApi = (data) => {
  return axios
    .get(`/post/temp/${data.postKey}`)
    .then((response) => response)
    .catch((error) => error)
}
const tempSavePostApi = (data) => {
  return axios.post('/post/temp', data)
}
const removePostApi = (data) => {
  return axios
    .delete(`/post/${data}`)
    .then((response) => response)
    .catch((error) => error)
}
const getAllPostsApi = (data) => {
  const { orderBy, durationType, lastId, likeCount } = data
  const orderby = encodeURIComponent(orderBy)
  const durationtype = encodeURIComponent(durationType)
  return axios.get(
    `/posts?orderBy=${orderby}&durationType=${durationtype}&lastId=${lastId}&likeCount=${likeCount}`
  )
}
const likePostApi = (data) => {
  return axios.patch(`/post/${data}/like`)
}
const unlikePostApi = (data) => {
  return axios.patch(`/post/${data}/unlike`)
}
const getAllSeriesOnPostsApi = (data) => {
  const { nickname, updatedAt } = data
  return axios.get(
    `/posts/series?nickname=${encodeURIComponent(nickname)}&updatedAt=${
      updatedAt || 0
    }`
  )
}
const getAllPostsOnSeriesApi = (data) => {
  const { seriesName, seriesId, updatedAt } = data
  return axios.get(
    `/posts/series/${encodeURIComponent(
      seriesName
    )}?seriesId=${seriesId}&updatedAt=${updatedAt || 0}`
  )
}
const getPostMetaListOnSeriesApi = (data) => {
  return axios.get(`/posts/series?postKey=${data}`)
}
const getAllPostsOnHashApi = (data) => {
  const { lastId, hashtag } = data
  return axios.get(
    `/posts/hashtags/${encodeURIComponent(hashtag)}?lastId=${lastId}`
  )
}
/**
REQUESTS
*/

function* addPostRequest(action) {
  try {
    const post = yield call(addPostApi, action.data)
    yield put({ type: TYPES.ADD_POST_FULFILLED, payload: post.data })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.ADD_POST_REJECTED, error })
    }
  }
}
function* getPostsRequest(action) {
  try {
    const posts = yield call(getPostsApi, action.data)
    yield put({
      type: TYPES.GET_POSTS_FULFILLED,
      payload: posts.data
    })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.GET_POSTS_REJECTED, error })
    }
  }
}
function* getPostRequest(action) {
  const { response, data: successData } = yield call(getPostApi, action.data)
  if (successData) {
    yield put({ type: TYPES.GET_POST_FULFILLED, payload: successData })
  }
  if (response) {
    const { data } = response
    yield put({ type: TYPES.GET_POST_REJECTED, error: data })
  }
}
function* getTempPostRequest(action) {
  const { response, data: successData } = yield call(
    getTempPostApi,
    action.data
  )
  if (successData) {
    yield put({ type: TYPES.GET_TEMP_POST_FULFILLED, payload: successData })
  }
  if (response) {
    const { data } = response
    yield put({ type: TYPES.GET_TEMP_POST_REJECTED, error: data })
  }
}
function* tempSavePostRequest(action) {
  try {
    const post = yield call(tempSavePostApi, action.data)
    yield put({ type: TYPES.TEMP_SAVE_POST_FULFILLED, payload: post.data })
  } catch (error) {
    if (error) {
      yield put({ type: TYPES.TEMP_SAVE_POST_REJECTED, error })
    }
  }
}
function* removePostRequest(action) {
  const { response, data: successData } = yield call(removePostApi, action.data)
  if (successData) {
    yield put({ type: TYPES.REMOVE_POST_FULFILLED, payload: successData })
  }
  if (response) {
    const { data } = response
    yield put({ type: TYPES.REMOVE_POST_REJECTED, error: data })
  }
}
function* getAllPostsRequest(action) {
  try {
    const posts = yield call(getAllPostsApi, action.data)
    yield put({ type: TYPES.GET_ALL_POSTS_FULFILLED, payload: posts.data })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.GET_ALL_POSTS_REJECTED, error })
    }
  }
}
function* likePostRequest(action) {
  try {
    const postInfo = yield call(likePostApi, action.data)
    yield put({ type: TYPES.LIKE_POST_FULFILLED, payload: postInfo.data })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.LIKE_POST_REJECTED, error })
    }
  }
}
function* unlikePostRequest(action) {
  try {
    const postInfo = yield call(unlikePostApi, action.data)
    yield put({ type: TYPES.UNLIKE_POST_FULFILLED, payload: postInfo.data })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.UNLIKE_POST_REJECTED, error })
    }
  }
}
function* getAllSeriesOnPostsRequest(action) {
  try {
    const posts = yield call(getAllSeriesOnPostsApi, action.data)
    yield put({
      type: TYPES.GET_ALL_SERIES_ON_POSTS_FULFILLED,
      payload: posts.data
    })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.GET_ALL_SERIES_ON_POSTS_REJECTED, error })
    }
  }
}
function* getAllPostsOnSeriesRequest(action) {
  try {
    const posts = yield call(getAllPostsOnSeriesApi, action.data)
    yield put({
      type: TYPES.GET_ALL_POSTS_ON_SERIES_FULFILLED,
      payload: posts.data
    })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.GET_ALL_POSTS_ON_SERIES_REJECTED, error })
    }
  }
}
function* getPostMetaListOnSeriesRequest(action) {
  try {
    const posts = yield call(getPostMetaListOnSeriesApi, action.data)
    yield put({
      type: TYPES.GET_POST_META_LIST_ON_SERIES_FULFILLED,
      payload: posts.data
    })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.GET_POST_META_LIST_ON_SERIES_REJECTED, error })
    }
  }
}
function* getAllPostsOnHashRequest(action) {
  try {
    const posts = yield call(getAllPostsOnHashApi, action.data)
    yield put({
      type: TYPES.GET_ALL_POSTS_ON_HASH_FULFILLED,
      payload: posts.data
    })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.GET_ALL_POSTS_ON_HASH_REJECTED, error })
    }
  }
}

/**
WATCHERS
*/

function* watchAddPost() {
  yield takeLatest(TYPES.ADD_POST_REQUEST, addPostRequest)
}
function* watchGetPosts() {
  yield takeLatest(TYPES.GET_POSTS_REQUEST, getPostsRequest)
}
function* watchGetPost() {
  yield takeLatest(TYPES.GET_POST_REQUEST, getPostRequest)
}
function* watchGetTempPost() {
  yield takeLatest(TYPES.GET_TEMP_POST_REQUEST, getTempPostRequest)
}
function* watchTempSavePost() {
  yield takeLatest(TYPES.TEMP_SAVE_POST_REQUEST, tempSavePostRequest)
}
function* watchRemovePost() {
  yield takeLatest(TYPES.REMOVE_POST_REQUEST, removePostRequest)
}
function* watchGetAllPosts() {
  yield takeLatest(TYPES.GET_ALL_POSTS_REQUEST, getAllPostsRequest)
}
function* watchLikePost() {
  yield takeLatest(TYPES.LIKE_POST_REQUREST, likePostRequest)
}
function* watchUnlikePost() {
  yield takeLatest(TYPES.UNLIKE_POST_REQUREST, unlikePostRequest)
}
function* watchGetAllSeriesOnPosts() {
  yield takeLatest(
    TYPES.GET_ALL_SERIES_ON_POSTS_REQUEST,
    getAllSeriesOnPostsRequest
  )
}
function* watchGetAllPostsOnSeries() {
  yield takeLatest(
    TYPES.GET_ALL_POSTS_ON_SERIES_REQUEST,
    getAllPostsOnSeriesRequest
  )
}
function* watchGetPostMetaListOnSeries() {
  yield takeLatest(
    TYPES.GET_POST_META_LIST_ON_SERIES_REQUEST,
    getPostMetaListOnSeriesRequest
  )
}
function* watchGetAllPostsOnHash() {
  yield takeLatest(
    TYPES.GET_ALL_POSTS_ON_HASH_REQUEST,
    getAllPostsOnHashRequest
  )
}

export default function* postSagas() {
  yield all([
    fork(watchAddPost),
    fork(watchGetPosts),
    fork(watchGetPost),
    fork(watchGetTempPost),
    fork(watchTempSavePost),
    fork(watchRemovePost),
    fork(watchGetAllPosts),
    fork(watchLikePost),
    fork(watchUnlikePost),
    fork(watchGetAllSeriesOnPosts),
    fork(watchGetAllPostsOnSeries),
    fork(watchGetPostMetaListOnSeries),
    fork(watchGetAllPostsOnHash)
  ])
}
