import { all, call, put, takeLatest, fork } from 'redux-saga/effects'
import axios from 'axios'
import { TYPES } from '../reducers/rootReducer'

/**
APIS
*/
const addCommentApi = (data) => {
  return axios.post('/comment', data)
}
const getCommentsApi = (data) => {
  return axios.get(`/comments/${data}`)
}
const removeCommentApi = (data) => {
  const { id, childLength } = data
  return axios.delete(`/comment/${id}?childLength=${childLength}`)
}
const updateCommentApi = (data) => {
  const { content, id } = data
  return axios.patch(`/comment/${id}`, { content })
}
/**
REQUESTS
*/
function* addCommentRequest(action) {
  try {
    const comment = yield call(addCommentApi, action.data)
    yield put({
      type: TYPES.ADD_COMMENT_FULFILLED,
      payload: comment.data
    })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.ADD_COMMENT_REJECTED, error })
    }
  }
}
function* getCommentsRequest(action) {
  try {
    const comments = yield call(getCommentsApi, action.data.postKey)
    yield put({ type: TYPES.GET_COMMENTS_FULFILLED, payload: comments.data })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.GET_COMMENTS_REJECTED, error })
    }
  }
}
function* removeCommentRequest(action) {
  try {
    const comment = yield call(removeCommentApi, action.data)
    yield put({
      type: TYPES.REMOVE_COMMENT_FULFILLED,
      payload: comment.data,
      input: action.data
    })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.REMOVE_COMMENT_REJECTED, error })
    }
  }
}
function* updateCommentRequest(action) {
  try {
    const comment = yield call(updateCommentApi, action.data)
    yield put({
      type: TYPES.UPDATE_COMMENT_FULFILLED,
      payload: comment.data
    })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.UPDATE_COMMENT_REJECTED, error })
    }
  }
}
/**
WATCHERS
*/
function* watchAddCommentRequest() {
  yield takeLatest(TYPES.ADD_COMMENT_REQUEST, addCommentRequest)
}
function* watchGetCommentsRequest() {
  yield takeLatest(TYPES.GET_COMMENTS_REQUEST, getCommentsRequest)
}
function* watchRemoveCommentRequest() {
  yield takeLatest(TYPES.REMOVE_COMMENT_REQUEST, removeCommentRequest)
}
function* watchUpdateCommentRequest() {
  yield takeLatest(TYPES.UPDATE_COMMENT_REQUEST, updateCommentRequest)
}

export default function* commentSagas() {
  yield all([fork(watchAddCommentRequest)])
  yield all([fork(watchGetCommentsRequest)])
  yield all([fork(watchRemoveCommentRequest)])
  yield all([fork(watchUpdateCommentRequest)])
}
