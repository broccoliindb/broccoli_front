import { all, fork } from 'redux-saga/effects'
import axios from 'axios'
import postSagas from './post'
import userSagas from './user'
import seriesSagas from './series'
import searchSagas from './search'
import commentSagas from './comment'
import hashtagSagas from './hastag'

axios.defaults.baseURL = `${process.env.API_URL}`
axios.defaults.withCredentials = true

export default function* rootSaga() {
  yield all([
    fork(postSagas),
    fork(userSagas),
    fork(seriesSagas),
    fork(searchSagas),
    fork(commentSagas),
    fork(hashtagSagas)
  ])
}
