import { all, call, put, takeLatest, fork } from 'redux-saga/effects'
import axios from 'axios'
import { TYPES } from '../reducers/rootReducer'

/**
APIS
*/

const loginApi = (data) => {
  return axios
    .post('/auth/login', data)
    .then((response) => response)
    .catch((error) => error)
}
const logoutApi = () => {
  return axios.get('/auth/logout')
}
const loadMeInfoApi = () => {
  return axios.get('/user')
}
const signupApi = (data) => {
  return axios
    .post('/auth/signup', data)
    .then((response) => response)
    .catch((error) => error)
}
const updateMeInfoApi = (data) => {
  const { user, password, whoami, nickname, blogname, avatar } = data
  if (!user) {
    return Promise.reject(new Error('사용자 정보가 없습니다.'))
  }
  if (
    !password &&
    !whoami &&
    !nickname &&
    !blogname &&
    avatar === 'undefined'
  ) {
    return Promise.reject(new Error('변경할 정보가 없습니다.'))
  }
  return axios
    .patch(`/user/${data.user}/update`, data)
    .then((response) => response)
    .catch((error) => error)
}
const removeAccountApi = (data) => {
  return axios
    .delete(`/user/${data.user}`)
    .then((response) => response)
    .catch((error) => error)
}
const resetPasswordApi = (data) => {
  return axios
    .get(`/user/email?email=${data}`)
    .then((response) => response)
    .catch((error) => error)
  //이메일로 유저확인
  //유저있으면 메일보내기
}
const checkTokenApi = (data) => {
  return axios
    .get(`/auth/check?token=${data}`)
    .then((response) => response)
    .catch((error) => error)
}
const getUserInfoApi = (data) => {
  const { userId, nickname } = data
  if (userId) {
    return axios.get(`/user/userInfo?userId=${userId}`)
  }
  return axios.get(`/user/userInfo?nickname=${encodeURIComponent(nickname)}`)
}
/**
REQUESTS
*/
function* loginRequest(action) {
  const { response, data: successData } = yield call(loginApi, action.data)
  if (successData) {
    yield put({ type: TYPES.LOGIN_FULFILLED, payload: successData })
  }
  if (response) {
    const { data } = response
    yield put({ type: TYPES.LOGIN_REJECTED, error: data })
  }
}
function* logoutRequest(action) {
  try {
    yield call(logoutApi, action.data)
    yield put({ type: TYPES.LOGOUT_FULFILLED })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.LOGOUT_REJECTED, error })
    }
  }
}
function* loadMeInfoRequest() {
  try {
    const user = yield call(loadMeInfoApi)
    yield put({ type: TYPES.LOAD_ME_INFO_FULFILLED, payload: user.data })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.LOAD_ME_INFO_REJECTED, error })
    }
  }
}
function* signupRequest(action) {
  const { response, data: successData } = yield call(signupApi, action.data)
  if (successData) {
    yield put({ type: TYPES.SIGNUP_FULFILLED, payload: successData })
  }
  if (response) {
    const { data } = response
    yield put({ type: TYPES.SIGNUP_REJECTED, error: data })
  }
}
function* updateMeInfoRequest(action) {
  try {
    const { response, data: successData } = yield call(
      updateMeInfoApi,
      action.data
    )
    if (successData) {
      yield put({
        type: TYPES.UPDATE_ME_INFO_FULFILLED,
        payload: successData
      })
    }
    if (response) {
      const { data } = response
      yield put({ type: TYPES.UPDATE_ME_INFO_REJECTED, error: data })
    }
  } catch (err) {
    yield put({ type: TYPES.UPDATE_ME_INFO_REJECTED, error: err })
  }
}
function* removeAccountRequest(action) {
  const { response, data: successData } = yield call(
    removeAccountApi,
    action.data
  )
  if (successData) {
    yield put({ type: TYPES.REMOVE_ACCOUNT_FULFILLED, payload: successData })
  }
  if (response) {
    const { data } = response
    yield put({ type: TYPES.REMOVE_ACCOUNT_REJECTED, error: data })
  }
}
function* resetPasswordRequest(action) {
  const { response, data: successData } = yield call(
    resetPasswordApi,
    action.data
  )
  if (successData) {
    yield put({ type: TYPES.RESET_PASSWORD_FULFILLED, payload: successData })
  }
  if (response) {
    const { data } = response
    yield put({ type: TYPES.RESET_PASSWORD_REJECTED, error: data })
  }
}
function* checkTokenRequest(action) {
  const { response, data: successData } = yield call(checkTokenApi, action.data)
  if (successData) {
    yield put({ type: TYPES.CHECK_TOKEN_FULFILLED, payload: successData })
  }
  if (response) {
    const { data } = response
    yield put({ type: TYPES.CHECK_TOKEN_REJECTED, error: data })
  }
}
function* getUserInfoRequest(action) {
  try {
    const user = yield call(getUserInfoApi, action.data)
    yield put({ type: TYPES.GET_USER_INFO_FULFILLED, payload: user.data })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.GET_USER_INFO_REJECTED, error })
    }
  }
}

/**
WATCHERS
*/
function* watchLoginRequest() {
  yield takeLatest(TYPES.LOGIN_REQUEST, loginRequest)
}
function* watchLogoutRequest() {
  yield takeLatest(TYPES.LOGOUT_REQUEST, logoutRequest)
}
function* watchLoadMeInfoRequest() {
  yield takeLatest(TYPES.LOAD_ME_INFO_REQUEST, loadMeInfoRequest)
}
function* watchSignupRequest() {
  yield takeLatest(TYPES.SIGNUP_REQUEST, signupRequest)
}
function* watchUpdateMeInfoRequest() {
  yield takeLatest(TYPES.UPDATE_ME_INFO_REQUEST, updateMeInfoRequest)
}
function* watchRemoveAccountRequest() {
  yield takeLatest(TYPES.REMOVE_ACCOUNT_REQUEST, removeAccountRequest)
}
function* watchResetPasswordRequest() {
  yield takeLatest(TYPES.RESET_PASSWORD_REQUEST, resetPasswordRequest)
}
function* watchCheckTokenRequest() {
  yield takeLatest(TYPES.CHECK_TOKEN_REQUEST, checkTokenRequest)
}
function* watchGetUserInfoRequest() {
  yield takeLatest(TYPES.GET_USER_INFO_REQUEST, getUserInfoRequest)
}

export default function* userSagas() {
  yield all([
    fork(watchLoginRequest),
    fork(watchLogoutRequest),
    fork(watchLoadMeInfoRequest),
    fork(watchSignupRequest),
    fork(watchUpdateMeInfoRequest),
    fork(watchRemoveAccountRequest),
    fork(watchResetPasswordRequest),
    fork(watchCheckTokenRequest),
    fork(watchGetUserInfoRequest)
  ])
}
