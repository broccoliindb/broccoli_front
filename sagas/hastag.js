import { all, call, put, takeLatest, fork } from 'redux-saga/effects'
import axios from 'axios'
import { TYPES } from '../reducers/rootReducer'

/**
APIS
*/

const getHashtagsApi = (nickname) => {
  return axios.get(`/hashtags/${encodeURIComponent(nickname)}`)
}
/**
REQUESTS
*/

function* getHashtagsRequest(action) {
  try {
    const post = yield call(getHashtagsApi, action.data)
    yield put({ type: TYPES.GET_HASHTAGS_FULFILLED, payload: post.data })
  } catch (error) {
    if (error) {
      console.error(error)
      yield put({ type: TYPES.GET_HASHTAGS_REJECTED, error })
    }
  }
}

/**
WATCHERS
*/

function* watchGetHashtags() {
  yield takeLatest(TYPES.GET_HASHTAGS_REQUEST, getHashtagsRequest)
}

export default function* hashtagSagas() {
  yield all([fork(watchGetHashtags)])
}
