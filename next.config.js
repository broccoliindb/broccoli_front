const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true'
})

module.exports = withBundleAnalyzer({
  eslint: {
    ignoreDuringBuilds: true
  },
  compress: true,
  env: {
    SEARCH_URL:
      process.env.NODE_ENV === 'production'
        ? process.env.SEARCH_PROD
        : process.env.SEARCH_DEV,
    API_URL:
      process.env.NODE_ENV === 'production'
        ? process.env.API_PROD
        : process.env.API_DEV
  },
  images: {
    domains: [
      'avatars.githubusercontent.com',
      'localhost',
      'api.broccolidb.com',
      'k.kakaocdn.net',
      'lh3.googleusercontent.com',
      'broccolidb.com'
    ]
  },
  i18n: {
    locales: ['ko', 'en'],
    defaultLocale: 'ko'
  },
  webpack(config, { webpack }) {
    const prod = process.env.NODE_ENV === 'production'
    return {
      ...config,
      mode: prod ? 'production' : 'development',
      devtool: prod ? 'hidden-source-map' : 'eval',
      plugins: [...config.plugins]
    }
  }
})
