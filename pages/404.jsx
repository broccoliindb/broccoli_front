import React from 'react'
import styled from 'styled-components'
import Link from 'next/link'

const Container = styled.main`
  padding: 1rem;
  a {
    h2 {
      &:hover {
        color: var(--everGreenColor);
      }
    }
  }
`

const NotFound = () => {
  return (
    <Container>
      <h1>찾으시는 페이지는 존재하지 않습니다.😭😭</h1>
      <Link href="/">
        <a>
          <h2>🏠홈으로 돌아가기</h2>
        </a>
      </Link>
    </Container>
  )
}

export default NotFound
