import React from 'react'
import axios from 'axios'
import { END } from 'redux-saga'
import wrapper from '../store/configureStore'
import { TYPES } from '../reducers/rootReducer'
import Hash from '../components/Hash'

const HashPosts = () => {
  return <Hash />
}

export const getServerSideProps = wrapper.getServerSideProps(
  async (context) => {
    const { value, lastId } = context.query
    const cookie = context.req ? context.req.headers.cookie : ''
    axios.defaults.headers.cookie = ''
    if (context.req && cookie) {
      axios.defaults.headers.cookie = cookie
    }
    context.store.dispatch({
      type: TYPES.LOAD_ME_INFO_REQUEST
    })
    context.store.dispatch({
      type: TYPES.GET_ALL_POSTS_ON_HASH_REQUEST,
      data: { hashtag: value, lastId: lastId || 0 }
    })
    context.store.dispatch(END)

    await context.store.sagaTask.toPromise()
  }
)

export default HashPosts
