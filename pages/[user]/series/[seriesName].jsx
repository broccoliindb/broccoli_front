import React, { useCallback, useRef, useEffect, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import axios from 'axios'
import { END } from 'redux-saga'
import Link from 'next/link'
import useIntersectionObserver from '../../../hooks/useIntersectionObserver'
import Layout from '../../../components/Layout'
import wrapper from '../../../store/configureStore'
import { STATUS, TYPES } from '../../../reducers/rootReducer'
import PostSummary from '../../../components/PostSummary'
import Loader from '../../../components/controls/Loader'

const Container = styled.div`
  padding: 1rem;
  display: grid;
  @media only screen and (max-width: 600px) {
    padding: 1rem 0;
  }
`
const Contents = styled.ul``
const Content = styled.li`
  display: grid;
  grid-template-rows: repeat(auto-fit, auto);
  padding-top: 1rem;
  padding-bottom: 2rem;
`
const Observer = styled.div`
  width: 100;
  height: 2rem;
`
const SeriesTitle = styled.div`
  padding: 1rem;
`
const SeriesItems = () => {
  const dispatch = useDispatch()
  const loader = useRef(false)
  const observerRef = useRef()
  const {
    posts,
    status: { getAllPostsOnSeriesStatus }
  } = useSelector((state) => state.post)
  const { siteOwner } = useSelector((state) => state.user)
  const router = useRouter()
  const { seriesName, user } = router.query
  const { query } = router
  const seriesId = query?.seriesId
  const updatedAt = posts.length > 0 && posts[posts.length - 1].updatedAt
  const owner = useMemo(() => {
    if (siteOwner) return siteOwner
    return {
      nickname: '',
      avatar: '',
      defaultavatar: '',
      blogname: ''
    }
  }, [siteOwner])
  const handler = useCallback(() => {
    if (updatedAt) {
      dispatch({
        type: TYPES.GET_ALL_POSTS_ON_SERIES_REQUEST,
        data: {
          seriesName,
          seriesId,
          updatedAt
        }
      })
    }
  }, [posts])
  const [setNode] = useIntersectionObserver(
    {
      rootMargin: '0px',
      threshold: 0,
      updatedAt: updatedAt || 0
    },
    handler
  )
  const getPostParams = useCallback((post) => {
    return {
      title: post.title,
      updatedat: post.updatedAt,
      content: post.content,
      postkey: post.postKey,
      thumbnail: post.thumbnail,
      nickname: post.User.nickname,
      avatar: post.User.avatar || post.User.defaultavatar,
      likeCount: post.Liker?.length,
      commentCount: post.Comments?.length,
      hashtags: post.Hashtags
    }
  }, [])

  useEffect(() => {
    if (!observerRef.current) return
    setNode(observerRef.current)
  }, [])
  return (
    <Layout siteOwner={owner}>
      <>
        <SeriesTitle>
          <Link href={`/${user}/series`}>
            <a>
              <h4
                style={{
                  borderBottom: '4px solid var(--everGreenColor)',
                  width: 'max-content',
                  color: 'var(--everGreenColor)'
                }}
              >
                시리즈
              </h4>
            </a>
          </Link>
          <h2 style={{ margin: 0 }}>{seriesName}</h2>
        </SeriesTitle>
        <Container>
          <Contents>
            {posts.length === 0 && getAllPostsOnSeriesStatus === STATUS.DONE ? (
              <h2 style={{ margin: '0 1rem' }}>😢 아무 컨텐츠가 없습니다</h2>
            ) : (
              <>
                {posts.map((post) => (
                  <Content key={post.id}>
                    <PostSummary post={getPostParams(post)} isMyDocs />
                  </Content>
                ))}
              </>
            )}
          </Contents>
          <div style={{ height: '2rem' }}>
            <Loader isLoader={loader.current} />
          </div>
          <Observer ref={observerRef} />
        </Container>
      </>
    </Layout>
  )
}

export const getServerSideProps = wrapper.getServerSideProps(
  async (context) => {
    const { user, seriesName } = context.params
    const nickname = user.replace('@', '')
    const { seriesId } = context.query
    if (!user.startsWith('@')) {
      return {
        notFound: true
      }
    }
    const cookie = context.req ? context.req.headers.cookie : ''
    axios.defaults.headers.cookie = ''
    if (context.req && cookie) {
      axios.defaults.headers.cookie = cookie
    }
    context.store.dispatch({
      type: TYPES.LOAD_ME_INFO_REQUEST
    })
    context.store.dispatch({
      type: TYPES.GET_USER_INFO_REQUEST,
      data: { nickname }
    })
    context.store.dispatch({
      type: TYPES.GET_ALL_POSTS_ON_SERIES_REQUEST,
      data: {
        seriesName,
        seriesId
      }
    })
    context.store.dispatch(END)

    await context.store.sagaTask.toPromise()
  }
)

export default SeriesItems
