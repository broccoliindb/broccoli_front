import React, { useCallback, useEffect, useRef, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Link from 'next/link'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import axios from 'axios'
import { END } from 'redux-saga'
import Layout from '../../../components/Layout'
import wrapper from '../../../store/configureStore'
import { STATUS, TYPES } from '../../../reducers/rootReducer'
import BlogTabs from '../../../components/BlogTabs'
import useIntersectionObserver from '../../../hooks/useIntersectionObserver'
import SeriesSummary from '../../../components/SeriesSummary'

const Container = styled.div`
  padding: 1rem;
  @media only screen and (max-width: 600px) {
    padding: 1rem 0;
  }
`
const Contents = styled.ul`
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
`
const Content = styled.li`
  display: grid;
  grid-template-rows: repeat(auto-fit, auto);
  padding-top: 1rem;
  padding-bottom: 2rem;
`
const Observer = styled.div`
  width: 100;
  height: 2rem;
`
const Posts = () => {
  const dispatch = useDispatch()
  const observerRef = useRef()
  const {
    postsOnSeries: series,
    status: { getAllSeriesOnPostsStatus }
  } = useSelector((state) => state.post)
  const { siteOwner } = useSelector((state) => state.user)
  const router = useRouter()
  const { query } = router
  const { user } = query
  const updatedAt = series.length > 0 && series[series.length - 1].updatedAt
  const owner = useMemo(() => {
    if (siteOwner) return siteOwner
    return {
      nickname: '',
      avatar: '',
      defaultavatar: '',
      blogname: ''
    }
  }, [siteOwner])
  const handler = useCallback(() => {
    if (updatedAt) {
      dispatch({
        type: TYPES.GET_ALL_SERIES_ON_POSTS_REQUEST,
        data: {
          nickname: user.replace('@', ''),
          updatedAt
        }
      })
    }
  }, [series])
  const [setNode] = useIntersectionObserver(
    {
      rootMargin: '0px',
      threshold: 0,
      updatedAt: updatedAt || 0
    },
    handler
  )

  const getPostParams = useCallback((item) => {
    return {
      id: item.seriesId,
      title: item.seriesName,
      updatedat: item.updatedAt,
      thumbnail: item.thumbnail,
      seriesCount: item.seriesCount
    }
  }, [])
  useEffect(() => {
    if (!observerRef.current) return
    setNode(observerRef.current)
  }, [])
  return (
    <Layout siteOwner={owner}>
      <>
        <BlogTabs />
        <Container>
          <Contents>
            {series.length === 0 &&
            getAllSeriesOnPostsStatus === STATUS.DONE ? (
              <h2 style={{ margin: '0 1rem' }}>😢 아무 시리즈가 없습니다</h2>
            ) : (
              <>
                {series.map((item) => {
                  return (
                    <Link
                      href={`/${user}/series/${encodeURIComponent(
                        item.seriesName
                      )}?seriesId=${item.seriesId}`}
                      key={item.seriesId}
                    >
                      <Content>
                        <SeriesSummary item={getPostParams(item)} />
                      </Content>
                    </Link>
                  )
                })}
              </>
            )}
          </Contents>
          <Observer ref={observerRef} />
        </Container>
      </>
    </Layout>
  )
}

export const getServerSideProps = wrapper.getServerSideProps(
  async (context) => {
    const { user } = context.params
    const nickname = encodeURIComponent(user.replace('@', ''))
    if (!user.startsWith('@')) {
      return {
        notFound: true
      }
    }
    const cookie = context.req ? context.req.headers.cookie : ''
    axios.defaults.headers.cookie = ''
    if (context.req && cookie) {
      axios.defaults.headers.cookie = cookie
    }
    context.store.dispatch({
      type: TYPES.LOAD_ME_INFO_REQUEST
    })
    context.store.dispatch({
      type: TYPES.GET_USER_INFO_REQUEST,
      data: { nickname }
    })
    context.store.dispatch({
      type: TYPES.GET_ALL_SERIES_ON_POSTS_REQUEST,
      data: {
        nickname
      }
    })
    context.store.dispatch(END)

    await context.store.sagaTask.toPromise()
  }
)

export default Posts
