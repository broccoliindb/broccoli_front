import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import axios from 'axios'
import { END } from 'redux-saga'
import Layout from '../../components/Layout'
import wrapper from '../../store/configureStore'
import { TYPES } from '../../reducers/rootReducer'
import BlogTabs from '../../components/BlogTabs'
import GridBox from '../../components/controls/GridBox'
import Avatar from '../../components/controls/Avatar'
import Paragraph from '../../components/controls/Paragraph'

const Container = styled.div`
  padding: 1rem;
  @media only screen and (max-width: 600px) {
    padding: 0;
  }
`
const Introduction = styled(GridBox)`
  margin-top: 1rem;
  grid-template-columns: 35% 1fr;
  width: 100%;
  @media only screen and (max-width: 600px) {
    grid-template-columns: 1fr;
    width: 100%;
  }
`
const Whoami = styled.div`
  width: 100%;
  @media only screen and (max-width: 600px) {
    margin: 1.5rem 0;
  }
`
const WhoamiGridBox = styled(GridBox)`
  padding: 0 1rem;
  place-items: center;
  grid-template-columns: 1fr;
  grid-gap: 1rem;
  h2 {
    margin: 0;
  }
`
const IntroductionGridBox = styled(GridBox)`
  grid-template-columns: 1fr;
  grid-template-rows: 1fr auto auto;
  place-items: center;
  grid-gap: 0.5rem;
  padding-right: 2rem;
  border-right: 1px solid var(--greyColor3);
  @media only screen and (max-width: 600px) {
    padding-right: 0;
    border-right: none;
  }
`
const TempContainer = styled.div`
  padding: 0.5rem 0;
  width: 100%;
`
const TempPersonalInfo = styled.div`
  display: grid;
  grid-gap: 1rem;
`
const TempItem = styled.div`
  display: grid;
  grid-template-columns: 30% 1fr;
  grid-template-rows: 1.5rem;
  place-items: center;

  @media only screen and (max-width: 600px) {
    grid-template-columns: 1fr;
  }
`
const Summary = styled.div`
  margin-top: 2rem;
  @media only screen and (max-width: 600px) {
    padding: 0 1rem;
    margin-top: 0;
  }
`
const Title = styled.div`
  background-color: var(--everGreenColor);
  color: var(--whiteColor);
  width: 100%;
  font-size: 1.3rem;
  line-height: 1.5rem;
  font-family: 'do hyeon';
  margin: 0;
  padding: 0.3rem;
  justify-self: center;
`
const Content = styled.div`
  font-family: 'Noto Serif KR';
  line-height: 1.5rem;
  padding: 0.5rem;
  width: 100%;
  justify-self: center;
  &:hover {
    color: var(--everGreenColor);
    text-decoration: underline;
    text-underline-position: under;
  }
`
const Header = styled.h4`
  margin-top: 3rem;
  @media only screen and (max-width: 600px) {
    margin-left: 1rem;
  }
`
const ItemContainer = styled.ul`
  margin: 1rem 0;
  @media only screen and (max-width: 600px) {
    padding: 0 1rem;
  }
`
const Item = styled.li`
  margin-bottom: 2rem;
`

const GridBoxWrapper = styled(GridBox)`
  grid-template-columns: 1fr auto;
  align-items: center;
  border-bottom: 2px solid var(--greyColor3);
  padding-bottom: 1rem;
  h5 {
    margin: 0.5rem 0;
  }
  time {
    justify-self: end;
  }
  @media only screen and (max-width: 600px) {
    grid-template-columns: 1fr;
  }
`
const HistoryContents = styled.ul`
  margin: 1rem 0;
  li {
    padding: 1rem 0.5rem;
  }
`
const ContentTitle = styled.span`
  font-weight: bold;
`
const ContentsUl = styled.ul`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  list-style: disc inside;
  li {
    padding: 1rem 0 0 1rem;
    line-height: 1.5rem;
  }
`

const Posts = () => {
  const { siteOwner } = useSelector((state) => state.user)
  const avatar = siteOwner?.avatar || siteOwner?.defaultavatar
  const nickname = siteOwner?.nickname || ''
  const whoami = siteOwner?.UserDetail?.whoami || ''
  const owner = useMemo(() => {
    if (siteOwner) return siteOwner
    return {
      nickname: '',
      avatar: '',
      defaultavatar: '',
      blogname: ''
    }
  }, [siteOwner])
  return (
    <Layout siteOwner={owner}>
      <>
        <BlogTabs />
        <Container>
          <Introduction>
            <IntroductionGridBox>
              <div>
                {avatar && <Avatar width={200} height={200} imgPath={avatar} />}
              </div>
            </IntroductionGridBox>
            <Whoami>
              <WhoamiGridBox>
                <h2>{nickname}</h2>
                <Paragraph isDefault={!whoami}>
                  {whoami || '소개말이 없습니다.'}
                </Paragraph>
                <>
                  {nickname === 'broccoli' && (
                    <TempContainer>
                      <TempPersonalInfo>
                        <TempItem>
                          <Title>기술블로그</Title>
                          <Content>
                            <a
                              target="_blank"
                              rel="noreferrer"
                              href="https://velog.io/@broccoliindb"
                            >
                              <span>https://velog.io/@broccoliindb</span>
                            </a>
                          </Content>
                        </TempItem>
                        <TempItem>
                          <Title>Bitbucket</Title>
                          <Content>
                            <a
                              target="_blank"
                              rel="noreferrer"
                              href="https://bitbucket.org/broccoliindb/"
                            >
                              https://bitbucket.org/broccoliindb/
                            </a>
                          </Content>
                        </TempItem>
                      </TempPersonalInfo>
                    </TempContainer>
                  )}
                </>
              </WhoamiGridBox>
            </Whoami>
          </Introduction>
          <>
            {nickname === 'broccoli' && (
              <TempContainer>
                <Summary>
                  <p>
                    2015년 9월부터 ezCaretech 이라는 의료IT전문 회사에서 약
                    5년간 근무하며 EHR이라는 전자의료차트를 개발했습니다.
                  </p>
                  <p>
                    재직중 업무개발자로서는 간호모듈에 필요한 신규 화면 및
                    요구사항들을 수행하고 개발했으며 국내외 차세대 프로젝트
                    SI업무와 분당서울대 병원 SM업무에 투입되어 근무했습니다.
                  </p>
                  <p>
                    2018년 부터는 기술팀에 소속되어 클라우드 환경에서 서비스되는
                    Cloud EHR 프로그램에 개발에 참여했습니다. 기술팀
                    소속으로서는 업무개발자들에게 필요로하는 모듈개발과 trouble
                    shooting, Framework 관련 개발 및 유지보수 및 이력관리와
                    배포를 맡아 근무했습니다.
                  </p>
                </Summary>
                <Header>주요기술</Header>
                <ItemContainer>
                  <Item>
                    <h5 style={{ margin: 0 }}>
                      😎 자주 사용하는 기술 스택 및 언어
                    </h5>
                    <ContentsUl>
                      <li>Ubuntu</li>
                      <li>Javascript / ES6</li>
                      <li>CSS / SCSS / styled-components</li>
                      <li>HTML</li>
                      <li>React</li>
                      <li>Next.js</li>
                      <li>Hugo</li>
                      <li>Git</li>
                      <li>Redux</li>
                      <li>Redux-Saga</li>
                      <li>Express</li>
                      <li>Node.js</li>
                      <li>Passport.js</li>
                      <li>Sequelize</li>
                      <li>MySQL</li>
                      <li>Multer</li>
                      <li>eslint</li>
                      <li>regular expression</li>
                    </ContentsUl>
                  </Item>
                  <Item>
                    <h5 style={{ margin: 0 }}>😄 학습중인 기술 스택 및 언어</h5>
                    <ContentsUl>
                      <li>Typescript</li>
                      <li>Elasticsearch</li>
                      <li>Kibana</li>
                      <li>SWR</li>
                      <li>GraphQL</li>
                    </ContentsUl>
                  </Item>
                  <Item>
                    <h5 style={{ margin: 0 }}>
                      😄 활용해 본 기술 스택 및 언어
                    </h5>
                    <ContentsUl>
                      <li>C#</li>
                      <li>WPF</li>
                      <li>Oracle</li>
                      <li>Ember.js</li>
                      <li>Jenkins</li>
                      <li>Netlify</li>
                      <li>Heroku</li>
                      <li>Docker</li>
                      <li>Bash</li>
                      <li>Webpack</li>
                      <li>Gulp</li>
                      <li>Redis</li>
                      <li>winston</li>
                      <li>Pm2</li>
                      <li>Nodemon</li>
                      <li>Nodemailer</li>
                      <li>Ant Design</li>
                      <li>Tui Editor</li>
                      <li>Froala Editor</li>
                      <li>TeamViewer API</li>
                      <li>행안부 주소 API</li>
                      <li>Telegram API</li>
                    </ContentsUl>
                  </Item>
                </ItemContainer>
                <Header>경력사항</Header>
                <ItemContainer>
                  <Item>
                    <GridBoxWrapper>
                      <h5>1. Cloud EHR(Electronic health records) 개발</h5>
                      <time>2018-06~2020~05</time>
                    </GridBoxWrapper>
                    <HistoryContents>
                      <li>
                        <ContentTitle>개발영역: </ContentTitle>
                        Front-end
                      </li>
                      <li>
                        <ContentTitle>포지션: </ContentTitle>
                        기술팀
                      </li>
                      <li>
                        <ContentTitle>개발언어: </ContentTitle>
                        javascript / ES6
                      </li>
                      <li>
                        <ContentTitle>기술셋</ContentTitle>
                        <ContentsUl>
                          <li>
                            Ember Framework: EHR프레임워크인 HIS프레임워크의
                            core프레임워크로서 사용함
                          </li>
                          <li>ES6: 개발언어</li>
                          <li>Sass: 스타일</li>
                          <li>git: 코드관리</li>
                          <li>eslint: 소스품질관리</li>
                          <li>Jenkins: 배포툴</li>
                          <li>텔레그램 API: 이슈발생시 메세지전달 활용</li>
                          <li>Hugo: 기술블로그작성 프레임워크</li>
                          <li>Froala Editor API: 사용자 에디터로서 모듈활용</li>
                          <li>TeamViewer API: 사용자 원격지원로서 활용</li>
                          <li>
                            행안부 도로명 주소 API: 사용자 주소입력 지원활용
                          </li>
                          <li>kibana: 에러로그 확인 활용</li>
                        </ContentsUl>
                      </li>
                      <li>
                        <ContentTitle>상세업무</ContentTitle>
                        <ContentsUl>
                          <li>업무개발자를 위한 공통화면 개발</li>
                          <li>
                            Troble shooting: 개발작업 오류 및 빌드오류,
                            개발환경관련 이슈지원
                          </li>
                          <li>기술지원페이지 구현 및 유지 보수</li>
                          <li>소스코드 버전 관리</li>
                          <li>메모리누수 테스트 및 에러로그</li>
                          <li>외부 라이브러리 사용 테스트</li>
                          <li>사용자 지원 원격모듈 구현 및 관리</li>
                          <li>업무개발자교육</li>
                        </ContentsUl>
                      </li>
                    </HistoryContents>
                  </Item>
                  <Item>
                    <GridBoxWrapper>
                      <h5>2. 분당서울대병원 SM</h5>
                      <time>2017-09~2018~05</time>
                    </GridBoxWrapper>
                    <HistoryContents>
                      <li>
                        <ContentTitle>개발영역: </ContentTitle>
                        Desktop Application (Windows 10)
                      </li>
                      <li>
                        <ContentTitle>포지션: </ContentTitle>
                        업무개발팀 (간호영역)
                      </li>
                      <li>
                        <ContentTitle>개발언어: </ContentTitle>
                        C# / oracle
                      </li>
                      <li>
                        <ContentTitle>기술셋</ContentTitle>
                        <ContentsUl>
                          <li>
                            WPF: 데스크탑용 EHR프레임워크인 HIS프레임워크의
                            core프레임워크로서 사용함
                          </li>
                          <li>C#: 개발언어</li>
                          <li>tfs: 코드관리</li>
                          <li>
                            Oracle: 데이터 조회 수정 생성 및 통계데이터 취합에
                            활용
                          </li>
                        </ContentsUl>
                      </li>
                      <li>
                        <ContentTitle>상세업무</ContentTitle>
                        <ContentsUl>
                          <li>
                            분당병원 Service Request 처리(보건복지부 고시 및
                            병원내부 요청사항)
                          </li>
                          <li>통계 데이터(병원 연구목적)제공</li>
                          <li>신규화면 개발</li>
                          <li>프로그램 유지 보수</li>
                          <li>응급진료정보망(NEDIS)관련 페이지 관리</li>
                        </ContentsUl>
                      </li>
                    </HistoryContents>
                  </Item>
                  <Item>
                    <GridBoxWrapper>
                      <h5>3. 미국 Aurora 병원 차세대 프로젝트 SI</h5>
                      <time>2016-12~2017~08</time>
                    </GridBoxWrapper>
                    <HistoryContents>
                      <li>
                        <ContentTitle>개발영역: </ContentTitle>
                        Desktop Application (Windows 10)
                      </li>
                      <li>
                        <ContentTitle>포지션: </ContentTitle>
                        업무개발팀 (간호/식이/기록)
                      </li>
                      <li>
                        <ContentTitle>개발언어: </ContentTitle>
                        C# / oracle
                      </li>
                      <li>
                        <ContentTitle>기술셋</ContentTitle>
                        <ContentsUl>
                          <li>
                            WPF: 데스크탑용 EHR프레임워크인 HIS프레임워크의
                            core프레임워크로서 사용함
                          </li>
                          <li>C#: 개발언어</li>
                          <li>tfs: 코드관리</li>
                          <li>Oracle: 데이터 조회 수정 생성</li>
                        </ContentsUl>
                      </li>
                      <li>
                        <ContentTitle>상세업무</ContentTitle>
                        <ContentsUl>
                          <li>사용자 교육 및 현장 지원</li>
                          <li>간호, 식이, 기록 관련 신규페이지 개발</li>
                          <li>페이지 유지관리</li>
                        </ContentsUl>
                      </li>
                    </HistoryContents>
                  </Item>
                  <Item>
                    <GridBoxWrapper>
                      <h5>4. 서울대 본원 차세대 프로젝트 SI</h5>
                      <time>2016-06~2017~11</time>
                    </GridBoxWrapper>
                    <HistoryContents>
                      <li>
                        <ContentTitle>개발영역: </ContentTitle>
                        Desktop Application (Windows 10)
                      </li>
                      <li>
                        <ContentTitle>포지션: </ContentTitle>
                        업무개발팀 (간호)
                      </li>
                      <li>
                        <ContentTitle>개발언어: </ContentTitle>
                        C# / oracle
                      </li>
                      <li>
                        <ContentTitle>기술셋</ContentTitle>
                        <ContentsUl>
                          <li>
                            WPF: 데스크탑용 EHR프레임워크인 HIS프레임워크의
                            core프레임워크로서 사용함
                          </li>
                          <li>C#: 개발언어</li>
                          <li>tfs: 코드관리</li>
                          <li>Oracle: 데이터 조회 수정 생성</li>
                        </ContentsUl>
                      </li>
                      <li>
                        <ContentTitle>상세업무</ContentTitle>
                        <ContentsUl>
                          <li>사용자 교육 및 현장 지원</li>
                          <li>신규페이지 개발</li>
                          <li>ASIS 데이터, TOBE 데이터 매핑작업</li>
                          <li>페이지 유지관리</li>
                        </ContentsUl>
                      </li>
                    </HistoryContents>
                  </Item>
                  <Item>
                    <GridBoxWrapper>
                      <h5>5. 해외사이트 HIS 유지보수</h5>
                      <time>2015-09~2016~05</time>
                    </GridBoxWrapper>
                    <HistoryContents>
                      <li>
                        <ContentTitle>개발영역: </ContentTitle>
                        Desktop Application (Windows 10)
                      </li>
                      <li>
                        <ContentTitle>포지션: </ContentTitle>
                        업무개발팀
                      </li>
                      <li>
                        <ContentTitle>개발언어: </ContentTitle>
                        C# / oracle
                      </li>
                      <li>
                        <ContentTitle>기술셋</ContentTitle>
                        <ContentsUl>
                          <li>
                            WPF: 데스크탑용 EHR프레임워크인 HIS프레임워크의
                            core프레임워크로서 사용함
                          </li>
                          <li>C#: 개발언어</li>
                          <li>tfs: 코드관리</li>
                          <li>Oracle: 데이터 조회 수정 생성</li>
                        </ContentsUl>
                      </li>
                      <li>
                        <ContentTitle>상세업무</ContentTitle>
                        <ContentsUl>
                          <li>의무기록관련 신규개발</li>
                        </ContentsUl>
                      </li>
                    </HistoryContents>
                  </Item>
                </ItemContainer>
              </TempContainer>
            )}
          </>
        </Container>
      </>
    </Layout>
  )
}

export const getServerSideProps = wrapper.getServerSideProps(
  async (context) => {
    const { user } = context.params
    const nickname = user.replace('@', '')
    if (!user.startsWith('@')) {
      return {
        notFound: true
      }
    }
    const cookie = context.req ? context.req.headers.cookie : ''
    axios.defaults.headers.cookie = ''
    if (context.req && cookie) {
      axios.defaults.headers.cookie = cookie
    }
    context.store.dispatch({
      type: TYPES.LOAD_ME_INFO_REQUEST
    })
    context.store.dispatch({
      type: TYPES.GET_USER_INFO_REQUEST,
      data: { nickname }
    })
    context.store.dispatch(END)

    await context.store.sagaTask.toPromise()
  }
)

export default Posts
