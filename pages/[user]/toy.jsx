import React, { useMemo } from 'react'
import styled from 'styled-components'
import axios from 'axios'
import { END } from 'redux-saga'
import { useSelector } from 'react-redux'
import Image from 'next/image'
import Layout from '../../components/Layout'
import wrapper from '../../store/configureStore'
import { TYPES } from '../../reducers/rootReducer'
import BlogTabs from '../../components/BlogTabs'
import GridBox from '../../components/controls/GridBox'

const Container = styled.div`
  padding: 1rem;
  @media only screen and (max-width: 600px) {
    padding: 0;
  }
`
const ItemContainer = styled.ul`
  margin: 1rem 0;
  @media only screen and (max-width: 600px) {
    padding: 0 1rem;
  }
`
const Item = styled.li`
  margin-bottom: 2rem;
`
const GridBoxWrapper = styled(GridBox)`
  grid-template-columns: 1fr auto;
  align-items: center;
  border-bottom: 2px solid var(--greyColor3);
  padding-bottom: 1rem;
  h4 {
    margin: 0.5rem 0;
  }
  time {
    justify-self: end;
  }
  @media only screen and (max-width: 600px) {
    grid-template-columns: 1fr;
  }
`
const Anchor = styled.div`
  margin: 1rem 0;
  padding: 1rem 0;
  font-size: 1.5rem;
  text-align: center;
  a {
    &:hover {
      color: var(--everGreenColor);
      text-decoration: underline;
      text-underline-position: under;
    }
  }
  @media only screen and (max-width: 600px) {
    font-size: 1rem;
  }
`
const Images = styled.div`
  display: grid;
  grid-template-columns: 50%;
  justify-content: center;
  @media only screen and (max-width: 600px) {
    grid-template-columns: 80%;
  }
`
const Contents = styled.ul`
  margin: 1rem 0;
  li {
    padding: 1rem 0.5rem;
  }
`
const Details = styled.div`
  padding: 1rem;
  width: 100%;
  word-wrap: break-word;
  line-height: 1.5rem;
`
const ContentTitle = styled.span`
  font-weight: bold;
`
const ContentsUl = styled.ul`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  list-style: disc inside;
  li {
    padding: 1rem 0 0 1rem;
    line-height: 1.5rem;
  }
`

const Toy = () => {
  const { siteOwner } = useSelector((state) => state.user)
  const owner = useMemo(() => {
    if (siteOwner) return siteOwner
    return {
      nickname: '',
      avatar: '',
      defaultavatar: '',
      blogname: ''
    }
  }, [siteOwner])
  const nickname = siteOwner?.nickname || ''
  return (
    <Layout siteOwner={owner}>
      <>
        <BlogTabs />
        <Container>
          {nickname === 'broccoli' && (
            <ItemContainer>
              <Item>
                <GridBoxWrapper>
                  <h4>1. Whale </h4>
                  <time>2021-05 ~ current</time>
                </GridBoxWrapper>
                <Anchor>
                  <a
                    target="_blank"
                    rel="noreferrer"
                    href="https://broccolidb.com"
                  >
                    <span>🤺 방문하기</span>
                  </a>
                </Anchor>
                <Images>
                  <Image width={1000} height={1000} src="/temp/whale1.png" />
                </Images>
                <Contents>
                  <li>
                    <ContentTitle>목적: </ContentTitle>
                    블로그 만들기. [ 현재 프로토타입으로 velog 클론 ]
                  </li>
                  <li>
                    <ContentTitle>기능: </ContentTitle>
                    <ContentsUl>
                      <li>🧑 개인화</li>
                      <li>🖊️ 컨텐츠 작성</li>
                      <li>💭 대댓글 작성</li>
                      <li>❤️ 좋아요</li>
                      <li>🌐 SNS 로그인</li>
                      <li>#️⃣ 해시태그</li>
                      <li>🔍 컨텐츠 검색</li>
                    </ContentsUl>
                  </li>
                  <li>
                    <ContentTitle>기술셋</ContentTitle>
                    <ContentsUl>
                      <li>Next.js</li>
                      <li>Styled-components</li>
                      <li>Tui-editor</li>
                      <li>Redux</li>
                      <li>Redux-saga</li>
                      <li>Express.js</li>
                      <li>MySQL</li>
                      <li>Sequelize</li>
                      <li>Redis</li>
                      <li>Nodemailer</li>
                      <li>Passport.js</li>
                      <li>git: 코드관리</li>
                      <li>eslint: 소스품질관리</li>
                      <li>AWS EC2</li>
                      <li>AWS Elasticsearch</li>
                      <li>AWS S3</li>
                    </ContentsUl>
                  </li>
                  <li>
                    <ContentTitle>비고</ContentTitle>
                    <Details>
                      내가 항상 가장 필요로 하는 프로그램은 노트앱이다. 이미
                      세상엔 다양한 노트앱과 블로그가 있지만 내가 직접 기능을
                      담고 수정하고 개선할 수 있는 프로그램을 만들고 싶었다.
                      내가 항시 필요성을 느끼는 프로그램을 내가 관리한다면
                      개발연습을 하기에도 그리고 그 프로그램을 끝없이
                      유지관리하기에도 좋을 것이라 판단하여 만들어봄. 현재는
                      프로토타입이라 velog를 클론하여 기능위주로 구성함.
                    </Details>
                  </li>
                </Contents>
              </Item>
              <Item>
                <GridBoxWrapper>
                  <h4>2. Popcorn site clone </h4>
                  <time>2020-12 ~ 2021-02</time>
                </GridBoxWrapper>
                <Anchor>
                  <a
                    target="_blank"
                    rel="noreferrer"
                    href="https://popcorn-clone-yong.netlify.app"
                  >
                    <span>🤺 방문하기 </span>
                  </a>
                </Anchor>
                <Images>
                  <Image width={1000} height={1000} src="/temp/popcorn1.png" />
                </Images>
                <Contents>
                  <li>
                    <ContentTitle>목적: </ContentTitle>
                    리액트를 활용한 영화정보를 조회 및 검색 앱 만들기
                  </li>
                  <li>
                    <ContentTitle>기능: </ContentTitle>
                    <ContentsUl>
                      <li>ℹ️ 영화 정보 조회</li>
                      <li>🔍 컨텐츠 검색</li>
                    </ContentsUl>
                  </li>
                  <li>
                    <ContentTitle>기술셋</ContentTitle>
                    <ContentsUl>
                      <li>React.js</li>
                      <li>Styled-components</li>
                      <li>Context API</li>
                      <li>git: 코드관리</li>
                      <li>eslint: 소스품질관리</li>
                      <li>netlify</li>
                    </ContentsUl>
                  </li>
                  <li>
                    <ContentTitle>비고</ContentTitle>
                    <Details>
                      React의 함수형 컴포넌트를 활용한 앱을 만들기 위한 연습앱
                      상태관리를 위해서 context api를 활용하고 Infinite scroll을
                      위해서 IntersectionObserver를 활용함.
                    </Details>
                  </li>
                </Contents>
              </Item>
              <Item>
                <GridBoxWrapper>
                  <h4>3. Grab Algorithm </h4>
                  <time>2020-08 ~ 2021-08</time>
                </GridBoxWrapper>
                <Anchor>
                  <a
                    target="_blank"
                    rel="noreferrer"
                    href="https://grab-algorithm.netlify.app"
                  >
                    <span>🤺 방문하기 </span>
                  </a>
                </Anchor>
                <Images>
                  <Image
                    width={1000}
                    height={1000}
                    src="/temp/grab-algorithm1.png"
                  />
                </Images>
                <Contents>
                  <li>
                    <ContentTitle>목적: </ContentTitle>
                    알고리즘 스터디를 위한 markdown형 블로그
                  </li>
                  <li>
                    <ContentTitle>기능: </ContentTitle>
                    <ContentsUl>
                      <li>🖊️ 컨텐츠 작성</li>
                      <li>💭 댓글 작성</li>
                      <li>🔍 컨텐츠 검색</li>
                      <li>🍔 각 항목별 리스트 분류</li>
                    </ContentsUl>
                  </li>
                  <li>
                    <ContentTitle>기술셋</ContentTitle>
                    <ContentsUl>
                      <li>Hugo</li>
                      <li>CSS</li>
                      <li>HTML</li>
                      <li>netlify</li>
                      <li>gulp</li>
                      <li>flexSearch.js</li>
                      <li>git: 코드관리</li>
                    </ContentsUl>
                  </li>
                  <li>
                    <ContentTitle>비고</ContentTitle>
                    <Details>
                      알고리즘 스터디를 할 때 같이 공부하는 스터디원들과 같이
                      로직을 확인하고 의견을 교환하는 용도로 만들어 봄
                    </Details>
                  </li>
                </Contents>
              </Item>
              <Item>
                <GridBoxWrapper>
                  <h4>4. 카카오톡 화면 클론 </h4>
                  <time>2020-07 ~ 2020-07</time>
                </GridBoxWrapper>
                <Anchor>
                  <a
                    target="_blank"
                    rel="noreferrer"
                    href="https://kakaotalk-clone-yong.netlify.app"
                  >
                    <span>🤺 방문하기</span>
                  </a>
                </Anchor>
                <Images>
                  <Image width={1000} height={1000} src="/temp/kakao1.png" />
                </Images>
                <Contents>
                  <li>
                    <ContentTitle>목적: </ContentTitle>
                    css와 html을 연습할 목적으로 따라해본 카카오톡 UI
                  </li>
                  <li>
                    <ContentTitle>기능: </ContentTitle>
                    <ContentsUl>
                      <li>🖊️ 챗화면</li>
                      <li>🧑 친구리스트화면</li>
                      <li>💭 댓글화면</li>
                      <li>⚙️ 설정화면</li>
                      <li>… 더보기화면</li>
                      <li>🔍 검색화면</li>
                    </ContentsUl>
                  </li>
                  <li>
                    <ContentTitle>기술셋</ContentTitle>
                    <ContentsUl>
                      <li>CSS</li>
                      <li>HTML</li>
                      <li>git: 코드관리</li>
                      <li>netlify</li>
                    </ContentsUl>
                  </li>
                  <li>
                    <ContentTitle>비고</ContentTitle>
                    <Details>
                      BEM을 활용해 클래스를 만들고 카카오톡의 기본 UI를 CSS와
                      HTML을 활용해서 클론함.
                    </Details>
                  </li>
                </Contents>
              </Item>
            </ItemContainer>
          )}
          {nickname !== 'broccoli' && (
            <ItemContainer>
              <h1>저장된 토이 프로젝트정보가 없습니다.😭😭</h1>
            </ItemContainer>
          )}
        </Container>
      </>
    </Layout>
  )
}

export const getServerSideProps = wrapper.getServerSideProps(
  async (context) => {
    const { user } = context.params
    const nickname = encodeURIComponent(user.replace('@', ''))
    if (!user.startsWith('@')) {
      return {
        notFound: true
      }
    }
    const cookie = context.req ? context.req.headers.cookie : ''
    axios.defaults.headers.cookie = ''
    if (context.req && cookie) {
      axios.defaults.headers.cookie = cookie
    }
    context.store.dispatch({
      type: TYPES.GET_USER_INFO_REQUEST,
      data: { nickname }
    })
    context.store.dispatch({
      type: TYPES.LOAD_ME_INFO_REQUEST
    })

    context.store.dispatch(END)

    await context.store.sagaTask.toPromise()
  }
)

export default Toy
