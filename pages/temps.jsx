import React, { useCallback, useMemo, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import styled from 'styled-components'
import axios from 'axios'
import { END } from 'redux-saga'
import { useRouter } from 'next/router'
import Layout from '../components/Layout'
import wrapper from '../store/configureStore'
import { STATUS, TYPES } from '../reducers/rootReducer'
import PostSummary from '../components/PostSummary'
import { LOAD_POSTS_TYPE } from '../reducers/enums'
import Loading from '../components/controls/Loading'

const Container = styled.div`
  padding: 1rem;
  @media only screen and (max-width: 600px) {
    padding: 0;
  }
`
const Contents = styled.ul``
const Content = styled.li`
  display: grid;
  grid-template-rows: repeat(auto-fit, auto);
  padding-top: 1rem;
  padding-bottom: 2rem;
  border-bottom: 1px solid var(--greyColor4);
`

const Posts = () => {
  const { posts, status: postStatus } = useSelector((state) => state.post)
  const router = useRouter()
  const dispatch = useDispatch()
  const { toastStatus, getPostsStatus } = postStatus
  const { me } = useSelector((state) => state.user)
  const owner = useMemo(() => {
    if (me) return me
    return {
      nickname: '',
      avatar: '',
      defaultavatar: '',
      blogname: ''
    }
  }, [me])
  const getPostParams = useCallback((post) => {
    return {
      title: post.title,
      tempTitle: post.tempTitle,
      updatedat: post.updatedAt,
      content: post.content,
      tempContent: post.tempContent,
      postkey: post.postKey,
      thumbnail: post.thumbnail,
      nickname: post.User.nickname,
      avatar: post.User.avatar || post.User.defaultavatar,
      likeCount: post.Liker?.length,
      commentCount: post.Comments?.length,
      hashtags: post.Hashtags
    }
  }, [])
  useEffect(() => {
    if (!me) {
      dispatch({
        type: TYPES.GET_POSTS_REQUEST,
        data: {
          loadType: LOAD_POSTS_TYPE.IN_TEMP
        }
      })
    }
  }, [me])
  useEffect(() => {
    if (!me && getPostsStatus === STATUS.DONE && toastStatus === STATUS.DONE) {
      router.replace('/')
    }
  }, [me, postStatus])
  return (
    <Layout siteOwner={owner}>
      <Container>
        <Contents>
          {posts.length === 0 && getPostsStatus === STATUS.DONE ? (
            <Content>
              <h2 style={{ margin: '0 1rem' }}>😢 아무 컨텐츠가 없습니다</h2>
            </Content>
          ) : (
            <>
              {posts.map((post) => (
                <Content key={post.id}>
                  <PostSummary post={getPostParams(post)} isTemp />
                </Content>
              ))}
            </>
          )}
        </Contents>
        {!me && toastStatus === STATUS.LOADING && (
          <Loading content="로그인 사용자가 없습니다. 홈으로 이동합니다." />
        )}
      </Container>
    </Layout>
  )
}

export const getServerSideProps = wrapper.getServerSideProps(
  async (context) => {
    const cookie = context.req ? context.req.headers.cookie : ''
    axios.defaults.headers.cookie = ''
    if (context.req && cookie) {
      axios.defaults.headers.cookie = cookie
    }
    context.store.dispatch({
      type: TYPES.LOAD_ME_INFO_REQUEST
    })
    context.store.dispatch({
      type: TYPES.GET_POSTS_REQUEST,
      data: {
        loadType: LOAD_POSTS_TYPE.IN_TEMP
      }
    })
    context.store.dispatch(END)

    await context.store.sagaTask.toPromise()
  }
)

export default Posts
