import React, { useEffect, useState, useCallback, useRef, useMemo } from 'react'
import { useRouter } from 'next/router'
import styled from 'styled-components'
import axios from 'axios'
import { END } from 'redux-saga'
import { useSelector, useDispatch } from 'react-redux'
import Layout from '../components/Layout'
import Avatar from '../components/controls/Avatar'
import wrapper from '../store/configureStore'
import { STATUS, TYPES, REDUCER } from '../reducers/rootReducer'
import Button from '../components/controls/Button'
import Input from '../components/controls/Input'
import GridBox from '../components/controls/GridBox'
import Paragraph from '../components/controls/Paragraph'
import useInput from '../hooks/useInput'
import InputButton from '../components/controls/InputButton'
import getImage, { UPLOAD_TYPE } from '../libs/uploadImage'
import MessageBox from '../components/controls/MessageBox'
import useDebounce from '../hooks/useDebounce'
import Loading from '../components/controls/Loading'
import Toast from '../components/controls/Toast'

const Container = styled.div`
  padding: 1rem;
  width: 100%;
`
const Introduction = styled(GridBox)`
  grid-template-columns: 20% 1fr;
  width: 100%;
  @media only screen and (max-width: 600px) {
    grid-template-columns: 1fr;
    width: 100%;
  }
`
const Whoami = styled.div`
  padding-left: 1rem;
  width: 100%;
  @media only screen and (max-width: 600px) {
    padding: 0;
    margin: 2rem 0;
  }
`
const WhoamiGridBox = styled(GridBox)`
  grid-template-columns: 1fr;
  grid-gap: 1rem;
  h2 {
    margin: 0;
  }
`
const ContentsGridBox = styled(GridBox)`
  grid-template-columns: repeat(auto-fit, minmax(10px, max-content));
  grid-template-rows: 1fr;
  @media only screen and (max-width: 600px) {
    grid-gap: 1rem;
  }
`
const ContentGridBox = styled(GridBox)`
  grid-template-columns: 20% 1fr;
  padding: 1rem 0;
  @media only screen and (max-width: 600px) {
    grid-template-columns: 1fr;
    padding: 0;
    grid-gap: 1rem;
  }
`
const ButtonGridBox = styled(GridBox)`
  grid-gap: 0.3rem;
  grid-template-columns: 1fr auto;
  button {
    display: grid;
    justify-self: end;
  }
  @media only screen and (max-width: 600px) {
    justify-self: center;
    grid-template-columns: 1fr 1fr;
    padding: 0;
    grid-gap: 0.5rem;
  }
`
const IntroductionGridBox = styled(GridBox)`
  grid-template-columns: 1fr;
  grid-template-rows: 1fr auto auto;
  place-items: center;
  grid-gap: 0.5rem;
  padding-right: 2rem;
  border-right: 1px solid var(--greyColor3);
  @media only screen and (max-width: 600px) {
    padding-right: 0;
    border-right: none;
  }
`
const Upload = styled.div``
const EDIT_TYPE = {
  EDIT: 'EDIT',
  VIEW: 'VIEW',
  SAVE: 'SAVE'
}
const Settings = () => {
  const avatarInput = useRef(null)
  const router = useRouter()
  const { token } = router.query
  const dispatch = useDispatch()
  const [showMessageBox, setShowMessageBox] = useState(false)
  const [message, setMessage] = useState('')
  const [toastMessage, setToastMessage] = useState('')
  const [toastType, setToastType] = useState(null)
  const { me, status: userStatus } = useSelector((state) => state.user)
  const { logoutStatus, checkTokenStatus, toastStatus, removeAccountStatus } =
    userStatus
  const [avatar, setAvatar] = useState(me && (me?.avatar || me?.defaultavatar))
  const [nickname, onChangeNickname, setNickname] = useInput(me?.nickname)
  const [whoami, onChangeWhoami, setWhoami] = useInput(me?.UserDetail?.whoami)
  const [password, onChangePassword, setPassword] = useInput('')
  const [blogname, onChangeBlogname, setBlogname] = useInput(me?.blogname)
  const [isEdited, setIsEdited] = useState(false)
  const owner = useMemo(() => {
    if (me) return me
    return {
      nickname: '',
      avatar: '',
      defaultavatar: '',
      blogname: ''
    }
  }, [me])
  const changePassword = useCallback(() => {
    const passwordRegex = new RegExp(
      '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})'
    )
    if (!password.match(passwordRegex)) {
      setMessage(
        '비밀번호는 최소 한 개의 대문자,소문자,특수문자,숫자를 포함한 8자리 이상의 수 여야합니다. '
      )
      return
    }
    dispatch({
      type: TYPES.UPDATE_ME_INFO_REQUEST,
      data: { user: me.id, password }
    })
    setPassword('')
  }, [password, me])
  const changeBlogname = useCallback(() => {
    dispatch({
      type: TYPES.UPDATE_ME_INFO_REQUEST,
      data: { user: me.id, blogname }
    })
  }, [blogname, me])
  const clearPassword = useCallback(() => {
    setPassword('')
  }, [])
  const clearBlogname = useCallback(() => {
    setBlogname('')
  }, [])
  const onSetWhoami = useCallback(
    (editType) => () => {
      if (editType === EDIT_TYPE.EDIT) {
        setIsEdited(true)
      } else if (editType === EDIT_TYPE.VIEW) {
        if (!nickname) {
          setNickname(me.nickname)
        }
        setIsEdited(false)
      } else {
        dispatch({
          type: TYPES.UPDATE_ME_INFO_REQUEST,
          data: { user: me.id, whoami, nickname }
        })
        setIsEdited(false)
      }
    },
    [nickname, whoami]
  )
  const onUploadAvatar = useCallback(() => {
    avatarInput.current.click()
  }, [])
  const onDeleteAvatar = useCallback(() => {
    dispatch({
      type: TYPES.UPDATE_ME_INFO_REQUEST,
      data: { user: me.id, avatar: null }
    })
  }, [])
  const onChangeAvatar = useCallback(async (e) => {
    const avatarUploaded = e.target.files[0]
    if (!avatarUploaded) return
    const image = await getImage(avatarUploaded, UPLOAD_TYPE.AVATAR)
    dispatch({
      type: TYPES.UPDATE_ME_INFO_REQUEST,
      data: { user: me.id, avatar: image.data.src }
    })
  }, [])
  const onRemoveAccount = useCallback(() => {
    dispatch({ type: TYPES.REMOVE_ACCOUNT_REQUEST, data: { user: me.id } })
    setShowMessageBox(false)
  }, [])
  const onDragOver = useCallback((e) => {
    e.preventDefault()
  }, [])

  const onDragEnter = useCallback((e) => {
    e.preventDefault()
  }, [])

  const onDragLeave = useCallback((e) => {
    e.preventDefault()
  }, [])

  const onFileDrop = useCallback(async (e) => {
    e.preventDefault()
    const fileUploaded = e.dataTransfer.files[0]
    if (!fileUploaded) return
    const image = await getImage(fileUploaded, UPLOAD_TYPE.AVATAR)
    dispatch({
      type: TYPES.UPDATE_ME_INFO_REQUEST,
      data: { user: me.id, avatar: image.data.src }
    })
  }, [])
  const onFocus = useCallback(() => {
    setMessage('')
  }, [])
  const onLoadRemoveMessageBox = useCallback(() => {
    setShowMessageBox(true)
  })
  const onCancelRemoveAccount = useCallback(() => {
    setShowMessageBox(false)
  }, [])
  const deleteAvatar = useDebounce(onDeleteAvatar, 300)
  const uploadAvatar = useDebounce(onUploadAvatar, 300)
  const removeAccount = useDebounce(onRemoveAccount, 300)
  useEffect(() => {
    if (me) {
      setAvatar(me && (me.avatar || me.defaultavatar))
      setNickname(me.nickname)
      setBlogname(me.blogname || '')
      setWhoami(me.UserDetail?.whoami || '')
    }
    if (!me) {
      if (
        !token &&
        removeAccountStatus === STATUS.DONE &&
        toastStatus === STATUS.DONE
      ) {
        setToastMessage('')
        setToastType(null)
        return router.replace('/')
      }
      if (checkTokenStatus === STATUS.ERROR && toastStatus === STATUS.DONE) {
        setToastMessage('')
        setToastType(null)
        return router.replace('/')
      }
      if (logoutStatus === STATUS.DONE && toastStatus === STATUS.DONE) {
        setToastMessage('')
        setToastType(null)
        return router.replace('/')
      }
    }
  }, [me, userStatus, token])
  useEffect(() => {
    if (!me) {
      if (token && checkTokenStatus !== STATUS.DONE) {
        if (logoutStatus !== STATUS.DONE) {
          dispatch({ type: TYPES.CHECK_TOKEN_REQUEST, data: token })
        }
      }
    }
  }, [])
  useEffect(() => {
    if (removeAccountStatus === STATUS.DONE) {
      setToastMessage('회원탈퇴가 완료되었습니다.')
      setToastType(STATUS.DONE)
    }
    if (checkTokenStatus === STATUS.ERROR) {
      setToastMessage('유효하지 않은 토큰입니다.')
      setToastType(STATUS.ERROR)
    }
    if (logoutStatus === STATUS.DONE) {
      setToastMessage('로그아웃되었습니다.')
      setToastType(STATUS.DONE)
    }
  }, [removeAccountStatus, checkTokenStatus, logoutStatus])
  return (
    <Layout siteOwner={owner}>
      <Container>
        {((!me && !token) ||
          (!me && token && checkTokenStatus === STATUS.DONE)) && (
          <h2 style={{ margin: '0.5rem 1rem', lineHeight: '3rem' }}>
            😢 올바른 사용자 정보가 없습니다.
          </h2>
        )}
        {!me && token && checkTokenStatus === STATUS.LOADING && (
          <Loading content="토큰 확인중 입니다." />
        )}
        {me && (
          <>
            <Introduction>
              <IntroductionGridBox>
                <Upload
                  onDragOver={onDragOver}
                  onDragEnter={onDragEnter}
                  onDragLeave={onDragLeave}
                  onDrop={onFileDrop}
                >
                  {avatar && (
                    <Avatar width={128} height={128} imgPath={avatar} />
                  )}
                </Upload>
                <label
                  htmlFor="avatar"
                  style={{ width: '100%', padding: 0, margin: 0 }}
                >
                  <Button
                    width100
                    onClick={uploadAvatar}
                    style={{
                      fontSize: '1rem',
                      backgroundColor: 'var(--everGreenColor)',
                      color: 'var(--whiteColor)',
                      borderRadius: '5px',
                      padding: '0.5rem'
                    }}
                  >
                    이미지 업로드
                  </Button>
                  <input
                    ref={avatarInput}
                    type="file"
                    id="avatar"
                    onChange={onChangeAvatar}
                    style={{ display: 'none' }}
                  />
                </label>
                <Button
                  width100
                  isBorder
                  onClick={deleteAvatar}
                  style={{
                    fontSize: '1rem',
                    backgroundColor: 'var(--whiteColor)',
                    borderRadius: '5px',
                    color: 'var(--everGreenColor)',
                    padding: '0.5rem'
                  }}
                >
                  이미지 제거
                </Button>
              </IntroductionGridBox>
              <Whoami>
                {!isEdited && (
                  <WhoamiGridBox>
                    <h2
                      style={{
                        overflow: 'hidden',
                        wordSpace: 'nowrap',
                        textOverflow: 'ellipsis'
                      }}
                    >
                      {nickname}
                    </h2>
                    <Paragraph isDefault={!whoami}>
                      {whoami || '소개말이 없습니다.'}
                    </Paragraph>
                    <Button
                      onClick={onSetWhoami(EDIT_TYPE.EDIT)}
                      style={{
                        fontSize: '1rem',
                        backgroundColor: 'var(--everGreenColor)',
                        color: 'var(--whiteColor)',
                        justifySelf: 'end',
                        borderRadius: '5px',
                        padding: '0.5rem'
                      }}
                    >
                      수정
                    </Button>
                  </WhoamiGridBox>
                )}
                {isEdited && (
                  <WhoamiGridBox>
                    <Input
                      isHeader
                      isBorder
                      value={nickname || ''}
                      onChange={onChangeNickname}
                      placeholder="별칭은 무엇인가요?"
                      required
                    />
                    <Input
                      style={{ padding: '0.5rem 0' }}
                      isBorder
                      value={whoami || ''}
                      onChange={onChangeWhoami}
                      placeholder="나는 어떤 사람인가요?"
                    />
                    <ButtonGridBox>
                      <Button
                        isBorder
                        onClick={onSetWhoami(EDIT_TYPE.VIEW)}
                        style={{
                          fontSize: '1rem',
                          backgroundColor: 'var(--whiteColor)',
                          color: 'var(--everGreenColor)',
                          padding: '0.5rem',
                          borderRadius: '5px'
                        }}
                      >
                        취소
                      </Button>
                      <Button
                        onClick={onSetWhoami(EDIT_TYPE.SAVE)}
                        style={{
                          fontSize: '1rem',
                          backgroundColor: 'var(--everGreenColor)',
                          color: 'var(--whiteColor)',
                          padding: '0.5rem',
                          borderRadius: '5px'
                        }}
                      >
                        저장
                      </Button>
                    </ButtonGridBox>
                  </WhoamiGridBox>
                )}
              </Whoami>
            </Introduction>
            <ContentsGridBox
              style={{
                gridTemplateRows: 'repeat(auto-fit, minmax(10px, max-content))',
                gridTemplateColumns: '1fr'
              }}
            >
              <ContentGridBox>
                <h5
                  style={{ margin: 0, display: 'flex', alignItems: 'center' }}
                >
                  블로그명
                </h5>
                <div>
                  <Paragraph style={{ marginBottom: '0.3rem' }} isDefault>
                    디폴트: 닉네임.log
                  </Paragraph>
                  <InputButton
                    type="text"
                    isInputButton
                    value={blogname || ''}
                    onChange={onChangeBlogname}
                    isClear
                    isBorder
                    onClear={clearBlogname}
                    onClick={changeBlogname}
                    placeholder="블로그명을 입력하세요."
                    content="블로그명변경"
                  />
                </div>
              </ContentGridBox>
              <ContentGridBox>
                <h5
                  style={{ margin: 0, display: 'flex', alignItems: 'center' }}
                >
                  이메일주소
                </h5>
                <div>
                  <Paragraph style={{ marginBottom: '0.3rem' }} isDefault>
                    비밀번호변경시 토큰발행에 사용됩니다.
                  </Paragraph>
                  <p>{me.email}</p>
                </div>
              </ContentGridBox>
              <ContentGridBox>
                <h5
                  style={{ margin: 0, display: 'flex', alignItems: 'center' }}
                >
                  비밀번호변경
                </h5>
                <form>
                  {message && (
                    <Paragraph
                      style={{
                        color: 'var(--redColor)',
                        marginBottom: '0.3rem'
                      }}
                      isDefault
                    >
                      {message}
                    </Paragraph>
                  )}
                  <InputButton
                    type="password"
                    InputButton
                    isBorder
                    value={password || ''}
                    onChange={onChangePassword}
                    isClear
                    onFocus={onFocus}
                    onClear={clearPassword}
                    onClick={changePassword}
                    placeholder="비밀번호를 변경하세요"
                    content="비밀번호변경"
                    autoComplete="off"
                  />
                </form>
              </ContentGridBox>
              <ContentGridBox>
                <h5
                  style={{ margin: 0, display: 'flex', alignItems: 'center' }}
                >
                  회원탈퇴
                </h5>
                <Button
                  onClick={onLoadRemoveMessageBox}
                  style={{
                    justifySelf: 'end',
                    fontSize: '1rem',
                    backgroundColor: 'var(--everGreenColor)',
                    color: 'var(--whiteColor)',
                    padding: '0.5rem',
                    borderRadius: '5px'
                  }}
                >
                  회원탈퇴
                </Button>
              </ContentGridBox>
            </ContentsGridBox>
          </>
        )}
        {showMessageBox && (
          <MessageBox
            onCancel={onCancelRemoveAccount}
            onOK={removeAccount}
            title="회원탈퇴"
            message="회원 탈퇴하시겠습니까? 회원탈퇴시 작성한 모든 컨텐츠는 삭제됩니다."
          />
        )}
        {!me && toastStatus === STATUS.LOADING && (
          <Loading content="홈으로 이동합니다." />
        )}
        {toastMessage && (
          <Toast
            toastToward={REDUCER.USER}
            wait={3000}
            type={toastType}
            message={toastMessage}
          />
        )}
      </Container>
    </Layout>
  )
}

export const getServerSideProps = wrapper.getServerSideProps(
  async (context) => {
    const cookie = context.req ? context.req.headers.cookie : ''
    axios.defaults.headers.cookie = ''
    if (context.req && cookie) {
      axios.defaults.headers.cookie = cookie
    }
    context.store.dispatch({
      type: TYPES.LOAD_ME_INFO_REQUEST
    })
    context.store.dispatch(END)

    await context.store.sagaTask.toPromise()
  }
)

export default Settings
