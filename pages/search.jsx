import React, { useCallback, useState, useRef, useEffect, useMemo } from 'react'
import axios from 'axios'
import { useRouter } from 'next/router'
import { END } from 'redux-saga'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import wrapper from '../store/configureStore'
import { TYPES } from '../reducers/rootReducer'
import Layout from '../components/Layout'
import GridBox from '../components/controls/GridBox'
import FlexBox from '../components/controls/FlexBox'
import SearchedPost from '../components/SearchedPost'
import SearchInput from '../components/controls/SearchInput'
import useDebounce from '../hooks/useDebounce'
import Switch from '../components/controls/Switch'

const Container = styled.div`
  padding: 1rem;
  @media only screen and (max-width: 600px) {
    padding: 0;
  }
`
const Contents = styled.ul``
const Content = styled.li`
  display: grid;
  grid-template-rows: repeat(auto-fit, auto);
  padding-top: 1rem;
  padding-bottom: 2rem;
  border-bottom: 1px solid var(--greyColor4);
`
const FlexBoxWrapper = styled(FlexBox)`
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  padding-left: 0.5rem;
`
const SearchTermToken = styled.div`
  background-color: var(--greyColor3);
  width: max-content;
  padding: 0.3rem;
  margin: 0.2rem;
  border-radius: 5px;
`
const Search = () => {
  const searchInput = useRef(null)
  const [term, setTerm] = useState('')
  const [isAll, setIsAll] = useState(false)
  const [isFocus, setIsFocus] = useState(false)
  const { searchTermTokens } = useSelector((state) => state.search)
  const me = useSelector((state) => state.user.me)
  const {
    total: { value: totalCount },
    hits: posts
  } = useSelector((state) => state.search.searchResult)
  const nickname = me?.nickname
  const userId = me?.id
  const router = useRouter()
  const owner = useMemo(() => {
    if (me) return me
    return {
      nickname: '',
      avatar: '',
      defaultavatar: '',
      blogname: ''
    }
  }, [me])
  const searchRequest = useCallback(
    (word) => {
      if (!isAll && nickname) {
        router.replace({
          pathname: '/search',
          query: { username: nickname, userId, q: word }
        })
      } else {
        router.replace({
          pathname: '/search',
          query: { q: word }
        })
      }
    },
    [isAll, nickname]
  )
  const searchDebounce = useDebounce((word) => searchRequest(word), 400)
  const onSearch = (e) => {
    setTerm(e.target.value)
    searchDebounce(e.target.value)
  }

  const Results = useCallback(() => {
    if (!term) return ''
    const statement =
      totalCount === 0
        ? '검색결과가 없습니다.'
        : `총 ${totalCount}개의 결과가 있습니다.`
    return <div style={{ margin: '0 0 1rem 0.5rem' }}>{statement}</div>
  }, [term, totalCount])
  const searchFocus = useCallback(() => {
    if (!isFocus) {
      setIsFocus(true)
      searchInput.current.focus()
    }
  }, [isFocus])
  const searchFocusOut = useCallback(() => {
    if (isFocus) {
      setIsFocus(false)
    }
  }, [isFocus])
  const onSwitch = useCallback(() => {
    if (!me) return
    setIsAll((prev) => {
      if (prev && nickname) {
        router.replace({
          pathname: '/search',
          query: { username: nickname, userId, q: term }
        })
      } else {
        router.replace({
          pathname: '/search',
          query: { q: term }
        })
      }
      return !prev
    })
  }, [me, term, nickname])
  const getSearchCondition = useCallback(() => {
    const searchCondition = isAll
      ? '전체 블로그 내에서 검색합니다.'
      : '내 블로그내에서만 검색합니다.'
    if (!me) {
      return (
        <button
          type="button"
          style={{
            backgroundColor: 'white',
            color: 'var(--greyColor4)',
            border: 'none',
            cursor: 'pointer',
            lineHeight: '2rem',
            textDecoration: 'underline'
          }}
        >
          <span
            style={{
              fontSize: '1.2rem',
              justifySelf: 'start'
            }}
          >
            전체 블로그 내에서 검색합니다.
          </span>
        </button>
      )
    }
    return (
      <GridBox
        style={{ placeItems: 'center', gridTemplateColumns: 'auto 1fr' }}
      >
        <button
          type="button"
          onClick={onSwitch}
          style={{
            backgroundColor: 'white',
            color: 'var(--everGreenColor)',
            border: 'none',
            cursor: 'pointer',
            lineHeight: '2rem',
            textDecoration: 'underline',
            textUnderlinePosition: 'under',
            justifySelf: 'start'
          }}
        >
          <span
            style={{
              fontSize: '1.2rem',
              justifySelf: 'start'
            }}
          >
            {searchCondition}
          </span>
        </button>
        <Switch
          style={{ justifySelf: 'start' }}
          onClick={onSwitch}
          isSelected={isAll}
        />
      </GridBox>
    )
  }, [isAll, me])
  const getPostParams = useCallback((post) => {
    return {
      title: post.title,
      updatedat: post.updatedat,
      content: post.content,
      postkey: post.postkey,
      thumbnail: post.thumbnail,
      nickname: post.nickname,
      avatar: post.avatar || post.defaultavatar,
      userId: post.userId
    }
  }, [])
  useEffect(() => {
    if (!me) {
      setIsAll(true)
      router.replace({
        pathname: '/search',
        query: { q: term }
      })
    }
  }, [me])
  useEffect(() => {
    if (!searchInput.current) return
    searchInput.current.focus()
    setIsFocus(true)
  }, [])
  return (
    <Layout siteOwner={owner}>
      <div>
        <GridBox
          style={{
            gridTemplateColumns: '1fr',
            gridGap: '0.1rem',
            marginTop: '2rem'
          }}
        >
          {getSearchCondition()}
          <SearchInput
            onClick={searchFocus}
            onFocus={searchFocus}
            onBlur={searchFocusOut}
            ref={searchInput}
            isFocus={isFocus}
            style={{
              marginBottom: '1rem',
              gridColumn: '1/-1'
            }}
            placeholder="검색어를 입력하세요."
            value={term}
            onChange={onSearch}
          />
        </GridBox>
        <FlexBoxWrapper>
          {searchTermTokens && searchTermTokens.length > 0 && (
            <span style={{ fontWeight: 'bold', fontSize: '1.2rem' }}>
              검색토큰:
            </span>
          )}
          {searchTermTokens.map((item, index) => (
            <SearchTermToken key={`${index + item.token}`}>
              {item.token}
            </SearchTermToken>
          ))}
        </FlexBoxWrapper>
        <Results />
        <Container>
          <Contents>
            {totalCount > 0 &&
              posts.map((post) => {
                const { _id: id } = post
                const { _source: postContents } = post
                return (
                  <Content key={post.id}>
                    <SearchedPost
                      key={id}
                      post={getPostParams(postContents)}
                      termTokens={searchTermTokens}
                    />
                  </Content>
                )
              })}
          </Contents>
        </Container>
      </div>
    </Layout>
  )
}

export const getServerSideProps = wrapper.getServerSideProps(
  async (context) => {
    const cookie = context.req ? context.req.headers.cookie : ''
    axios.defaults.headers.cookie = ''
    const { q, username, userId } = context.query
    if (context.req && cookie) {
      axios.defaults.headers.cookie = cookie
    }
    context.store.dispatch({
      type: TYPES.LOAD_ME_INFO_REQUEST
    })
    context.store.dispatch({
      type: TYPES.SEARCH_TERM_REQUEST,
      data: { q, username, userId }
    })
    if (q) {
      context.store.dispatch({
        type: TYPES.GET_SEARCH_TERM_TOKENS_REQUEST,
        data: q
      })
    }
    context.store.dispatch(END)

    await context.store.sagaTask.toPromise()
  }
)

export default Search
