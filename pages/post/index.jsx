import React, { useEffect, useState, useCallback, useMemo } from 'react'
import { useRouter } from 'next/router'
import styled from 'styled-components'
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'
import { END } from 'redux-saga'
import { TYPES, STATUS, REDUCER } from '../../reducers/rootReducer'
import wrapper from '../../store/configureStore'
import PostEditForm from '../../components/PostEditForm'
import MessageBox from '../../components/controls/MessageBox'
import Toast from '../../components/controls/Toast'
import Loading from '../../components/controls/Loading'

const Container = styled.main`
  position: relative;
`

const Post = () => {
  const router = useRouter()
  const dispatch = useDispatch()
  const me = useSelector((state) => state.user.me)
  const loginNickname = me?.nickname
  const {
    status: postStatus,
    mainPost,
    message
  } = useSelector((state) => state.post)
  const { addPostStatus, tempSaveStatus, addHashTagStatus } = postStatus
  const { isTemp, isUse, postKey } = mainPost
  const { query } = router
  const key = query?.key
  const initMainPost = useMemo(
    () => ({
      id: -1,
      title: '',
      postKey: '',
      content: '',
      markdown: '',
      updatedAt: '',
      Liker: [],
      Comments: [],
      Hashtags: []
    }),
    []
  )
  const [showMessageBox, setLoadMessageBox] = useState(key && isTemp && isUse)
  /**
  임시저장글 로드 취소하기
  */
  const onCancelLoadTempPost = useCallback(() => {
    setLoadMessageBox(false)
  }, [])

  /**
  임시저장글 로드하기
  */
  const onLoadTempPost = useCallback(() => {
    dispatch({ type: TYPES.GET_TEMP_POST_REQUEST, data: { postKey } })
    setLoadMessageBox(false)
  }, [mainPost])

  const postToast = useCallback(() => {
    if (!message) return
    return (
      <Toast
        toastToward={REDUCER.POST}
        wait={3000}
        type={tempSaveStatus || addHashTagStatus}
        message={message}
      />
    )
  }, [postStatus, message])
  /**
  상태변경시
  1. 게시글 저장시 viewer로 이동
  2. 임시저장시
  */
  useEffect(() => {
    if (addPostStatus === STATUS.DONE) {
      router.replace(`/@${loginNickname}/${postKey}`)
    }
    if (tempSaveStatus === STATUS.DONE) {
      if (!key) {
        router.replace(`/post?key=${postKey}`)
      }
    }
  }, [postStatus])
  useEffect(() => {
    if (!me) {
      router.replace('/')
    }
  }, [me])

  return (
    <Container>
      {addPostStatus === STATUS.LOADING && <Loading content="저장중입니다." />}
      {key && <PostEditForm prevPost={mainPost} />}
      {!key && <PostEditForm prevPost={initMainPost} />}
      {showMessageBox && (
        <MessageBox
          onCancel={onCancelLoadTempPost}
          onOK={onLoadTempPost}
          title="임시 포스트 불러오기"
          message="임시저장된 포스트를 불러오시겠습니까?"
        />
      )}
      {postToast()}
    </Container>
  )
}

export const getServerSideProps = wrapper.getServerSideProps(
  async (context) => {
    const { key } = context.query
    const cookie = context.req ? context.req.headers.cookie : ''
    axios.defaults.headers.cookie = ''
    if (context.req && cookie) {
      axios.defaults.headers.cookie = cookie
    }
    context.store.dispatch({
      type: TYPES.LOAD_ME_INFO_REQUEST
    })
    if (key) {
      context.store.dispatch({
        type: TYPES.GET_POST_REQUEST,
        data: { postKey: key }
      })
    }
    context.store.dispatch(END)

    await context.store.sagaTask.toPromise()
  }
)

export default Post
