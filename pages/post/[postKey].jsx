import React from 'react'
import axios from 'axios'
import { END } from 'redux-saga'
import { TYPES } from '../../reducers/rootReducer'
import wrapper from '../../store/configureStore'
import PostKey from '../../components/PostKey'

const UserPost = () => {
  return <PostKey />
}
export const getServerSideProps = wrapper.getServerSideProps(
  async (context) => {
    const { postKey } = context.params
    const { authorId } = context.query

    const cookie = context.req ? context.req.headers.cookie : ''
    axios.defaults.headers.cookie = ''
    if (context.req && cookie) {
      axios.defaults.headers.cookie = cookie
    }
    context.store.dispatch({
      type: TYPES.LOAD_ME_INFO_REQUEST
    })
    context.store.dispatch({
      type: TYPES.GET_USER_INFO_REQUEST,
      data: { userId: authorId }
    })
    context.store.dispatch({
      type: TYPES.GET_POST_REQUEST,
      data: { postKey: context.query ? context.query.postKey : '' }
    })
    context.store.dispatch({
      type: TYPES.GET_COMMENTS_REQUEST,
      data: { postKey: context.query && context.query.postKey }
    })
    context.store.dispatch({
      type: TYPES.GET_POST_META_LIST_ON_SERIES_REQUEST,
      data: postKey
    })
    context.store.dispatch(END)
    await context.store.sagaTask.toPromise()
  }
)

export default UserPost
