import React from 'react'
import Head from 'next/head'
import PropTypes from 'prop-types'
import { createGlobalStyle } from 'styled-components'
import reset from 'styled-reset'
import wrapper from '../store/configureStore'
import '../rootStyle.css'
import '../rootVariables.css'

const GlobalStyle = createGlobalStyle`
${reset}
* {
  font-family: 'Nanum Gothic', sans-serif;
  input {
    font-family: sans-serif
  }
  body {
    font-family: 'Nanum Gothic';
  }
  box-sizing: border-box;
  outline: none;
  a {
    text-decoration:none;
    color: inherit;
  }
  p {
    padding-bottom: 1rem;
    line-height:1.5rem;
  }
 
  span.term {
    background-color: yellow;
  }
  img {
    max-width: 100%100vh;
    display: block;
  }
  hr {
    background-color: var(--greyColor4);
    height: 1px;
    border: 0;
    width: 100%;
  }
  h1, h2, h3, h4, h5 {
    font-weight: normal !important;
    font-family: 'Do hyeon';
    margin-bottom: 1.5rem;
    color: var(--blackColor);
  }
  h1 {
    font-size: 3rem;
  }
  h2 {
    font-size: 2.5rem;
  }
  h3 {
    font-size: 2rem;
  }
  h4 {
    font-size: 1.5rem;
  }
  h5 {
    font-size: 1.2rem;
  }
  button {
    width: max-content;
    font-family: 'Do hyeon';
    cursor: pointer;
  }
  p, div, span {
    font-family: inherit;
  }
  input:-webkit-autofill,
  input:-webkit-autofill:hover, 
  input:-webkit-autofill:focus, 
  input:-webkit-autofill:active {
    -webkit-box-shadow: 0 0 0 30px white inset !important;
    box-shadow:0 0 0 30px white inset !important;
  }

  ::-webkit-scrollbar {
    width: 8px;
    @media only screen and (max-width: 600px) {
      display: none;
    }
  }
  ::-webkit-scrollbar-track {
    background-color: var(--greyColor2);
    border-radius: 5px;
  }
  
  ::-webkit-scrollbar-thumb {
    background-color: var(--greyColor3);
    border-radius: 5px;
  }
  [placeholder]{
    text-overflow:ellipsis;
  }
  ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
      text-overflow:ellipsis;
  }
  ::-moz-placeholder { /* Firefox 19+ */
      text-overflow:ellipsis;
  }
  :-ms-input-placeholder { /* IE 10+ */
      text-overflow:ellipsis;
  }
  :-moz-placeholder { /* Firefox 18- */
      text-overflow:ellipsis;
  }
  pre {
    white-space: pre-wrap;       /* Since CSS 2.1 */
    white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
    white-space: -pre-wrap;      /* Opera 4-6 */
    white-space: -o-pre-wrap;    /* Opera 7 */
    word-wrap: break-word;
  }
}
`

const App = ({ Component, pageProps }) => {
  return (
    <>
      <Head>
        <meta charSet="UTF-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <link rel="shortcut icon" href="/favicon.ico" />
        <meta
          name="viewport"
          content="initial-scale=1.0, width=device-width, initial-scale=1"
        />
      </Head>
      <GlobalStyle />
      <Component {...pageProps} />
    </>
  )
}

App.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object
}

App.defaultProps = {
  pageProps: {}
}

export default wrapper.withRedux(App)
