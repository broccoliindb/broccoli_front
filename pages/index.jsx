import React, { useEffect, useCallback, useRef, useMemo, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import styled from 'styled-components'
import { END } from 'redux-saga'
import { useRouter } from 'next/router'
import axios from 'axios'
import {
  faHeart,
  faHistory,
  faCalendarAlt
} from '@fortawesome/free-solid-svg-icons'
import { STATUS, TYPES } from '../reducers/rootReducer'
import Layout from '../components/Layout'
import wrapper from '../store/configureStore'
import PostSummary from '../components/PostSummary'
import useIntersectionObserver from '../hooks/useIntersectionObserver'
import Loader from '../components/controls/Loader'
import ComboBox from '../components/controls/ComboBox'
import Tabs from '../components/controls/Tabs'
import { addIcon, CustomFAIcon } from '../libs/FontAwesome'
import Loading from '../components/controls/Loading'

addIcon(faCalendarAlt)

const Container = styled.div`
  padding: 1rem;
  @media only screen and (max-width: 600px) {
    padding: 1rem 0;
  }
`
const Contents = styled.ul`
  display: grid;
  grid-gap: 1rem;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
`
const Content = styled.li`
  display: grid;
  grid-template-rows: repeat(auto-fit, auto);
  padding-top: 1rem;
  padding-bottom: 2rem;
`
const Observer = styled.div`
  width: 100;
  height: 2rem;
`
const SearchOptions = styled.div`
  display: grid;
  grid-template-columns: auto auto 1fr;
  align-items: center;
`

const Home = () => {
  const dispatch = useDispatch()
  const router = useRouter()
  const { query } = router
  const { durationType, orderBy, isLoggedSuccess } = query
  const tabItems = useMemo(
    () => [
      {
        displayName: '인기순',
        icon: faHeart,
        iconColor: 'var(--redLigthColor)',
        value: 'LIKE'
      },
      {
        displayName: '최신순',
        icon: faHistory,
        iconColor: 'var(--darkVioletLigthColor)',
        value: 'DATE'
      }
    ],
    []
  )
  const comboBoxItems = useMemo(
    () => [
      { displayName: '전체', value: 'A' },
      { displayName: '이번달', value: 'M' },
      { displayName: '이번주', value: 'W' },
      { displayName: '오늘', value: 'T' }
    ],
    []
  )
  const selectedSearchDurationItem = useMemo(() => {
    const item = comboBoxItems.find((i) => i.value === durationType)
    if (item) {
      return item
    }
    return comboBoxItems[0]
  }, [durationType])
  const selectedSearchOrderbyItem = useMemo(() => {
    const item = tabItems.find((i) => i.value === orderBy)
    if (item) {
      return item
    }
    return tabItems[0]
  }, [orderBy])
  const { me } = useSelector((state) => state.user)
  const {
    posts,
    status: { getAllPostsStatus }
  } = useSelector((state) => state.post)
  const [orderbyItem, setOrderbyItem] = useState(selectedSearchOrderbyItem)
  const [durationItem, setDurationItem] = useState(selectedSearchDurationItem)
  const [isShowList, setIsShowList] = useState(false)
  const loader = useRef(false)
  const observerRef = useRef()
  const lastId = posts.length > 0 && posts[posts.length - 1].id
  const likeCount = posts.length > 0 && posts[posts.length - 1].Liker.count
  const handler = useCallback(() => {
    dispatch({
      type: TYPES.GET_ALL_POSTS_REQUEST,
      data: {
        orderBy: orderBy || 'LIKE',
        durationType: durationType || 'A',
        lastId: lastId || 0,
        likeCount: Number.isInteger(likeCount) ? likeCount : null
      }
    })
  }, [lastId, likeCount, orderBy, durationType])
  const [setNode] = useIntersectionObserver(
    {
      rootMargin: '0px',
      threshold: 0,
      lastId: lastId || 0,
      likeCount,
      durationType,
      orderBy
    },
    handler
  )

  const getPostParams = useCallback((post) => {
    let Hashtag = []
    if (post.Hashtag?.id) {
      const ids = post.Hashtag.id.split(',')
      const tags = post.Hashtag.tag.split(',')
      Hashtag = ids.map((i, index) => {
        return {
          id: i,
          tag: tags[index]
        }
      })
    }
    return {
      title: post.title,
      updatedat: post.updatedAt,
      content: post.content,
      postkey: post.postKey,
      thumbnail: post.thumbnail,
      nickname: post.User.nickname,
      avatar: post.User.avatar || post.User.defaultavatar,
      likeCount: post.Liker?.count || 0,
      commentCount: post.Comment?.count || 0,
      hashtags: Hashtag
    }
  }, [])
  const owner = useMemo(() => {
    if (me) return me
    return {
      nickname: '',
      avatar: '',
      defaultavatar: '',
      blogname: ''
    }
  }, [me])
  const onSelectedComboBoxItem = useCallback(
    (item) => () => {
      setDurationItem(item)
      const newQuery = { durationType: item.value }
      const queries = { ...query, ...newQuery }
      const q = new URLSearchParams(queries)
      router.push(`/?${q.toString()}`)
      setIsShowList(false)
    },
    [query]
  )
  const onSelectedTabItem = useCallback(
    (item) => () => {
      setOrderbyItem(item)
      const newQuery = { orderBy: item.value }
      const queries = { ...query, ...newQuery }
      const q = new URLSearchParams(queries)
      router.push(`/?${q.toString()}`)
    },
    [query]
  )
  useEffect(() => {
    if (!observerRef.current) return
    setNode(observerRef.current)
  }, [])
  useEffect(() => {
    if (isLoggedSuccess && me) {
      const returnTo = sessionStorage.getItem('returnTo')
      if (returnTo !== '/' && returnTo) {
        router.replace(returnTo)
      }
    }
  }, [isLoggedSuccess, me])
  return (
    <Layout siteOwner={owner}>
      <>
        <SearchOptions>
          <Tabs
            itemsSource={tabItems}
            selectedItem={orderbyItem}
            onSelectedItem={onSelectedTabItem}
          />
          <ComboBox
            itemsSource={comboBoxItems}
            selectedItem={durationItem}
            onSelectedItem={onSelectedComboBoxItem}
            setIsShowList={setIsShowList}
            isShowList={isShowList}
          >
            <div>
              <CustomFAIcon color="var(--greyColor5)" icon={faCalendarAlt} />
              <span style={{ marginLeft: '1rem' }}>
                {durationItem.displayName || ''}
              </span>
            </div>
          </ComboBox>
        </SearchOptions>
        <Container>
          <Contents>
            {posts.length === 0 && getAllPostsStatus === STATUS.DONE ? (
              <h2 style={{ margin: '0 1rem' }}>😢 아무 컨텐츠가 없습니다</h2>
            ) : (
              <>
                {posts.map((post) => (
                  <Content key={post.id}>
                    <PostSummary post={getPostParams(post)} />
                  </Content>
                ))}
              </>
            )}
          </Contents>
          <div style={{ height: '2rem' }}>
            <Loader isLoader={loader.current} />
          </div>
          <Observer ref={observerRef} />
        </Container>
      </>
    </Layout>
  )
}

export const getServerSideProps = wrapper.getServerSideProps(
  async (context) => {
    const cookie = context.req ? context.req.headers.cookie : ''
    axios.defaults.headers.cookie = ''
    const { orderBy, durationType, lastId, likeCount } = context.query
    if (context.req && cookie) {
      axios.defaults.headers.cookie = cookie
    }
    context.store.dispatch({
      type: TYPES.LOAD_ME_INFO_REQUEST
    })
    context.store.dispatch({
      type: TYPES.GET_ALL_POSTS_REQUEST,
      data: {
        orderBy: orderBy || 'LIKE',
        durationType: durationType || 'A',
        lastId: lastId || 0,
        likeCount: likeCount || null
      }
    })
    context.store.dispatch(END)
    await context.store.sagaTask.toPromise()
  }
)

export default Home
