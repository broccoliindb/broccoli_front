## Trouble shooting

### ❗document is not defined @document_is_not_define

> quill은 client 사이드에서 초기화된다. quill은 SSR에서의 동작매커니즘의 순서매커니즘과 맞지 않다.

```
quill : document 정의 -> react-quill 로드 -> react-quill 의 document 조작
SSR에서의 quill: react-quill 로드 ->  react-quill 의 document 조작 -> document정의 : 에러발생
```

- import는 코드의 top level에서만 작동되기때문에 require를 사용
- window 객체가 생성됬을때 참조함

2. Warning: Expected server HTML to contain a matching <div> in <div>

[참조링크](https://github.com/vercel/next.js/discussions/17443)

> Code that is only supposed to run in the browser should be executed inside useEffect. That's required because the first render should match the initial render of the server. If you manipulate that result it creates a mismatch and React won't be able to hydrate the page successfully.

- SSR에서는 반드시 첫번째 initial render가 server의 initial render와 매칭되어야하는데 quill이 dom을 조작하면 mismatch가 일어나서 hydrate가 제기능을 못할 수 있다. 따라서 useEffect 안에서 접근해라. 이건 hydration 이후에 동작한다.

3. react-dom.development.js?61bb:67 Warning: Prop `className` did not match. Server: "sc-iCfLBT" Client: "sc-gsDJrp"

[참조링크](https://github.com/vercel/next.js/tree/master/examples/with-styled-components)

4. ./node_modules/@uiw/react-md-editor/lib/esm/index.css

Quill 에디터가 마그다운기능을 하지 않아서 마크다운 에디터를 찾던중 @uiw/react-md-editor가 괜찮아보여서 시도해봤는데 아래 에러가 남.

`CSS Imported by a Dependency`

[참조링크](https://github.com/vercel/next.js/blob/master/errors/css-npm.md)

Next.js app에서 종속하고 있는 패키지가 css 파일을 로드하게 되면 이 에러가 발생한다. 요즘 일반적으로 css파일을 종속모듈속에 포함시키지 않고 사용자의 사용페이지에 따로 css를 import하는 것이 추세이다. 단지 Next.js는 노드모듈속에 css파일이 포함된 패키지라면 빌드를 지원하지 않는다.

4. window is not defined

> Next.js is universal, which means it executes code first server-side, then client-side. The window object is only present client-side, so if you absolutely need to have access to it in some React component, you should put that code in componentDidMount. This lifecycle method will only be executed on the client. You may also want to check if there isn't some alternative universal library which may suit your needs.

위에 설명은 예전 문의로부터 응답된 답이다. 요즘 hook 추세에는 맞지 않으니 필요한 부분만 이해하자면 next.js는
`it executes code first server-side, then client-side` 이말이다. 클라이언트에서만 동작하는 참조를 위한 코드는 수정될 필요가 있는데 이를위해 nextjs는

`with no ssr`이라는 기능을 갖고 있다. [참조링크](https://nextjs.org/docs/advanced-features/dynamic-import#with-no-ssr)

```js
import dynamic from 'next/dynamic'

const DynamicComponentWithNoSSR = dynamic(
  () => import('../components/hello3'),
  { ssr: false }
)

function Home() {
  return (
    <div>
      <Header />
      <DynamicComponentWithNoSSR />
      <p>HOME PAGE is here!</p>
    </div>
  )
}

export default Home
```
