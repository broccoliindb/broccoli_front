import produce from 'immer'
import { LOAD_STATUS } from './enums'

const initState = {
  isLogin: false,
  modals: {
    showLoginModal: false,
    showResetPasswordModal: false
  },
  status: {
    toastStatus: null,
    signupStatus: null,
    loginStatus: null,
    logoutStatus: null,
    loadMeInfoStatus: null,
    updateMeInfoStatus: null,
    removeAccountStatus: null,
    resetPasswordStatus: null,
    checkTokenStatus: null,
    getUserInfoStatus: null
  },
  siteOwner: {
    nickname: '',
    avatar: '',
    defaultavatar: '',
    blogname: ''
  },
  error: null,
  message: null,
  me: null
}

export const USERTYPES = {
  CLEAR_USER_ERROR: 'CLEAR_ERROR',
  CLEAR_USER_MESSAGE: 'CLEAR_MESSAGE',
  CLEAR_USER_ERR_MESSAGE: 'CLEAR_ERR_MESSAGE',

  SHOW_LOGIN_MODAL: 'SHOW_LOGIN_MODAL',
  CLOSE_LOGIN_MODAL: 'CLOSE_LOGIN_MODAL',

  SHOW_RESET_PASSWORD_MODAL: 'SHOW_RESET_PASSWORD_MODAL',
  CLOSE_RESET_PASSWORD_MODAL: 'CLOSE_RESET_PASSWORD_MODAL',

  SIGNUP_REQUEST: 'SIGNUP_REQUEST',
  SIGNUP_FULFILLED: 'SIGNUP_FULFILLED',
  SIGNUP_REJECTED: 'SIGNUP_REJECTED',

  LOGIN_REQUEST: 'LOGIN_REQUEST',
  LOGIN_FULFILLED: 'LOGIN_FULFILLED',
  LOGIN_REJECTED: 'LOGIN_REJECTED',

  LOGOUT_REQUEST: 'LOGOUT_REQUEST',
  LOGOUT_FULFILLED: 'LOGOUT_FULFILLED',
  LOGOUT_REJECTED: 'LOGOUT_REJECTED',

  LOAD_ME_INFO_REQUEST: 'LOAD_ME_INFO_REQUEST',
  LOAD_ME_INFO_FULFILLED: 'LOAD_ME_INFO_FULFILLED',
  LOAD_ME_INFO_REJECTED: 'LOAD_ME_INFO_REJECTED',

  UPDATE_ME_INFO_REQUEST: 'UPDATE_ME_INFO_REQUEST',
  UPDATE_ME_INFO_FULFILLED: 'UPDATE_ME_INFO_FULFILLED',
  UPDATE_ME_INFO_REJECTED: 'UPDATE_ME_INFO_REJECTED',

  REMOVE_ACCOUNT_REQUEST: 'REMOVE_ACCOUNT_REQUEST',
  REMOVE_ACCOUNT_FULFILLED: 'REMOVE_ACCOUNT_FULFILLED',
  REMOVE_ACCOUNT_REJECTED: 'REMOVE_ACCOUNT_REJECTED',

  RESET_PASSWORD_REQUEST: 'RESET_PASSWORD_REQUEST',
  RESET_PASSWORD_FULFILLED: 'RESET_PASSWORD_FULFILLED',
  RESET_PASSWORD_REJECTED: 'RESET_PASSWORD_REJECTED',

  CHECK_TOKEN_REQUEST: 'CHECK_TOKEN_REQUEST',
  CHECK_TOKEN_FULFILLED: 'CHECK_TOKEN_FULFILLED',
  CHECK_TOKEN_REJECTED: 'CHECK_TOKEN_REJECTED',

  GET_USER_INFO_REQUEST: 'GET_USER_INFO_REQUEST',
  GET_USER_INFO_FULFILLED: 'GET_USER_INFO_FULFILLED',
  GET_USER_INFO_REJECTED: 'GET_USER_INFO_REJECTED',

  USER_TOAST_START: 'USER_TOAST_START',
  USER_TOAST_END: 'USER_TOAST_END'
}
/* eslint-disable no-param-reassign */
const reducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case USERTYPES.SHOW_LOGIN_MODAL: {
        draft.modals.showLoginModal = true
        draft.modals.showResetPasswordModal = false
        return draft
      }
      case USERTYPES.CLOSE_LOGIN_MODAL: {
        draft.modals.showLoginModal = false
        return draft
      }
      case USERTYPES.SHOW_RESET_PASSWORD_MODAL: {
        draft.modals.showLoginModal = false
        draft.modals.showResetPasswordModal = true
        return draft
      }
      case USERTYPES.CLOSE_RESET_PASSWORD_MODAL: {
        draft.modals.showResetPasswordModal = false
        return draft
      }
      case USERTYPES.SIGNUP_REQUEST: {
        draft.status.signupStatus = LOAD_STATUS.LOADING
        return draft
      }
      case USERTYPES.SIGNUP_FULFILLED: {
        draft.status.signupStatus = LOAD_STATUS.DONE
        draft.isLogin = !!action.payload
        draft.me = action.payload
        draft.modals.showLoginModal = false
        return draft
      }
      case USERTYPES.SIGNUP_REJECTED: {
        draft.status.signupStatus = LOAD_STATUS.ERROR
        draft.isLogin = false
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case USERTYPES.LOGIN_REQUEST: {
        draft.status.logoutStatus = null
        draft.status.loginStatus = LOAD_STATUS.LOADING
        return draft
      }
      case USERTYPES.LOGIN_FULFILLED: {
        draft.status.loginStatus = LOAD_STATUS.DONE
        draft.isLogin = !!action.payload
        draft.me = action.payload
        draft.modals.showLoginModal = false
        return draft
      }
      case USERTYPES.LOGIN_REJECTED: {
        draft.status.loginStatus = LOAD_STATUS.ERROR
        draft.isLogin = false
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case USERTYPES.LOGOUT_REQUEST: {
        draft.status.loginStatus = null
        draft.status.logoutStatus = LOAD_STATUS.LOADING
        return draft
      }
      case USERTYPES.LOGOUT_FULFILLED: {
        draft.status.logoutStatus = LOAD_STATUS.DONE
        draft.isLogin = false
        draft.me = ''
        return draft
      }
      case USERTYPES.LOGOUT_REJECTED: {
        draft.status.logoutStatus = LOAD_STATUS.ERROR
        draft.isLogin = true
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case USERTYPES.LOAD_ME_INFO_REQUEST: {
        draft.status.loadMeInfoStatus = LOAD_STATUS.LOADING
        return draft
      }
      case USERTYPES.LOAD_ME_INFO_FULFILLED: {
        draft.status.loadMeInfoStatus = LOAD_STATUS.DONE
        draft.me = action.payload
        draft.isLogin = !!action.payload
        return draft
      }
      case USERTYPES.LOAD_ME_INFO_REJECTED: {
        draft.status.loadMeInfoStatus = LOAD_STATUS.ERROR
        draft.isLogin = false
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case USERTYPES.UPDATE_ME_INFO_REQUEST: {
        draft.status.updateMeInfoStatus = LOAD_STATUS.LOADING
        return draft
      }
      case USERTYPES.UPDATE_ME_INFO_FULFILLED: {
        draft.status.updateMeInfoStatus = LOAD_STATUS.DONE
        draft.me = action.payload.user
        draft.message = action.payload.message
        return draft
      }
      case USERTYPES.UPDATE_ME_INFO_REJECTED: {
        draft.status.updateMeInfoStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case USERTYPES.REMOVE_ACCOUNT_REQUEST: {
        draft.status.removeAccountStatus = LOAD_STATUS.LOADING
        return draft
      }
      case USERTYPES.REMOVE_ACCOUNT_FULFILLED: {
        draft.status.removeAccountStatus = LOAD_STATUS.DONE
        draft.me = null
        draft.isLogin = false
        draft.message = '회원 탈퇴 완료했습니다.'
        return draft
      }
      case USERTYPES.REMOVE_ACCOUNT_REJECTED: {
        draft.status.removeAccountStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case USERTYPES.RESET_PASSWORD_REQUEST: {
        draft.status.resetPasswordStatus = LOAD_STATUS.LOADING
        return draft
      }
      case USERTYPES.RESET_PASSWORD_FULFILLED: {
        draft.status.resetPasswordStatus = LOAD_STATUS.DONE
        draft.message = action.payload.message
        return draft
      }
      case USERTYPES.RESET_PASSWORD_REJECTED: {
        draft.status.resetPasswordStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case USERTYPES.CHECK_TOKEN_REQUEST: {
        draft.status.checkTokenStatus = LOAD_STATUS.LOADING
        return draft
      }
      case USERTYPES.CHECK_TOKEN_FULFILLED: {
        draft.status.checkTokenStatus = LOAD_STATUS.DONE
        draft.me = action.payload
        draft.isLogin = !!action.payload
        draft.message = '토큰확인이 되었습니다. 비밀번호를 변경해주세요'
        return draft
      }
      case USERTYPES.CHECK_TOKEN_REJECTED: {
        draft.status.checkTokenStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case USERTYPES.CLEAR_USER_ERROR: {
        draft.error = ''
        return draft
      }
      case USERTYPES.CLEAR_USER_MESSAGE: {
        draft.message = ''
        return draft
      }
      case USERTYPES.CLEAR_USER_ERR_MESSAGE: {
        draft.message = ''
        draft.error = ''
        return draft
      }
      case USERTYPES.GET_USER_INFO_REQUEST: {
        draft.status.getUserInfoStatus = LOAD_STATUS.LOADING
        return draft
      }
      case USERTYPES.GET_USER_INFO_FULFILLED: {
        draft.status.getUserInfoStatus = LOAD_STATUS.DONE
        draft.siteOwner = action.payload
        return draft
      }
      case USERTYPES.GET_USER_INFO_REJECTED: {
        draft.status.getUserInfoStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case USERTYPES.USER_TOAST_START: {
        draft.status.toastStatus = LOAD_STATUS.LOADING
        return draft
      }
      case USERTYPES.USER_TOAST_END: {
        draft.status.toastStatus = LOAD_STATUS.DONE
        draft.message = ''
        return draft
      }
      default:
        return state
    }
  })
export default reducer
