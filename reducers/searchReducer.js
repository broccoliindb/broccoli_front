import produce from 'immer'
import { LOAD_STATUS } from './enums'

const initState = {
  searchResult: {
    total: { value: 0 },
    hits: []
  },
  searchTermTokens: [],
  status: {
    searchStatus: null,
    getSearchTermTokensStatus: null
  },
  error: null,
  message: null
}

export const SEARCHTYPES = {
  SEARCH_TERM_REQUEST: 'SEARCH_TERM_REQUEST',
  SEARCH_TERM_FULFILLED: 'SEARCH_TERM_FULFILLED',
  SEARCH_TERM_REJECTED: 'SEARCH_TERM_REJECTED',
  GET_SEARCH_TERM_TOKENS_REQUEST: 'GET_SEARCH_TERM_TOKENS_REQUEST',
  GET_SEARCH_TERM_TOKENS_FULFILLED: 'GET_SEARCH_TERM_TOKENS_FULFILLED',
  GET_SEARCH_TERM_TOKENS_REJECTED: 'GET_SEARCH_TERM_TOKENS_REJECTED'
}
/* eslint-disable no-param-reassign */
const reducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SEARCHTYPES.SEARCH_TERM_REQUEST: {
        draft.status.searchStatus = LOAD_STATUS.LOADING
        return draft
      }
      case SEARCHTYPES.SEARCH_TERM_FULFILLED: {
        draft.status.searchStatus = LOAD_STATUS.DONE
        draft.searchResult = action.payload
        return draft
      }
      case SEARCHTYPES.SEARCH_TERM_REJECTED: {
        draft.status.searchStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case SEARCHTYPES.GET_SEARCH_TERM_TOKENS_REQUEST: {
        draft.status.getSearchTermTokensStatus = LOAD_STATUS.LOADING
        return draft
      }
      case SEARCHTYPES.GET_SEARCH_TERM_TOKENS_FULFILLED: {
        draft.status.getSearchTermTokensStatus = LOAD_STATUS.DONE
        draft.searchTermTokens = action.payload
        return draft
      }
      case SEARCHTYPES.GET_SEARCH_TERM_TOKENS_REJECTED: {
        draft.status.getSearchTermTokensStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      default:
        return state
    }
  })
export default reducer
