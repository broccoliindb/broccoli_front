module.exports = {
  LOAD_STATUS: {
    LOADING: 'LOADING',
    DONE: 'DONE',
    ERROR: 'ERROR',
    INIT: 'INIT'
  },
  LOAD_POSTS_TYPE: {
    IN_TEMP: 'IN_TEMP',
    IN_USE: 'IN_USE'
  }
}
