import produce from 'immer'
import { LOAD_STATUS } from './enums'

const initState = {
  status: {
    getHashtagsStatus: null
  },
  hashtags: [],
  error: null,
  message: null
}

export const HASHTAGTYPES = {
  GET_HASHTAGS_REQUEST: 'GET_HASHTAGS_REQUEST',
  GET_HASHTAGS_FULFILLED: 'GET_HASHTAGS_FULFILLED',
  GET_HASHTAGS_REJECTED: 'GET_HASHTAGS_REJECTED'
}
/* eslint-disable no-param-reassign */
const reducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case HASHTAGTYPES.GET_HASHTAGS_REQUEST: {
        draft.status.getHashtagsStatus = LOAD_STATUS.LOADING
        return draft
      }
      case HASHTAGTYPES.GET_HASHTAGS_FULFILLED: {
        draft.status.getHashtagsStatus = LOAD_STATUS.DONE
        draft.hashtags = action.payload
        return draft
      }
      case HASHTAGTYPES.GET_HASHTAGS_REJECTED: {
        draft.status.getHashtagsStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      default:
        return state
    }
  })
export default reducer
