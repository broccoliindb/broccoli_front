import produce from 'immer'
import { LOAD_STATUS } from './enums'

const initState = {
  status: {
    addSeriesStatus: null,
    getSeriesStatus: null
  },
  series: [],
  error: null,
  message: null
}

export const SERIESTYPES = {
  ADD_SERIES_REQUEST: 'ADD_SERIES_REQUEST',
  ADD_SERIES_FULFILLED: 'ADD_SERIES_FULFILLED',
  ADD_SERIES_REJECTED: 'ADD_SERIES_REJECTED',

  GET_SERIES_REQUEST: 'GET_SERIES_REQUEST',
  GET_SERIES_FULFILLED: 'GET_SERIES_FULFILLED',
  GET_SERIES_REJECTED: 'GET_SERIES_REJECTED'
}
/* eslint-disable no-param-reassign */
const reducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SERIESTYPES.ADD_SERIES_REQUEST: {
        draft.status.addSeriesStatus = LOAD_STATUS.LOADING
        return draft
      }
      case SERIESTYPES.ADD_SERIES_FULFILLED: {
        draft.status.addSeriesStatus = LOAD_STATUS.DONE
        draft.series.push(action.payload)
        return draft
      }
      case SERIESTYPES.ADD_SERIES_REJECTED: {
        draft.status.addSeriesStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case SERIESTYPES.GET_SERIES_REQUEST: {
        draft.status.getSeriesStatus = LOAD_STATUS.LOADING
        return draft
      }
      case SERIESTYPES.GET_SERIES_FULFILLED: {
        draft.status.getSeriesStatus = LOAD_STATUS.DONE
        draft.series = action.payload
        return draft
      }
      case SERIESTYPES.GET_SERIES_REJECTED: {
        draft.status.getSeriesStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      default:
        return state
    }
  })
export default reducer
