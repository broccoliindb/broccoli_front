import produce from 'immer'
import { LOAD_STATUS } from './enums'

const initState = {
  comment: {
    id: -1,
    upperId: null,
    postId: null,
    userId: null,
    comment: null
  },
  comments: [],
  status: {
    addCommentStatus: null,
    getCommentsStatus: null,
    removeCommentStatus: null,
    updateCommentStatus: null
  },
  error: null,
  message: null
}

export const COMMENTTYPES = {
  ADD_COMMENT_REQUEST: 'ADD_COMMENT_REQUEST',
  ADD_COMMENT_FULFILLED: 'ADD_COMMENT_FULFILLED',
  ADD_COMMENT_REJECTED: 'ADD_COMMENT_REJECTED',
  GET_COMMENTS_REQUEST: 'GET_COMMENTS_REQUEST',
  GET_COMMENTS_FULFILLED: 'GET_COMMENTS_FULFILLED',
  GET_COMMENTS_REJECTED: 'GET_COMMENTS_REJECTED',
  REMOVE_COMMENT_REQUEST: 'REMOVE_COMMENT_REQUEST',
  REMOVE_COMMENT_FULFILLED: 'REMOVE_COMMENT_FULFILLED',
  REMOVE_COMMENT_REJECTED: 'REMOVE_COMMENT_REJECTED',
  UPDATE_COMMENT_REQUEST: 'UPDATE_COMMENT_REQUEST',
  UPDATE_COMMENT_FULFILLED: 'UPDATE_COMMENT_FULFILLED',
  UPDATE_COMMENT_REJECTED: 'UPDATE_COMMENT_REJECTED'
}

/**
 * 리듀스에서 comments의 루프를 돌아서 적용하는 방식으로 store를 변경한다....ADD, REMOVE, MODIFY다~~
 */
/* eslint-disable no-param-reassign */
const reducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case COMMENTTYPES.ADD_COMMENT_REQUEST: {
        draft.status.addCommentStatus = LOAD_STATUS.LOADING
        return draft
      }
      case COMMENTTYPES.ADD_COMMENT_FULFILLED: {
        const index = draft.comments.findIndex(
          (i) => i.id === action.payload.id
        )
        if (index > -1) {
          draft.comments.splice(index, 1, action.payload)
        } else {
          draft.comments = [action.payload, ...draft.comments]
        }
        draft.status.addCommentStatus = LOAD_STATUS.DONE
        return draft
      }
      case COMMENTTYPES.ADD_COMMENT_REJECTED: {
        draft.status.addCommentStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case COMMENTTYPES.GET_COMMENTS_REQUEST: {
        draft.status.getCommentsStatus = LOAD_STATUS.LOADING
        return draft
      }
      case COMMENTTYPES.GET_COMMENTS_FULFILLED: {
        draft.status.getCommentsStatus = LOAD_STATUS.DONE
        draft.comments = action.payload
        return draft
      }
      case COMMENTTYPES.GET_COMMENTS_REJECTED: {
        draft.status.getCommentsStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case COMMENTTYPES.REMOVE_COMMENT_REQUEST: {
        draft.status.removeCommentStatus = LOAD_STATUS.LOADING
        return draft
      }
      case COMMENTTYPES.REMOVE_COMMENT_FULFILLED: {
        draft.status.removeCommentStatus = LOAD_STATUS.DONE
        if (action.payload) {
          const index = draft.comments.findIndex(
            (i) => i.id === action.payload.id
          )
          if (index > -1) {
            draft.comments.splice(index, 1, action.payload)
          }
        } else {
          const index = draft.comments.findIndex(
            (i) => i.id === action.input.id
          )
          if (index > -1) {
            draft.comments.splice(index, 1)
          }
        }
        return draft
      }
      case COMMENTTYPES.REMOVE_COMMENT_REJECTED: {
        draft.status.removeCommentStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case COMMENTTYPES.UPDATE_COMMENT_REQUEST: {
        draft.status.updateCommentStatus = LOAD_STATUS.LOADING
        return draft
      }
      case COMMENTTYPES.UPDATE_COMMENT_FULFILLED: {
        const index = draft.comments.findIndex(
          (i) => i.id === action.payload.id
        )
        if (index > -1) {
          draft.comments.splice(index, 1, action.payload)
        }
        draft.status.updateCommentStatus = LOAD_STATUS.DONE
        return draft
      }
      case COMMENTTYPES.UPDATE_COMMENT_REJECTED: {
        draft.status.updateCommentStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      default:
        return state
    }
  })
export default reducer
