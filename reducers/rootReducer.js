import { HYDRATE } from 'next-redux-wrapper'
import { combineReducers } from 'redux'
import post, { POSTTYPES } from './postReducer'
import user, { USERTYPES } from './userReducer'
import series, { SERIESTYPES } from './seriesReducer'
import search, { SEARCHTYPES } from './searchReducer'
import comment, { COMMENTTYPES } from './commentReducer'
import hashtag, { HASHTAGTYPES } from './hashtagReducer'
import { LOAD_STATUS } from './enums'

export const TYPES = {
  ...POSTTYPES,
  ...USERTYPES,
  ...SERIESTYPES,
  ...SEARCHTYPES,
  ...COMMENTTYPES,
  ...HASHTAGTYPES
}
export const STATUS = LOAD_STATUS

export const REDUCER = {
  POST: 'POST',
  USER: 'USER',
  SERIES: 'SERIES',
  SEARCH: 'SEARCH',
  HASHTAG: 'HASHTAG',
  COMMENT: 'COMMENT'
}

const rootReducer = (state = {}, action) => {
  switch (action.type) {
    case HYDRATE:
      return { ...state, ...action.payload }
    default: {
      const combineReducer = combineReducers({
        post,
        user,
        series,
        search,
        comment,
        hashtag
      })
      return combineReducer(state, action)
    }
  }
}
export default rootReducer
