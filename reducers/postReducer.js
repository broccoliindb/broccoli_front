import produce from 'immer'
import { LOAD_STATUS } from './enums'

const initState = {
  status: {
    toastStatus: null,
    addPostStatus: null,
    removePostStatus: null,
    getPostsStatus: null,
    getPostStatus: null,
    getTempPostStatus: null,
    tempSaveStatus: null,
    getAllPostsStatus: null,
    likePostStatus: null,
    unlikePostStatus: null,
    getAllPostsOnSeriesStatus: null,
    getAllPostsOnHashStatus: null,
    getAllSeriesOnPostsStatus: null,
    getPostMetaListOnSeriesStatus: null,
    addHashTagStatus: null
  },
  series: [],
  error: null,
  message: null,
  posts: [],
  postsOnSeries: [],
  mainPost: {
    id: -1,
    title: '',
    postKey: '',
    content: '',
    markdown: '',
    updatedAt: '',
    Liker: [],
    Comments: [],
    Hashtags: []
  }
}

export const POSTTYPES = {
  REMOVE_POST_REQUEST: 'REMOVE_POST_REQUEST',
  REMOVE_POST_FULFILLED: 'REMOVE_POST_FULFILLED',
  REMOVE_POST_REJECTED: 'REMOVE_POST_REJECTED',

  ADD_POST_REQUEST: 'ADD_POST_REQUEST',
  ADD_POST_FULFILLED: 'ADD_POST_FULFILLED',
  ADD_POST_REJECTED: 'ADD_POST_REJECTED',

  GET_POSTS_REQUEST: 'GET_POSTS_REQUEST',
  GET_POSTS_FULFILLED: 'GET_POSTS_FULFILLED',
  GET_POSTS_REJECTED: 'GET_POSTS_REJECTED',

  GET_ALL_SERIES_ON_POSTS_REQUEST: 'GET_ALL_SERIES_ON_POSTS_REQUEST',
  GET_ALL_SERIES_ON_POSTS_FULFILLED: 'GET_ALL_SERIES_ON_POSTS_FULFILLED',
  GET_ALL_SERIES_ON_POSTS_REJECTED: 'GET_ALL_SERIES_ON_POSTS_REJECTED',

  GET_ALL_POSTS_ON_SERIES_REQUEST: 'GET_ALL_POSTS_ON_SERIES_REQUEST',
  GET_ALL_POSTS_ON_SERIES_FULFILLED: 'GET_ALL_POSTS_ON_SERIES_FULFILLED',
  GET_ALL_POSTS_ON_SERIES_REJECTED: 'GET_ALL_POSTS_ON_SERIES_REJECTED',

  GET_ALL_POSTS_ON_HASH_REQUEST: 'GET_ALL_POSTS_ON_HASH_REQUEST',
  GET_ALL_POSTS_ON_HASH_FULFILLED: 'GET_ALL_POSTS_ON_HASH_FULFILLED',
  GET_ALL_POSTS_ON_HASH_REJECTED: 'GET_ALL_POSTS_ON_HASH_REJECTED',

  GET_POST_META_LIST_ON_SERIES_REQUEST: 'GET_POST_META_LIST_ON_SERIES_REQUEST',
  GET_POST_META_LIST_ON_SERIES_FULFILLED:
    'GET_POST_META_LIST_ON_SERIES_FULFILLED',
  GET_POST_META_LIST_ON_SERIES_REJECTED:
    'GET_POST_META_LIST_ON_SERIES_REJECTED',

  GET_POST_REQUEST: 'GET_POST_REQUEST',
  GET_POST_FULFILLED: 'GET_POST_FULFILLED',
  GET_POST_REJECTED: 'GET_POST_REJECTED',

  GET_TEMP_POST_REQUEST: 'GET_TEMP_POST_REQUEST',
  GET_TEMP_POST_FULFILLED: 'GET_TEMP_POST_FULFILLED',
  GET_TEMP_POST_REJECTED: 'GET_TEMP_POST_REJECTED',

  TEMP_SAVE_POST_REQUEST: 'TEMP_SAVE_POST_REQUEST',
  TEMP_SAVE_POST_FULFILLED: 'TEMP_SAVE_POST_FULFILLED',
  TEMP_SAVE_POST_REJECTED: 'TEMP_SAVE_POST_REJECTED',

  GET_ALL_POSTS_REQUEST: 'GET_ALL_POSTS_REQUEST',
  GET_ALL_POSTS_FULFILLED: 'GET_ALL_POSTS_FULFILLED',
  GET_ALL_POSTS_REJECTED: 'GET_ALL_POSTS_REJECTED',

  LIKE_POST_REQUREST: 'LIKE_POST_REQUREST',
  LIKE_POST_FULFILLED: 'LIKE_POST_FULFILLED',
  LIKE_POST_REJECTED: 'LIKE_POST_REJECTED',

  UNLIKE_POST_REQUREST: 'UNLIKE_POST_REQUREST',
  UNLIKE_POST_FULFILLED: 'UNLIKE_POST_FULFILLED',
  UNLIKE_POST_REJECTED: 'UNLIKE_POST_REJECTED',

  POST_TOAST_START: 'POST_TOAST_START',
  POST_TOAST_END: 'POST_TOAST_END',

  // 태그 20자 이상입력시 에러용
  ADD_HASHTAG_REJECTED: 'ADD_HASHTAG_REJECTED'
}
/* eslint-disable no-param-reassign */
const reducer = (state = initState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case POSTTYPES.REMOVE_POST_REQUEST: {
        draft.status.removePostStatus = LOAD_STATUS.LOADING
        return draft
      }
      case POSTTYPES.REMOVE_POST_FULFILLED: {
        draft.status.removePostStatus = LOAD_STATUS.DONE
        draft.mainPost = initState.mainPost
        draft.message = action.payload.message
        return draft
      }
      case POSTTYPES.REMOVE_POST_REJECTED: {
        draft.status.removePostStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case POSTTYPES.ADD_POST_REQUEST: {
        draft.status.addPostStatus = LOAD_STATUS.LOADING
        draft.status.tempSaveStatus = LOAD_STATUS.INIT
        draft.status.getTempPostStatus = LOAD_STATUS.INIT
        draft.status.getPostStatus = LOAD_STATUS.INIT
        return draft
      }
      case POSTTYPES.ADD_POST_FULFILLED: {
        draft.status.addPostStatus = LOAD_STATUS.DONE
        draft.mainPost = action.payload
        return draft
      }
      case POSTTYPES.ADD_POST_REJECTED: {
        draft.status.addPostStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case POSTTYPES.GET_POSTS_REQUEST: {
        draft.status.getPostsStatus = LOAD_STATUS.LOADING
        return draft
      }
      case POSTTYPES.GET_POSTS_FULFILLED: {
        const { results, message } = action.payload
        draft.status.getPostsStatus = LOAD_STATUS.DONE
        draft.posts = [...draft.posts, ...results]
        draft.message = message
        return draft
      }
      case POSTTYPES.GET_POSTS_REJECTED: {
        draft.status.getPostsStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case POSTTYPES.GET_ALL_SERIES_ON_POSTS_REQUEST: {
        draft.status.getAllSeriesOnPostsStatus = LOAD_STATUS.LOADING
        return draft
      }
      case POSTTYPES.GET_ALL_SERIES_ON_POSTS_FULFILLED: {
        draft.status.getAllSeriesOnPostsStatus = LOAD_STATUS.DONE
        draft.postsOnSeries = [...draft.postsOnSeries, ...action.payload]
        return draft
      }
      case POSTTYPES.GET_ALL_SERIES_ON_POSTS_REJECTED: {
        draft.status.getAllSeriesOnPostsStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case POSTTYPES.GET_ALL_POSTS_ON_SERIES_REQUEST: {
        draft.status.getAllPostsOnSeriesStatus = LOAD_STATUS.LOADING
        return draft
      }
      case POSTTYPES.GET_ALL_POSTS_ON_SERIES_FULFILLED: {
        draft.status.getAllPostsOnSeriesStatus = LOAD_STATUS.DONE
        draft.posts = [...draft.posts, ...action.payload]
        return draft
      }
      case POSTTYPES.GET_ALL_POSTS_ON_SERIES_REJECTED: {
        draft.status.getAllPostsOnSeriesStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case POSTTYPES.GET_ALL_POSTS_ON_HASH_REQUEST: {
        draft.status.getAllPostsOnHashStatus = LOAD_STATUS.LOADING
        return draft
      }
      case POSTTYPES.GET_ALL_POSTS_ON_HASH_FULFILLED: {
        draft.status.getAllPostsOnHashStatus = LOAD_STATUS.DONE
        draft.posts = [...draft.posts, ...action.payload]
        return draft
      }
      case POSTTYPES.GET_ALL_POSTS_ON_HASH_REJECTED: {
        draft.status.getAllPostsOnHashStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case POSTTYPES.GET_POST_META_LIST_ON_SERIES_REQUEST: {
        draft.status.getPostMetaListOnSeriesStatus = LOAD_STATUS.LOADING
        return draft
      }
      case POSTTYPES.GET_POST_META_LIST_ON_SERIES_FULFILLED: {
        draft.status.getPostMetaListOnSeriesStatus = LOAD_STATUS.DONE
        draft.postsOnSeries = [...draft.postsOnSeries, ...action.payload]
        return draft
      }
      case POSTTYPES.GET_POST_META_LIST_ON_SERIES_REJECTED: {
        draft.status.getPostMetaListOnSeriesStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case POSTTYPES.GET_POST_REQUEST: {
        draft.status.addPostStatus = LOAD_STATUS.INIT
        draft.status.tempSaveStatus = LOAD_STATUS.INIT
        draft.status.getTempPostStatus = LOAD_STATUS.INIT
        draft.status.getPostStatus = LOAD_STATUS.LOADING
        return draft
      }
      case POSTTYPES.GET_POST_FULFILLED: {
        draft.status.getPostStatus = LOAD_STATUS.DONE
        draft.mainPost = action.payload ? action.payload : initState.mainPost
        return draft
      }
      case POSTTYPES.GET_POST_REJECTED: {
        draft.status.getPostStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case POSTTYPES.GET_TEMP_POST_REQUEST: {
        draft.status.addPostStatus = LOAD_STATUS.INIT
        draft.status.tempSaveStatus = LOAD_STATUS.INIT
        draft.status.getTempPostStatus = LOAD_STATUS.LOADING
        draft.status.getPostStatus = LOAD_STATUS.INIT
        return draft
      }
      case POSTTYPES.GET_TEMP_POST_FULFILLED: {
        draft.status.getTempPostStatus = LOAD_STATUS.DONE
        draft.mainPost = action.payload ? action.payload : initState.mainPost
        return draft
      }
      case POSTTYPES.GET_TEMP_POST_REJECTED: {
        draft.status.getTempPostStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case POSTTYPES.TEMP_SAVE_POST_REQUEST: {
        draft.status.addPostStatus = LOAD_STATUS.INIT
        draft.status.tempSaveStatus = LOAD_STATUS.LOADING
        draft.status.getTempPostStatus = LOAD_STATUS.INIT
        draft.status.getPostStatus = LOAD_STATUS.INIT
        return draft
      }
      case POSTTYPES.TEMP_SAVE_POST_FULFILLED: {
        draft.status.tempSaveStatus = LOAD_STATUS.DONE
        draft.message = '임시저장되었습니다.'
        draft.mainPost = action.payload ? action.payload : initState.mainPost
        return draft
      }
      case POSTTYPES.TEMP_SAVE_POST_REJECTED: {
        draft.status.tempSaveStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case POSTTYPES.GET_ALL_POSTS_REQUEST: {
        draft.status.getAllPostsStatus = LOAD_STATUS.LOADING
        return draft
      }
      case POSTTYPES.GET_ALL_POSTS_FULFILLED: {
        draft.status.getAllPostsStatus = LOAD_STATUS.DONE
        draft.posts = [...draft.posts, ...action.payload]
        return draft
      }
      case POSTTYPES.GET_ALL_POSTS_REJECTED: {
        draft.status.getAllPostsStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case POSTTYPES.LIKE_POST_REQUREST: {
        draft.status.likePostStatus = LOAD_STATUS.LOADING
        return draft
      }
      case POSTTYPES.LIKE_POST_FULFILLED: {
        draft.status.likePostStatus = LOAD_STATUS.DONE
        draft.mainPost.Liker.push({
          id: action.payload.userId
        })
        return draft
      }
      case POSTTYPES.LIKE_POST_REJECTED: {
        draft.status.likePostStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case POSTTYPES.UNLIKE_POST_REQUREST: {
        draft.status.unlikePostStatus = LOAD_STATUS.LOADING
        return draft
      }
      case POSTTYPES.UNLIKE_POST_FULFILLED: {
        draft.status.unlikePostStatus = LOAD_STATUS.DONE
        draft.mainPost.Liker = draft.mainPost.Liker.filter(
          (i) => i.id !== action.payload.userId
        )
        return draft
      }
      case POSTTYPES.UNLIKE_POST_REJECTED: {
        draft.status.unlikePostStatus = LOAD_STATUS.ERROR
        draft.error = action.error.stack
        draft.message = action.error.message
        return draft
      }
      case POSTTYPES.POST_TOAST_START: {
        draft.status.toastStatus = LOAD_STATUS.LOADING
        return draft
      }
      case POSTTYPES.POST_TOAST_END: {
        draft.status.toastStatus = LOAD_STATUS.DONE
        draft.message = ''
        return draft
      }
      case POSTTYPES.ADD_HASHTAG_REJECTED: {
        draft.status.addHashTagStatus = LOAD_STATUS.ERROR
        draft.message = action.error.message
        return draft
      }
      default:
        return state
    }
  })
export default reducer
