import React, { useCallback } from 'react'
import styled from 'styled-components'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import Link from 'next/link'
import PropTypes from 'prop-types'
import GridBox from './controls/GridBox'
import Button from './controls/Button'

const Container = styled.div`
  overflow: auto;
  padding: 4.5rem 2rem 1rem 1rem;
  margin: 2rem 0 1rem 1rem;
  @media only screen and (max-width: 1199px) {
    visibility: collapse;
    display: none;
  }
  @media only screen and (min-width: 1200px) {
    grid-column: 2 / 6;
    grid-row: 1/-1;
  }
`
const ButtonWrapper = styled(Button)`
  padding: 0.5rem;
  &:hover {
    background-color: var(--everGreenColor);
    color: var(--whiteColor);
  }
`
const GridBoxWrapper = styled(GridBox)`
  grid-template-columns: 1fr;
  grid-template-rows: repeat(auto-fill, 2rem);
  grid-auto-rows: 2rem;
  grid-gap: 0.5rem;
`

const Sidebar = ({ isMyDocs }) => {
  const router = useRouter()
  const { query } = router
  const user = query?.user
  const hashtags = useSelector((state) => state.hashtag.hashtags)
  const getTag = useCallback(
    (tag) => {
      if (!tag) return ''
      if (tag.length > 16) {
        return `${tag.substring(0, 14)}...`
      }
      return tag
    },
    [hashtags]
  )
  return (
    <>
      {isMyDocs && (
        <Container>
          <h3>태그목록</h3>
          {hashtags && (
            <GridBoxWrapper>
              {hashtags.map((tag) => (
                <Link
                  key={tag.hashtagId}
                  href={`/${user}/hash?value=${encodeURIComponent(
                    tag.hashtag
                  )}`}
                >
                  <a>
                    <ButtonWrapper>
                      {`${getTag(tag.hashtag)} (${tag.count})`}
                    </ButtonWrapper>
                  </a>
                </Link>
              ))}
            </GridBoxWrapper>
          )}
        </Container>
      )}
    </>
  )
}

Sidebar.propTypes = {
  isMyDocs: PropTypes.bool.isRequired
}
export default Sidebar
