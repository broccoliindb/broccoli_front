import React, { useCallback, useState } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import Link from 'next/link'
import Button from './controls/Button'
import GridBox from './controls/GridBox'
import Avatar from './controls/Avatar'
import Comment from './Comment'
import useInput from '../hooks/useInput'
import { TYPES } from '../reducers/rootReducer'
import getLightTime from '../libs/CustomDate'

const Container = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
`
const ContainerGridBox = styled(GridBox)`
  background-color: var(--greyColor2);
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: repeat(4, auto);
  grid-gap: 0.2rem;
`
const HeaderGridBox = styled(GridBox)`
  background-color: var(--greyColor3);
  display: grid;
  grid-template-columns: 1fr auto;
  div,
  button {
    align-self: center;
  }
`
const ButtonGridBox = styled(GridBox)`
  grid-template-columns: 1fr auto;
  margin: 0;
  button {
    justify-self: end;
  }
  @media only screen and (max-width: 600px) {
    button {
      width: max-content;
    }
  }
`
const ButtonWrapper = styled(Button)`
  padding: 0.2rem;
  width: 100%;
  background-color: var(--greyColor4);
  color: var(--whiteColor);
  font-size: 1rem;
  &:hover {
    background-color: var(--everGreenColor);
  }
`
const User = styled.div`
  padding: 0.5rem;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  &:hover {
    cursor: pointer;
  }
  @media only screen and (max-width: 600px) {
    &:hover {
      background-color: transparent;
    }
  }
`

const Reply = ({
  id,
  lvl,
  postId,
  writer,
  writerId,
  content,
  writtenDate,
  avatar,
  childComments,
  isLoggedIn,
  onLoadRemoveMessageBox
}) => {
  const [mainContent, setMainContent] = useState(content)
  const { me } = useSelector((state) => state.user)
  const loginId = me?.id
  const onChangeMainContent = useCallback(
    (e) => {
      if (isLoggedIn) {
        setMainContent(e.target.value)
      }
    },
    [isLoggedIn]
  )
  const dispatch = useDispatch()
  const [isReplyOn, setIsReplyOn] = useState(false)
  const [isModifyReply, setIsModifyReply] = useState(false)
  const onReplyOn = useCallback(() => {
    setIsReplyOn(true)
  }, [])
  const [innerContent, onChangeInnerContent, setInnerContent] = useInput('')
  const onModifyReply = useCallback(() => {
    if (!isLoggedIn) return
    setIsModifyReply(true)
  }, [])
  const onCancelComment = useCallback(
    (type) => () => {
      if (type === 'UPDATE') {
        setIsModifyReply(false)
      }
      if (type === 'REPLY') {
        setIsReplyOn(false)
      }
    },
    []
  )
  const onSaveComment = useCallback(() => {
    dispatch({
      type: TYPES.ADD_COMMENT_REQUEST,
      data: { postId, content: innerContent, upperId: id }
    })
    setIsReplyOn(false)
    setInnerContent('')
  }, [innerContent])
  const onUpdateComment = useCallback(() => {
    dispatch({
      type: TYPES.UPDATE_COMMENT_REQUEST,
      data: { content: mainContent, id }
    })
    setIsModifyReply(false)
  }, [mainContent])
  return (
    <ContainerGridBox level={lvl}>
      <HeaderGridBox>
        <User>
          <Link href={`/@${writer}`}>
            <>
              <Avatar width={25} height={25} imgPath={avatar} />
              <GridBox
                style={{
                  gridTemplateColumns: '1fr',
                  gridTemplateRows: '1fr 1fr',
                  gridGap: '0.2rem',
                  marginLeft: '0.2rem'
                }}
              >
                <span>{writer}</span>
                <span>{getLightTime(writtenDate)}</span>
              </GridBox>
            </>
          </Link>
        </User>
        <>
          {loginId && writerId === loginId && (
            <ButtonGridBox>
              <Button
                style={{ color: 'var(--everGreenColor)', padding: '0.5rem' }}
                isUnderLine
                onClick={onModifyReply}
              >
                수정
              </Button>
              {childComments && childComments.length === 0 && (
                <Button
                  style={{ color: 'var(--everGreenColor)', padding: '0.5rem' }}
                  isUnderLine
                  onClick={onLoadRemoveMessageBox(id, childComments.length)}
                >
                  삭제
                </Button>
              )}
            </ButtonGridBox>
          )}
        </>
      </HeaderGridBox>
      {isModifyReply && (
        <Comment
          isLoggedIn={isLoggedIn}
          isUpdate
          lvl={lvl}
          content={mainContent}
          onCancelComment={onCancelComment('UPDATE')}
          onChangeContent={onChangeMainContent}
          onSaveComment={onUpdateComment}
        />
      )}
      {!isModifyReply && <div style={{ margin: '0.5rem' }}>{mainContent}</div>}
      {!isReplyOn && childComments && childComments.length > 0 && (
        <ButtonWrapper onClick={onReplyOn}>댓글쓰기</ButtonWrapper>
      )}
      {!isReplyOn && childComments && childComments.length === 0 && (
        <Button isUnderLine onClick={onReplyOn}>
          답글달기
        </Button>
      )}
      {isReplyOn && (
        <ButtonWrapper onClick={onCancelComment('REPLY')}>취소</ButtonWrapper>
      )}
      {isReplyOn && (
        <Container>
          └
          <Comment
            isLoggedIn={isLoggedIn}
            lvl={lvl}
            content={innerContent}
            onChangeContent={onChangeInnerContent}
            onSaveComment={onSaveComment}
          />
        </Container>
      )}
      {childComments &&
        childComments.map((child) => (
          <Container key={child.id}>
            └
            <Reply
              id={child.id}
              lvl={child.lvl}
              postId={postId}
              upperId={child.upperId}
              writer={child.nickname}
              writerId={child.userId}
              avatar={child.avatar || child.defaultavatar}
              content={child.content}
              childComments={child.childComments}
              writtenDate={child.updatedAt}
              isLoggedIn={isLoggedIn}
              onLoadRemoveMessageBox={onLoadRemoveMessageBox}
            />
          </Container>
        ))}
    </ContainerGridBox>
  )
}

Reply.propTypes = {
  id: PropTypes.number.isRequired,
  lvl: PropTypes.number.isRequired,
  postId: PropTypes.number.isRequired,
  writer: PropTypes.string.isRequired,
  writerId: PropTypes.number.isRequired,
  content: PropTypes.string,
  writtenDate: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  childComments: PropTypes.array,
  isLoggedIn: PropTypes.bool.isRequired,
  onLoadRemoveMessageBox: PropTypes.func.isRequired
}

Reply.defaultProps = {
  content: '',
  childComments: []
}

export default Reply
