import React, { useCallback, useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import { TYPES } from '../reducers/rootReducer'
import Input from './controls/Input'
import Button from './controls/Button'
import useDebounce from '../hooks/useDebounce'

const Container = styled.div`
  display: grid;
  background-color: var(--greyColor3);
  grid-template-rows: auto 1fr;
  margin-bottom: 1rem;
`
const Content = styled.div`
  margin: 0.5rem;
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 0.3rem;
`
const SeriesList = styled.ul`
  height: 10rem;
  overflow-y: auto;
`
const SeriesItem = styled.li`
  cursor: pointer;
  padding: 0.5rem;
  border-bottom: 1px solid var(--greyColor4);
  background-color: ${(props) =>
    props.selected ? 'var(--everGreenColor)' : 'var(--whiteColor)'};
  color: ${(props) =>
    props.selected ? 'var(--whiteColor)' : 'var(--blackColor)'};
`

const SeriesAdder = ({ onSelectSeriesItem, selected, series }) => {
  const { nickname, id } = useSelector((state) => state.user.me)
  const dispatch = useDispatch()
  const [value, setValue] = useState('')
  const [seriesPath, setSeriesPath] = useState(`/@${nickname}/series/`)
  const [isFocused, setFocus] = useState(false)
  const onAddSeries = useCallback(() => {
    if (!value) return
    dispatch({
      type: TYPES.ADD_SERIES_REQUEST,
      data: { name: value, userId: id }
    })
  }, [value])
  const onFocusSeriesName = useCallback(() => {
    setFocus(true)
  }, [])
  const onChangeValue = useCallback((e) => {
    setValue(e.target.value)
    setSeriesPath(`/@${nickname}/series/${e.target.value}`)
  }, [])
  const onCancelFocus = useCallback(() => {
    setFocus(false)
    setValue('')
    setSeriesPath(`/@${nickname}/series/`)
  }, [])
  const addSeries = useDebounce(onAddSeries, 300)
  useEffect(() => {
    if (selected) {
      setFocus(false)
      setValue('')
      setSeriesPath(`/@${nickname}/series/`)
    }
  }, [selected])
  return (
    <Container>
      <Content>
        <Input
          onFocus={onFocusSeriesName}
          placeholder="새로운 시리즈 이름을 입력하세요"
          value={value}
          onChange={onChangeValue}
        />
        {isFocused && (
          <Input
            style={{ marginBottom: '0.5rem' }}
            value={seriesPath}
            readonly
          />
        )}
        {isFocused && (
          <div style={{ textAlign: 'right' }}>
            <Button
              style={{
                marginLeft: '0.5rem',
                backgroundColor: 'var(--blackColor)',
                color: 'var(--whiteColor)',
                padding: '0.5rem',
                borderRadius: '5px',
                width: 'max-content'
              }}
              onClick={onCancelFocus}
            >
              취소
            </Button>
            <Button
              disabled={!value}
              style={{
                marginLeft: '0.5rem',
                backgroundColor: 'var(--everGreenColor)',
                color: 'var(--whiteColor)',
                padding: '0.5rem',
                borderRadius: '5px',
                width: 'max-content'
              }}
              onClick={addSeries}
            >
              시리즈추가
            </Button>
          </div>
        )}
      </Content>
      {series && series.length > 0 && (
        <SeriesList>
          {series.map((item) => (
            <SeriesItem
              selected={selected && selected.id === item.id}
              onClick={onSelectSeriesItem(item)}
              key={item.id}
            >
              {item.name}
            </SeriesItem>
          ))}
        </SeriesList>
      )}
    </Container>
  )
}

SeriesAdder.propTypes = {
  onSelectSeriesItem: PropTypes.func.isRequired,
  selected: PropTypes.shape({
    id: PropTypes.number.isRequired
  }).isRequired,
  series: PropTypes.array.isRequired
}

SeriesAdder.defaultProps = {}

export default SeriesAdder
