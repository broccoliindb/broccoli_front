import React, { useCallback } from 'react'
import Image from 'next/image'
import { useRouter } from 'next/router'
import styled from 'styled-components'
import Link from 'next/link'
import PropTypes from 'prop-types'
import { useSelector } from 'react-redux'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import MyMenu from './MyMenu'
import { addIcon, CustomFAIcon } from '../libs/FontAwesome'
import Button from './controls/Button'
import useDebounce from '../hooks/useDebounce'

addIcon(faSearch)

const Container = styled.header`
  grid-column: 1 / -1;
  grid-gap: 0.5rem;
  display: grid;
  grid-template-columns: auto 1fr auto auto;
  padding: 1rem;
  place-items: center;
  box-shadow: rgb(0 0 0 / 10%) 0px 2px 5px;
  position: fixed;
  width: 100%;
  background-color: var(--whiteColor);
  opacity: ${(props) => (props.isHeaderShow ? '1' : '0')};
  transform: ${(props) => (props.isHeaderShow ? '0' : 'translateY(-4.5rem)')};
  transition: opacity 0.1s linear, transform 0.2s ease-in-out;
  z-index: 1;
`
const Header = ({ isHeaderShow, siteOwner }) => {
  const router = useRouter()
  const { pathname } = router
  const { me } = useSelector((state) => state.user)
  const nickname = siteOwner?.nickname
  const blogname = siteOwner?.blogname
  const getBlogname = useCallback(() => {
    let displayname = blogname || (nickname && `${nickname}.log`)
    if ((!nickname && !me) || pathname === '/') {
      displayname = '모든기록지'
    }
    const linkPath = nickname ? `/@${nickname}` : '/'
    return (
      <Link href={linkPath}>
        <a
          style={{
            fontFamily: 'do hyeon',
            fontSize: '1.5rem',
            verticalAlign: 'middel',
            justifySelf: 'start'
          }}
        >
          {displayname}
        </a>
      </Link>
    )
  }, [blogname, nickname])
  const onSearchClick = useCallback(() => {
    if (!nickname) {
      return router.replace('/search')
    }
    router.replace(`/search?username=${nickname}`)
  }, [nickname])
  const searchClick = useDebounce(onSearchClick, 300)
  return (
    <Container isHeaderShow={isHeaderShow}>
      <Link href="/">
        <a>
          <Image src="/favicon-32x32.png" width={32} height={32} alt="logo" />
        </a>
      </Link>
      {getBlogname()}
      <Button
        padding="0 0.5rem 0 0"
        style={{
          justifySelf: 'end',
          display: 'grid',
          fontSize: '1.5rem',
          padding: '0.5rem',
          borderRadius: '50%'
        }}
      >
        <CustomFAIcon
          style={{
            width: '1.2rem',
            height: '1.2rem',
            justifySelf: 'end'
          }}
          icon={faSearch}
          onClick={searchClick}
        />
      </Button>
      <MyMenu />
    </Container>
  )
}

Header.propTypes = {
  isHeaderShow: PropTypes.bool.isRequired,
  siteOwner: PropTypes.shape({
    nickname: PropTypes.string,
    avatar: PropTypes.string,
    defaultavatar: PropTypes.string,
    blogname: PropTypes.string
  })
}

Header.defaultProps = {
  siteOwner: ''
}

export default Header
