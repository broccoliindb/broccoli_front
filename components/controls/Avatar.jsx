import React from 'react'
import Image from 'next/image'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const ImageS = styled(Image)`
  border-radius: 50%;
  margin-left: auto;
  margin-right: auto;
`
const Avatar = ({ width, height, imgPath, ...props }) => {
  return <ImageS width={width} height={height} src={imgPath} {...props} />
}

Avatar.propTypes = {
  imgPath: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired
}

export default Avatar
