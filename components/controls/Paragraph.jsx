import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const CustomParagraph = styled.div`
  word-wrap: break-word;
  line-height: 1.5rem;
  font-style: ${(props) => (props.isDefault ? 'italic' : 'normal')};
  font-family: ${(props) => props.fontFamily || 'Noto Serif KR'};
  color: ${(props) =>
    props.isDefault ? 'var(--everGreenColor)' : 'var(--blackColor)'};
`

const Paragraph = (props) => {
  const { style } = props
  return <CustomParagraph style={style} {...props} />
}

Paragraph.propTypes = {
  style: PropTypes.object
}

Paragraph.defaultProps = {
  style: {}
}
export default Paragraph
