import React, { useState, useCallback, useEffect } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const Container = styled.div`
  outline: none;
  border-radius: 10px;
  width: 2rem;
  height: 1rem;
  padding: 0 0.1rem;
  background-color: ${(props) =>
    props.isSelected ? 'var(--greyColor4)' : 'var(--everGreenColor)'};
  display: flex;
  justify-content: ${(props) => (props.isSelected ? 'flex-start' : 'flex-end')};
  align-items: center;
  cursor: pointer;
  @media only screen and (max-width: 600px) {
  }
`
const Switch = styled.div`
  border-radius: 50%;
  width: 0.8rem;
  height: 0.8rem;
  background-color: var(--whiteColor);
  border: none;
`

const SwitchButton = (props) => {
  const { style, onClick, isSelected, ...customProps } = props
  return (
    <Container
      style={style}
      onClick={onClick}
      isSelected={isSelected || false}
      {...customProps}
    >
      <Switch />
    </Container>
  )
}

SwitchButton.propTypes = {
  style: PropTypes.object,
  onClick: PropTypes.func.isRequired,
  isSelected: PropTypes.bool.isRequired
}

SwitchButton.defaultProps = {
  style: {}
}
export default SwitchButton
