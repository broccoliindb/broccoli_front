import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const Container = styled.div`
  display: flex;
  margin-bottom: 1rem;
  flex-direction: ${(props) =>
    props.flexDirection ? props.flexDirection : 'row'};
`

const FlexBox = (props) => {
  const { style } = props
  return <Container {...props} style={style} />
}

FlexBox.propTypes = {
  style: PropTypes.object
}

FlexBox.defaultProps = {
  style: {}
}

export default FlexBox
