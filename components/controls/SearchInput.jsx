import React, { memo, useMemo, forwardRef } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { faTimes, faSearch } from '@fortawesome/free-solid-svg-icons'
import { addIcon, CustomFAIcon } from '../../libs/FontAwesome'

addIcon(faTimes)
addIcon(faSearch)

const Container = styled.div`
  display: grid;
  grid-template-columns: 27px 1fr 27px;
  place-items: center;
  border: 1px solid
    ${(props) =>
      props.isFocus ? 'var(--everGreenColor)' : 'var(--greyColor3)'};
  border-radius: ${(props) => (props.isInputButton ? '5px 0 0 5px' : '5px')};
  margin: 0;
  padding: 0 0.5rem;
  svg {
    &:hover {
      opacity: 1;
    }
  }
`
const CustomInput = styled.input`
  width: 100%;
  border: none;
  padding: 0.5rem 0;
  font-size: ${(props) => (props.isHeader ? '3rem' : '1rem')};
  ::placeholder {
    color: var(--greyColor4);
    font-weight: normal;
  }
  @media only screen and (max-width: 600px) {
    font-size: ${(props) => (props.isHeader ? '2rem' : '1rem')};
  }
`

const SearchInput = forwardRef((props, ref) => {
  const {
    isFocus,
    value,
    onChange,
    placeholder,
    style,
    isClear,
    onClear,
    isHeader,
    isInputButton,
    onFocus,
    onClick,
    ...commonProps
  } = props
  const getColor = useMemo(() => {
    if (isFocus) {
      return 'var(--everGreenColor)'
    }
    return 'var(--greyColor3)'
  }, [isFocus])
  return (
    <Container
      onClick={onClick}
      onFocus={onFocus}
      isInputButton={isInputButton}
      isFocus={isFocus}
      style={style}
    >
      <CustomFAIcon icon={faSearch} color={getColor} />
      <CustomInput
        ref={ref}
        type="text"
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        isHeader={isHeader}
        {...commonProps}
      />
      {isClear && value && (
        <CustomFAIcon
          onClick={onClear}
          icon={faTimes}
          opacity="0.5"
          style={{
            cursor: 'pointer',
            color: 'var(--redLigthColor)',
            marginLeft: '16px'
          }}
        />
      )}
    </Container>
  )
})

SearchInput.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  style: PropTypes.object,
  placeholder: PropTypes.string,
  isClear: PropTypes.bool,
  onClear: PropTypes.func,
  isHeader: PropTypes.bool,
  isInputButton: PropTypes.bool,
  isFocus: PropTypes.bool.isRequired,
  onFocus: PropTypes.func,
  onClick: PropTypes.func
}

SearchInput.defaultProps = {
  value: '',
  onChange: () => {},
  style: {},
  placeholder: '',
  isClear: false,
  onClear: () => {},
  isHeader: false,
  isInputButton: false,
  onFocus: () => {},
  onClick: () => {}
}

export default memo(SearchInput)
