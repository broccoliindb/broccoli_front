import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { addIcon, CustomFAIcon } from '../../libs/FontAwesome'

addIcon(faTimes)

const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr 27px;
  place-items: center;
  border: ${(props) =>
    props.isBorder ? '1px solid var(--greyColor3)' : 'none'};
  border-radius: ${(props) => (props.isInputButton ? '5px 0 0 5px' : '5px')};
  svg {
    &:hover {
      opacity: 1;
    }
  }
`
const CustomInput = styled.input`
  font-family: 'sans-serif';
  width: 100%;
  border: none;
  font-weight: bold;
  font-size: ${(props) => (props.isHeader ? '3rem' : '1rem')};
  border-radius: ${(props) => (props.isBorder ? '5px' : 'none')};
  ::placeholder {
    color: var(--greyColor4);
    font-weight: normal;
  }
  @media only screen and (max-width: 600px) {
    font-size: ${(props) => (props.isHeader ? '2rem' : '1rem')};
  }
`

const Input = React.forwardRef((props, ref) => {
  const {
    value,
    onChange,
    placeholder,
    style,
    isClear,
    onClear,
    isHeader,
    isInputButton,
    isBorder,
    ...commonProps
  } = props
  return (
    <Container isBorder={isBorder} isInputButton={isInputButton} style={style}>
      <CustomInput
        type="text"
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        isHeader={isHeader}
        ref={ref}
        isBorder={isBorder}
        {...commonProps}
      />
      {isClear && value && (
        <CustomFAIcon
          onClick={onClear}
          icon={faTimes}
          opacity="0.5"
          style={{
            cursor: 'pointer',
            color: 'var(--redLigthColor)'
          }}
        />
      )}
    </Container>
  )
})

Input.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  style: PropTypes.object,
  placeholder: PropTypes.string,
  isClear: PropTypes.bool,
  isBorder: PropTypes.bool,
  onClear: PropTypes.func,
  isHeader: PropTypes.bool,
  isInputButton: PropTypes.bool
}

Input.defaultProps = {
  value: '',
  onChange: () => {},
  style: {},
  placeholder: '',
  isClear: false,
  isBorder: false,
  onClear: () => {},
  isHeader: false,
  isInputButton: false
}

export default Input
