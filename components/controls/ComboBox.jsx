import React, { useCallback, useMemo } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { faCaretDown, faCaretUp } from '@fortawesome/free-solid-svg-icons'
import { addIcon, CustomFAIcon } from '../../libs/FontAwesome'

addIcon(faCaretDown)
addIcon(faCaretUp)

const ComboBoxContainer = styled.ul`
  position: relative;
  right: 0;
  top: 0;
  display: grid;
  grid-gap: 0.3rem;
  grid-template-columns: 1fr;
  grid-template-rows: auto;
  place-items: center;
  font-size: 1.3rem;
  font-family: 'do hyeon';
  @media only screen and (max-width: 600px) {
    font-size: 1rem;
  }
`
const Item = styled.li`
  cursor: pointer;
  background-color: ${(props) =>
    props.isSelected ? 'var(--greyColor4)' : 'var(--whiteColor)'};
  color: ${(props) =>
    props.isSelected ? 'var(--blackColor)' : 'var(--greyColor5)'};
  padding: 1rem;
  min-width: 7rem;
  &:hover {
    background-color: var(--everGreenColor);
    color: var(--whiteColor);
  }
`
const DisplayItem = styled.div`
  cursor: pointer;
  display: grid;
  grid-template-columns: auto auto;
  grid-gap: 1rem;
  place-items: center;
  justify-self: start;
  height: 2.5rem;
`
const DisplayList = styled.div`
  position: absolute;
  top: 3rem;
  background-color: var(--greyColor2);
  font-size: 1rem;
  box-shadow: rgb(0 0 0 / 10%) 0px 2px 5px;
  z-index: 10;
`
const ComboBox = ({
  itemsSource,
  selectedItem,
  onSelectedItem,
  isShowList,
  setIsShowList,
  children
}) => {
  itemsSource.forEach((item) => {
    if (item.icon) {
      addIcon(item.icon)
    }
  })
  const onShowListItems = useCallback(() => {
    setIsShowList((prev) => !prev)
  }, [])
  const arrowIcon = useMemo(() => {
    if (isShowList) {
      return faCaretUp
    }
    return faCaretDown
  }, [isShowList])
  const getIsSelected = useCallback(
    (item) => {
      if (item === selectedItem) return true
      return false
    },
    [selectedItem]
  )
  const getIconColor = useCallback(
    (item) => {
      if (getIsSelected(item)) {
        return item.iconColor
      }
      return 'var(--greyColor4)'
    },
    [selectedItem]
  )
  const onLeaveFocusComboBox = useCallback(() => {
    setIsShowList(false)
  }, [])
  return (
    <ComboBoxContainer isShowList={isShowList}>
      <DisplayItem
        tabIndex={0}
        onClick={onShowListItems}
        onBlur={onLeaveFocusComboBox}
      >
        {children}
        <CustomFAIcon icon={arrowIcon} />
      </DisplayItem>
      <>
        {isShowList && (
          <DisplayList>
            {itemsSource.map((item) => (
              <Item
                key={item.displayName}
                onMouseDown={onSelectedItem(item)}
                isSelected={getIsSelected(item)}
              >
                {item.icon && (
                  <CustomFAIcon color={getIconColor(item)} icon={item.icon} />
                )}
                {item.displayName}
              </Item>
            ))}
          </DisplayList>
        )}
      </>
    </ComboBoxContainer>
  )
}

ComboBox.propTypes = {
  itemsSource: PropTypes.arrayOf(
    PropTypes.shape({
      displayName: PropTypes.string.isRequired,
      link: PropTypes.string
    })
  ).isRequired,
  onSelectedItem: PropTypes.func.isRequired,
  selectedItem: PropTypes.object,
  isShowList: PropTypes.bool.isRequired,
  setIsShowList: PropTypes.func.isRequired,
  children: PropTypes.element.isRequired
}

ComboBox.defaultProps = {
  selectedItem: {}
}
export default ComboBox
