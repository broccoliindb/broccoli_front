import React, { useCallback } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Input from './Input'
import Button from './Button'
import useDebounce from '../../hooks/useDebounce'

const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr auto;
  grid-template-rows: 1fr;
  margin-bottom: 1rem;
  border-radius: 5px;
`

const InputButton = (props) => {
  const {
    value,
    onChange,
    placeholder,
    style,
    isClear,
    onClear,
    content,
    onClick,
    disable,
    ...commonProps
  } = props
  const click = useDebounce(onClick, 300)
  return (
    <Container style={style}>
      <Input
        value={value || ''}
        onChange={onChange}
        placeholder={placeholder}
        isClear={isClear}
        onClear={onClear}
        isInputButton
        {...commonProps}
      />
      <Button
        onClick={click}
        disable={disable}
        style={{
          backgroundColor: 'var(--everGreenColor)',
          color: 'var(--whiteColor)',
          padding: '0.5rem 1rem',
          margin: '0',
          borderRadius: '0 5px 5px 0',
          cursor: 'pointer'
        }}
      >
        {content}
      </Button>
    </Container>
  )
}

InputButton.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  style: PropTypes.object,
  placeholder: PropTypes.string,
  isClear: PropTypes.bool,
  onClear: PropTypes.func,
  content: PropTypes.string,
  onClick: PropTypes.func,
  disable: PropTypes.bool
}

InputButton.defaultProps = {
  value: '',
  onChange: () => {},
  style: {},
  placeholder: '',
  isClear: false,
  onClear: () => {},
  content: '',
  onClick: () => {},
  disable: false
}

export default InputButton
