import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Container = styled.div`
  display: grid;
  width: 100%;
  grid-template-columns: repeat(auto-fit, minmax(10px, 1fr));
  grid-auto-rows: repeat(auto-fit, minmax(10px, max-content));
`

const GridBox = (props) => {
  const { style } = props
  return <Container style={style} {...props} />
}

GridBox.propTypes = {
  style: PropTypes.object
}

GridBox.defaultProps = {
  style: {}
}

export default GridBox
