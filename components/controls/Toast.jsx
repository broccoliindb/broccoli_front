import React, { useEffect, useState } from 'react'
import styled, { keyframes } from 'styled-components'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import { STATUS, TYPES, REDUCER } from '../../reducers/rootReducer'

const getToastType = (type) => {
  switch (type) {
    case STATUS.ERROR:
      return 'var(--redColor)'
    default:
      return 'var(--greenColor)'
  }
}

const transitionAnimation = keyframes`
  16.666% { transform: translateX(0);}
  83.333% { transform: translateX(0);}
`

const Container = styled.div`
  position: fixed;
  top: 4.5rem;
  right: 0;
  width: max-content;
  padding: 1rem;
  margin: 1rem;
  border-radius: 15px;
  background-color: ${(props) => getToastType(props.type)};
  opacity: ${(props) => (props.type === STATUS.LOADING ? 0 : 1)};
  transform: translateX(1000px);
  animation: ${transitionAnimation} ${(props) => props.wait / 1000}s ease-in-out
    forwards;
  z-index: 100;
  @media only screen and (max-width: 600px) {
    font-size: 0.8rem;
    margin: 0.5rem;
  }
`

const Toast = ({ toastToward, wait = 1500, type, message }) => {
  const dispatch = useDispatch()
  const [value, setValue] = useState(null)
  const toastStartAction = {}
  const toastEndAction = {}
  if (toastToward) {
    switch (toastToward) {
      case REDUCER.USER: {
        toastStartAction.type = TYPES.USER_TOAST_START
        toastEndAction.type = TYPES.USER_TOAST_END
        break
      }
      case REDUCER.POST: {
        toastStartAction.type = TYPES.POST_TOAST_START
        toastEndAction.type = TYPES.POST_TOAST_END
        break
      }
      default:
        toastStartAction.type = TYPES.USER_TOAST_START
        toastEndAction.type = TYPES.USER_TOAST_END
    }
  }
  useEffect(() => {
    if (message && (type === STATUS.DONE || type === STATUS.ERROR)) {
      dispatch(toastStartAction)
      setValue(
        setTimeout(() => {
          dispatch(toastEndAction)
        }, wait)
      )
    }
    return () => {
      clearTimeout(value)
    }
  }, [type, message])
  return (
    <>
      {message && type && (
        <Container type={type} wait={wait}>
          <div>{message}</div>
        </Container>
      )}
    </>
  )
}

Toast.propTypes = {
  toastToward: PropTypes.string,
  wait: PropTypes.number,
  type: PropTypes.string,
  message: PropTypes.string.isRequired
}

Toast.defaultProps = {
  toastToward: '',
  wait: 1500,
  type: STATUS.ERROR
}

export default Toast
