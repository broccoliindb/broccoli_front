import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Button from './Button'
import useDebounce from '../../hooks/useDebounce'

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  display: grid;
  place-items: center;
  background-color: var(--whiteBaseTransparnetBg);
  z-index: 10;
`
const Box = styled.div`
  display: grid;
  grid-template-rows: auto 1fr auto;
  padding: 1rem;
  background-color: var(--whiteColor);
  box-shadow: rgb(0 0 0 / 20%) 0px 2px 10px;
  border-radius: 15px;
  div {
    display: flex;
    justify-content: flex-end;
  }
`
const MessageBox = ({
  onCancel,
  onOK,
  title = '임시 포스트 불러오기',
  message = ''
}) => {
  const cancel = useDebounce(onCancel, 300)
  const ok = useDebounce(onOK, 300)
  return (
    <Container>
      <Box>
        <h4>{title}</h4>
        <p>{message}</p>
        <div>
          <Button
            isBorder
            style={{
              padding: '0.5rem',
              borderRadius: '5px',
              marginRight: '0.3rem',
              color: 'var(--everGreenColor)'
            }}
            onClick={cancel}
          >
            취소
          </Button>
          <Button
            onClick={ok}
            style={{
              backgroundColor: 'var(--everGreenColor)',
              color: 'var(--whiteColor)',
              padding: '0.5rem',
              borderRadius: '5px'
            }}
          >
            확인
          </Button>
        </div>
      </Box>
    </Container>
  )
}

MessageBox.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onOK: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired
}

export default MessageBox
