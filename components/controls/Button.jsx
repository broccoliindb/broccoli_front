import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const CustomButton = styled.button`
  outline: none;
  border-radius: ${(props) => props.borderRadius || 0};
  border: ${(props) =>
    props.isSelected || props.isBorder
      ? '1px solid var(--everGreenColor)'
      : 'none'};
  cursor: pointer;
  background-color: ${(props) => props.backgroundColor || 'transparent'};
  width: ${(props) => (props.width100 ? '100%' : 'max-content')};
  font-size: ${(props) => props.fontSize || '1rem'};
  padding: ${(props) => props.padding || '0'};
  color: ${(props) =>
    props.isSelected || props.isBorder ? props.color : 'var(--greyColor5)'};
  cursor: ${(props) => (props.disabled ? 'default' : 'pointer')};
  &:hover {
    opacity: ${(props) => (props.disabled ? 1 : 0.8)};
    color: ${(props) => props.isHover && props.hoverColor};
    background-color: ${(props) =>
      props.isUnderLine ? 'transparent' : props.backgroundColor};
    text-decoration: ${(props) => (props.isUnderLine ? 'underline' : 'none')};
    text-underline-position: ${(props) =>
      props.isUnderLine ? 'under' : 'none'};
  }
  @media only screen and (max-width: 600px) {
    font-size: 1rem;
    width: 100%;
  }
`

const Button = (props) => {
  const { type, onClick, style, children } = props
  return (
    <CustomButton
      type={type || 'button'}
      onClick={onClick}
      style={style}
      {...props}
    >
      {children}
    </CustomButton>
  )
}

Button.propTypes = {
  type: PropTypes.string,
  onClick: PropTypes.func,
  style: PropTypes.object,
  children: PropTypes.node.isRequired
}

Button.defaultProps = {
  onClick: () => {},
  type: 'button',
  style: {}
}
export default Button
