import styled from 'styled-components'

const Close = styled.button`
  border: none;
  width: max-content;
  background-color: transparent;
  margin-left: auto;
  margin-right: 0;
  margin-bottom: 2rem;
  width: 1.5rem;
  height: 1.5rem;
  &:hover {
    background-color: var(--greyColor4);
    border-radius: 50%;
  }
`

export default Close
