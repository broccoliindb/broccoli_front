import React, { useCallback } from 'react'
import Link from 'next/link'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { addIcon, CustomFAIcon } from '../../libs/FontAwesome'

const Container = styled.ul`
  padding: 1rem;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  font-size: 1.3rem;
  @media only screen and (max-width: 600px) {
    font-size: 1rem;
  }
`
const Item = styled.li`
  cursor: pointer;
  display: grid;
  grid-template-columns: ${(props) =>
    (props.isIcon && 'auto 1fr') || 'minmax(4rem, auto)'};
  place-items: center;
  grid-gap: 0.5rem;
  border-bottom: ${(props) =>
    props.isSelected
      ? '3px solid var(--everGreenColor)'
      : '3px solid transparent'};
  padding: 1rem 0;
  margin: 0 0.5rem;
  color: ${(props) => (props.isSelected ? props.color : 'var(--greyColor5)')};
  font-family: 'do hyeon';
  @media only screen and (max-width: 600px) {
    margin: 0 0.1rem;
  }
`
const Tabs = ({ itemsSource, selectedItem, onSelectedItem }) => {
  itemsSource.forEach((item) => {
    if (item.icon) {
      addIcon(item.icon)
    }
  })
  const getIsSelected = useCallback(
    (item) => {
      if (item === selectedItem) return true
      return false
    },
    [selectedItem]
  )
  const getIconColor = useCallback(
    (item) => {
      if (getIsSelected(item)) {
        return item.iconColor
      }
      return 'var(--greyColor4)'
    },
    [selectedItem]
  )
  return (
    <Container>
      {itemsSource.map((item) => (
        <div key={item.displayName}>
          {item.link && (
            <Link href={item.link}>
              <Item
                isIcon={!!item.icon}
                isSelected={getIsSelected(item)}
                onClick={onSelectedItem(item)}
              >
                {item.icon && (
                  <CustomFAIcon color={getIconColor(item)} icon={item.icon} />
                )}
                {item.displayName}
              </Item>
            </Link>
          )}
          {!item.link && (
            <Item
              isIcon={!!item.icon}
              isSelected={getIsSelected(item)}
              onClick={onSelectedItem(item)}
            >
              {item.icon && (
                <CustomFAIcon color={getIconColor(item)} icon={item.icon} />
              )}
              {item.displayName}
            </Item>
          )}
        </div>
      ))}
    </Container>
  )
}

Tabs.propTypes = {
  itemsSource: PropTypes.arrayOf(
    PropTypes.shape({
      displayName: PropTypes.string.isRequired,
      link: PropTypes.string
    })
  ).isRequired,
  selectedItem: PropTypes.object,
  onSelectedItem: PropTypes.func.isRequired
}

Tabs.defaultProps = {
  selectedItem: {}
}
export default Tabs
