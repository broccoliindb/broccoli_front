import React from 'react'
import styled from 'styled-components'
import { faHeart } from '@fortawesome/free-solid-svg-icons'
import PropTypes from 'prop-types'
import { addIcon, CustomFAIcon } from '../../libs/FontAwesome'

addIcon(faHeart)

const Container = styled.div`
  cursor: pointer;
  border: 2px solid var(--everGreenColor);
  color: var(--greyColor6);
  font-weight: bold;
  width: max-content;
  padding: 0.3rem 0.5rem;
  border-radius: 10px;
  display: grid;
  grid-template-columns: repeat(2, auto);
  grid-gap: 0.3rem;
  svg {
    color: ${(props) =>
      props.isLiked ? 'var(--redColor)' : 'var(--greyColor4)'};
  }
`

const LikeButton = ({ likeCount = 0, isLiked = false, onClick }) => {
  return (
    <Container onClick={onClick} isLiked={isLiked}>
      <CustomFAIcon icon={faHeart} />
      {likeCount}
    </Container>
  )
}

LikeButton.propTypes = {
  likeCount: PropTypes.number,
  onClick: PropTypes.func.isRequired,
  isLiked: PropTypes.bool
}

LikeButton.defaultProps = {
  likeCount: 0,
  isLiked: false
}

export default LikeButton
