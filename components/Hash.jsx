import React, { useCallback, useRef, useEffect, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import useIntersectionObserver from '../hooks/useIntersectionObserver'
import Layout from './Layout'
import { TYPES, STATUS } from '../reducers/rootReducer'
import PostSummary from './PostSummary'
import Loader from './controls/Loader'

const Container = styled.div`
  padding: 1rem;
  display: grid;
  @media only screen and (max-width: 600px) {
    padding: 1rem 0;
  }
`
const Content = styled.li`
  display: grid;
  grid-template-rows: repeat(auto-fit, auto);
  padding-top: 1rem;
  padding-bottom: 2rem;
  border-bottom: 1px solid var(--greyColor4);
`
const Observer = styled.div`
  width: 100;
  height: 2rem;
`

const HashPosts = () => {
  const dispatch = useDispatch()
  const loader = useRef(false)
  const observerRef = useRef()
  const { posts, status: postStatus } = useSelector((state) => state.post)
  const { getAllPostsOnHashStatus } = postStatus
  const { siteOwner, me } = useSelector((state) => state.user)
  const router = useRouter()
  const { query } = router
  const { user, value } = query
  const lastId = posts.length > 0 && posts[posts.length - 1].id
  const owner = useMemo(() => {
    if (siteOwner) return siteOwner
    if (me) return me
    return {
      nickname: '',
      avatar: '',
      defaultavatar: '',
      blogname: ''
    }
  }, [siteOwner, me])
  const handler = useCallback(() => {
    if (lastId) {
      dispatch({
        type: TYPES.GET_ALL_POSTS_ON_HASH_REQUEST,
        data: {
          hashtag: value,
          lastId
        }
      })
    }
  }, [posts])
  const [setNode] = useIntersectionObserver(
    {
      rootMargin: '0px',
      threshold: 0,
      lastId: lastId || 0,
      hashtag: value
    },
    handler
  )
  const getPostParams = useCallback((post) => {
    return {
      title: post.title,
      updatedat: post.updatedAt,
      content: post.content,
      postkey: post.postKey,
      thumbnail: post.thumbnail,
      nickname: post.User.nickname,
      avatar: post.User.avatar || post.User.defaultavatar,
      likeCount: post.Liker?.length,
      commentCount: post.Comments?.length,
      hashtags: post.Hashtags
    }
  }, [])

  useEffect(() => {
    if (!observerRef.current) return
    setNode(observerRef.current)
  }, [])
  return (
    <Layout siteOwner={owner}>
      <>
        <h2 style={{ margin: 0, padding: '1rem 0 0 1rem' }}>
          {user && <span>{`#${value} ${user}`}</span>}
          {!user && <span>{`#${value}`}</span>}
        </h2>
        <Container>
          <ul>
            {posts.length === 0 && getAllPostsOnHashStatus === STATUS.DONE ? (
              <h2 style={{ margin: '0 1rem' }}>😢 아무 컨텐츠가 없습니다</h2>
            ) : (
              <>
                {posts.map((post) => (
                  <Content key={post.id}>
                    <PostSummary post={getPostParams(post)} isMyDocs />
                  </Content>
                ))}
              </>
            )}
          </ul>
          <div style={{ height: '2rem' }}>
            <Loader isLoader={loader.current} />
          </div>
          <Observer ref={observerRef} />
        </Container>
      </>
    </Layout>
  )
}

export default HashPosts
