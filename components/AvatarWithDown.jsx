import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { faCaretDown } from '@fortawesome/free-solid-svg-icons'
import { addIcon, CustomFAIcon } from '../libs/FontAwesome'
import Avatar from './controls/Avatar'

addIcon(faCaretDown)

const Container = styled.div`
  display: grid;
  grid-template-columns: 2fr 1fr;
  grid-gap: 0.3rem;
  place-items: center;
  cursor: pointer;
  color: var(--greyColor5);
  &:hover {
    color: var(--blackColor);
  }
`
const AvatarWithDown = ({ width, height, onClick, imgPath, ...props }) => {
  return (
    <Container onClick={onClick} {...props}>
      <Avatar width={width} height={height} imgPath={imgPath} />
      <CustomFAIcon icon={faCaretDown} />
    </Container>
  )
}

AvatarWithDown.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
  imgPath: PropTypes.string.isRequired
}

export default AvatarWithDown
