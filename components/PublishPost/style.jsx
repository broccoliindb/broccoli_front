import styled from 'styled-components'
import GridBox from '../controls/GridBox'
import Button from '../controls/Button'

export const Container = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  z-index: 10;
  top: 0;
  left: 0;
  background-color: var(--greyColor2);
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  button {
    background-color: var(--whiteColor);
    .inner > span {
      margin-left: 1rem;
    }
  }
  @media only screen and (max-width: 600px) {
    overflow-y: auto;
  }
  @media only screen and (min-width: 601px) and (max-width: 670px) {
    * {
      font-size: 1rem;
    }
  }
`

export const Contents = styled.div`
  height: max-content;
  margin-top: auto;
  margin-bottom: auto;
  grid-column: 3 / span 8;
  display: grid;
  grid-template-columns: 1fr 1fr;
  @media only screen and (max-width: 600px) {
    grid-column: 1 / -1;
    grid-template-columns: 1fr;
  }
  @media only screen and (min-width: 601px) and (max-width: 910px) {
    grid-column: 1 / -1;
  }
  @media only screen and (min-width: 911px) and (max-width: 990px) {
    grid-column: 2 / span 10;
  }
`
export const Column = styled.div`
  border-right: ${(props) =>
    props.isBorderRight ? '1px solid var(--greyColor3)' : 'none'};
  padding: 1rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media only screen and (max-width: 600px) {
    border-right: none;
    padding: ${(props) => (props.second ? '0 1rem 1rem 1rem' : '1rem')};
  }
`
export const Content = styled.div`
  button {
    margin-left: 0;
  }
  margin-bottom: 1rem;
`
export const Upload = styled.div`
  background-color: var(--greyColor3);
  display: grid;
  place-items: center;
  padding: 1rem;
  margin-bottom: 1rem;
  input {
    display: none;
  }
  min-height: 15rem;
`
export const Summary = styled.textarea`
  width: 100%;
  margin-bottom: 0.5rem;
  border: none;
  box-shadow: rgb(0 0 0 / 5%) 0px 0px 4px;
`
export const Buttons = styled.div`
  margin-bottom: 1rem;
  display: flex;
  justify-content: space-between;
  button {
    flex: 1 0 50%;
  }
`
export const ThumnailContainer = styled.div`
  margin-bottom: 1rem;
  text-align: right;
  button {
    border: none;
    cursor: pointer;
    background-color: transparent;
    &:hover {
      text-decoration: underline;
      color: var(--everGreenColor);
    }
  }
`
export const ThumbnailContent = styled.div`
  background-image: url(${(props) => props.thumbnailUrl});
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  min-height: 15rem;
  width: 100%;
  opacity: 1;
`

export const ButtonGridBox = styled(GridBox)`
  margin-top: 1rem;
  grid-gap: 0.3rem;
  grid-template-columns: 1fr 1fr;
`
export const ButtonWrapper = styled(Button)`
  padding: 1rem;
  border-radius: 5px;
  color: ${(props) =>
    props.isSelected ? 'var(--everGreenColor)' : 'var(--greyColor4)'};
`

export const SeriesGridBox = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: auto 1rem;
  place-items: center;
  div {
    width: 100%;
    text-align: center;
    padding: 1rem;
    margin-bottom: 0.5rem;
    background-color: var(--whiteColor);
    color: var(--everGreenColor);
  }
  button {
    border: none;
    cursor: pointer;
    background-color: transparent;
    justify-self: end;
    &:hover {
      text-decoration: underline;
      color: var(--everGreenColor);
    }
  }
`

export const Paragraph = styled.p`
  text-align: right;
  color: ${(props) => props.color || 'var(--blackColor)'};
  @media only screen and (max-width: 600px) {
    padding-bottom: 0;
  }
`
