import React, { useState, useCallback, useMemo, useRef, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import PropTypes from 'prop-types'
import {
  faImage,
  faGlobeAsia,
  faLock,
  faPlus
} from '@fortawesome/free-solid-svg-icons'
import { TYPES } from '../../reducers/rootReducer'
import { addIcon, CustomFAIcon } from '../../libs/FontAwesome'
import Button from '../controls/Button'
import SeriesAdder from '../SeriesAdder'
import getImage, { UPLOAD_TYPE } from '../../libs/uploadImage'
import {
  Container,
  Contents,
  Column,
  Content,
  Upload,
  Summary,
  ButtonGridBox,
  ThumnailContainer,
  ThumbnailContent,
  SeriesGridBox,
  ButtonWrapper,
  Paragraph
} from './style'
import useDebounce from '../../hooks/useDebounce'

addIcon(faImage)
addIcon(faGlobeAsia)
addIcon(faLock)
addIcon(faPlus)

const BUTTON_TYPES = {
  PUBLIC: 'PUBLIC',
  PRIVATE: 'PRIVATE',
  CANCEL: 'CANCEL',
  PUBLISH: 'PUBLISH'
}
const SCOPE_TYPES = {
  PUBLIC: 'A',
  PRIVATE: 'P'
}
const trimTag = (html) => {
  if (html) {
    return html.replace(/(&nbsp;|<([^>]+)>)/gi, '').substring(0, 150)
  }
  return null
}

const PublishPost = ({
  prevPost,
  titleValue,
  htmlValue,
  markdownValue,
  thumbnail,
  tags,
  deletedTags,
  onCancel
}) => {
  const { id, scope: scopeValue, series: seriesValue } = prevPost
  const dispatch = useDispatch()
  const { series } = useSelector((state) => state.series)
  const fileInput = useRef(null)
  const [scope, setScope] = useState(scopeValue || SCOPE_TYPES.PUBLIC)
  const [thumbnailImage, setThumbnailImage] = useState(thumbnail)
  const [isForSeriesButton, setIsForSeriesButton] = useState(false)
  const [summary, setSummary] = useState(trimTag(htmlValue))
  const [showSeries, isShowSeries] = useState(false)
  const [selectedSeriesItem, setSelectedSeriesItem] = useState(null)
  const [hasSeries, setHasSeries] = useState(!!seriesValue)
  const setSeriesSelectionButtonStyle = useMemo(() => {
    if (!selectedSeriesItem) {
      return {
        backgroundColor: 'var(--greyColor3)',
        color: 'var(--greyColor4)',
        borderRadius: '5px'
      }
    }
    return {
      backgroundColor: 'var(--everGreenColor)',
      color: 'var(--whiteColor)',
      borderRadius: '5px'
    }
  }, [selectedSeriesItem])
  const onChangeContent = useCallback((e) => {
    if (e.target.value.length > 150) return
    setSummary(e.target.value)
  }, [])
  const onAddSeries = useCallback(() => {
    isShowSeries(true)
    setIsForSeriesButton(true)
  }, [])
  const onSelectSeriesItem = useCallback(
    (item) => () => {
      setSelectedSeriesItem(item)
    },
    []
  )
  const onRemoveSelectedItem = useCallback(() => {
    setHasSeries(false)
    setSelectedSeriesItem(null)
  }, [])
  const onUploadThumbnail = useCallback(() => {
    if (fileInput.current) {
      fileInput.current.click()
    }
  }, [fileInput])
  const onChangeImage = useCallback(
    async (e) => {
      const fileUploaded = e.target.files[0]
      if (!fileUploaded) return
      const image = await getImage(fileUploaded, UPLOAD_TYPE.POST)
      setThumbnailImage(image.data.src)
    },
    [UPLOAD_TYPE]
  )
  const onRemoveThumbnail = useCallback(() => {
    setThumbnailImage(null)
  }, [thumbnail])
  const onDragOver = useCallback((e) => {
    e.preventDefault()
  }, [])

  const onDragEnter = useCallback((e) => {
    e.preventDefault()
  }, [])

  const onDragLeave = useCallback((e) => {
    e.preventDefault()
  }, [])

  const onFileDrop = useCallback(async (e) => {
    e.preventDefault()
    const { files } = e.dataTransfer
    if (files && files.length > 0) {
      const image = await getImage(files[0], UPLOAD_TYPE.POST)
      setThumbnailImage(image.data.src)
    }
  }, [])
  const onClickButton = useCallback(
    (buttonType) => () => {
      switch (buttonType) {
        case BUTTON_TYPES.PUBLIC: {
          setScope(SCOPE_TYPES.PUBLIC)
          break
        }
        case BUTTON_TYPES.PRIVATE: {
          setScope(SCOPE_TYPES.PRIVATE)
          break
        }
        case BUTTON_TYPES.CANCEL: {
          if (showSeries) {
            isShowSeries(false)
            setIsForSeriesButton(false)
            setSelectedSeriesItem(null)
          } else {
            onCancel()
          }
          break
        }
        case BUTTON_TYPES.PUBLISH: {
          if (showSeries) {
            isShowSeries(false)
            setHasSeries(true)
            setIsForSeriesButton(false)
          }
          break
        }
        default:
      }
    },
    [showSeries]
  )
  const onPublish = useCallback(() => {
    dispatch({
      type: TYPES.ADD_POST_REQUEST,
      data: {
        id,
        title: titleValue,
        content: htmlValue,
        markdown: markdownValue,
        scope,
        summary,
        series: selectedSeriesItem,
        thumbnail: thumbnailImage,
        tags,
        deletedTags
      }
    })
  }, [selectedSeriesItem, thumbnailImage, scope])
  const getColor = useCallback(() => {
    if (summary && summary.length >= 150) {
      return 'var(--redColor)'
    }
  }, [summary])
  const uploadThumbnail = useDebounce(onUploadThumbnail, 300)
  const removeThumbnail = useDebounce(onRemoveThumbnail, 300)
  const addSeries = useDebounce(onAddSeries, 300)
  const removeSelectedItem = useDebounce(onRemoveSelectedItem, 300)
  const publish = useDebounce(onPublish, 300)
  useEffect(() => {
    dispatch({ type: TYPES.GET_SERIES_REQUEST })
  }, [])
  useEffect(() => {
    if (series && series.length > 0) {
      const item = series.find((i) => i.name === seriesValue)
      setSelectedSeriesItem(item)
    }
  }, [series])
  return (
    <Container>
      <Contents>
        <Column isBorderRight>
          <Content>
            <h4>포스트미리보기</h4>
            {!thumbnailImage && (
              <Upload
                onDragOver={onDragOver}
                onDragEnter={onDragEnter}
                onDragLeave={onDragLeave}
                onDrop={onFileDrop}
              >
                <CustomFAIcon
                  style={{
                    width: '6rem',
                    height: '6rem',
                    color: 'var(--greyColor4)'
                  }}
                  icon={faImage}
                />
                <Button
                  onClick={uploadThumbnail}
                  isBorder
                  style={{
                    color: 'var(--everGreenColor)',
                    backgroundColor: 'var(--whiteColor)',
                    padding: '0.3rem',
                    borderRadius: '5px'
                  }}
                >
                  썸네일 업로드
                </Button>
                <input
                  ref={fileInput}
                  onChange={onChangeImage}
                  type="file"
                  id="thumbnail"
                  name="thumnail"
                />
              </Upload>
            )}
            {thumbnailImage && (
              <ThumnailContainer>
                <label htmlFor="file">
                  <button type="button" onClick={uploadThumbnail}>
                    재업로드
                  </button>
                  <input
                    ref={fileInput}
                    type="file"
                    onChange={onChangeImage}
                    id="file"
                    style={{ display: 'none' }}
                  />
                </label>
                <button type="button" onClick={removeThumbnail}>
                  제거하기
                </button>
                <ThumbnailContent
                  onDragOver={onDragOver}
                  onDragEnter={onDragEnter}
                  onDragLeave={onDragLeave}
                  onDrop={onFileDrop}
                  thumbnailUrl={thumbnailImage}
                />
              </ThumnailContainer>
            )}
          </Content>
          <Content>
            <h4>{titleValue}</h4>
            <Summary
              placeholder="당시의 포스트를 짧게 소개해보세요."
              rows={4}
              value={summary}
              onChange={onChangeContent}
            />
            <Paragraph color={getColor()}>
              <span>{summary.length}</span>
              /150
            </Paragraph>
          </Content>
        </Column>
        <Column second>
          <Content>
            <h4>공개설정</h4>
            <ButtonGridBox>
              <ButtonWrapper
                width100
                isSelected={scope === SCOPE_TYPES.PUBLIC}
                onClick={onClickButton(BUTTON_TYPES.PUBLIC)}
              >
                <div className="inner">
                  <CustomFAIcon icon={faGlobeAsia} />
                  <span>전체공개</span>
                </div>
              </ButtonWrapper>
              <ButtonWrapper
                width100
                isSelected={scope === SCOPE_TYPES.PRIVATE}
                onClick={onClickButton(BUTTON_TYPES.PRIVATE)}
              >
                <div className="inner">
                  <CustomFAIcon icon={faLock} />
                  <span>비공개</span>
                </div>
              </ButtonWrapper>
            </ButtonGridBox>
          </Content>
          <Content>
            <h4>시리즈 설정</h4>
            {showSeries && !hasSeries && (
              <SeriesAdder
                onSelectSeriesItem={onSelectSeriesItem}
                selected={selectedSeriesItem}
                series={series}
              />
            )}
            {!showSeries && !hasSeries && (
              <Button
                onClick={addSeries}
                style={{
                  width: '100%',
                  color: 'var(--everGreenColor)',
                  padding: '1rem',
                  borderRadius: '5px'
                }}
              >
                <div className="inner">
                  <CustomFAIcon icon={faPlus} />
                  <span>시리즈에 추가하기</span>
                </div>
              </Button>
            )}
            {!showSeries && hasSeries && selectedSeriesItem && (
              <SeriesGridBox>
                <div>{selectedSeriesItem.name}</div>
                <button type="button" onClick={removeSelectedItem}>
                  시리즈에서제거
                </button>
              </SeriesGridBox>
            )}
          </Content>
          <ButtonGridBox>
            <Button
              width100
              style={{
                backgroundColor: 'var(--greyColor4)',
                color: 'var(--whiteColor)',
                padding: '0.5rem',
                borderRadius: '5px'
              }}
              onClick={onClickButton(BUTTON_TYPES.CANCEL)}
            >
              취소
            </Button>
            {!isForSeriesButton && (
              <Button
                width100
                style={{
                  backgroundColor: 'var(--everGreenColor)',
                  color: 'var(--whiteColor)',
                  padding: '0.5rem',
                  borderRadius: '5px'
                }}
                onClick={publish}
              >
                출간하기
              </Button>
            )}
            {isForSeriesButton && (
              <Button
                disabled={!selectedSeriesItem}
                width100
                style={setSeriesSelectionButtonStyle}
                onClick={onClickButton(BUTTON_TYPES.PUBLISH)}
              >
                선택하기
              </Button>
            )}
          </ButtonGridBox>
        </Column>
      </Contents>
    </Container>
  )
}

PublishPost.propTypes = {
  prevPost: PropTypes.shape({
    id: PropTypes.number.isRequired,
    scope: PropTypes.string.isRequired,
    series: PropTypes.string
  }).isRequired,
  titleValue: PropTypes.string.isRequired,
  htmlValue: PropTypes.string.isRequired,
  markdownValue: PropTypes.string.isRequired,
  onCancel: PropTypes.func.isRequired,
  thumbnail: PropTypes.string,
  tags: PropTypes.array,
  deletedTags: PropTypes.array
}

PublishPost.defaultProps = {
  thumbnail: '',
  tags: [],
  deletedTags: []
}

export default PublishPost
