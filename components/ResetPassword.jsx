import React, { useCallback, useState, useEffect, useMemo, useRef } from 'react'
import styled from 'styled-components'
import Image from 'next/image'
import { useDispatch, useSelector } from 'react-redux'
import {
  faGithub,
  faGoogle,
  faFacebook
} from '@fortawesome/free-brands-svg-icons'
import useInput from '../hooks/useInput'
import Button from './controls/Button'
import Input from './controls/Input'
import GridBox from './controls/GridBox'
import useDebounce from '../hooks/useDebounce'
import { STATUS, TYPES } from '../reducers/rootReducer'
import { addIcon } from '../libs/FontAwesome'
import Close from './controls/Close'

addIcon(faGithub)
addIcon(faGoogle)
addIcon(faFacebook)

const Backdrop = styled.div`
  position: fixed;
  display: grid;
  place-items: center;
  background-color: var(--whiteBaseTransparnetBg);
  width: 100vw;
  height: 100%;
  left: 0;
  top: 0;
  bottom: 0;
  z-index: 10;
`
const Container = styled.div`
  background-color: var(--whiteColor);
  width: 20rem;
  color: var(--greyColor6);
  font-size: 1rem;
  display: grid;
  grid-template-rows: auto 1fr;
  padding: 1rem;
  @media only screen and (max-width: 600px) {
    width: 100vw;
    height: 100vh;
  }
`
const Modal = styled.div`
  display: grid;
  grid-template-columns: minmax(150px, 15%) 1fr;
  place-items: center;
  height: auto;
  box-shadow: rgb(0 0 0 / 20%) 0px 2px 10px;
  background-color: var(--everGreenColor);
  @media only screen and (max-width: 600px) {
    grid-template-columns: 0 1fr;
    border: none;
  }
`
const Cover = styled.div`
  color: var(--whiteColor);
  @media only screen and (max-width: 600px) {
    display: none;
    visibility: collapse;
  }
`
const LoginForm = styled.div`
  margin-bottom: 5rem;
  @media only screen and (max-width: 600px) {
    margin-bottom: 0;
  }
`
const ResetPassword = () => {
  const resetBox = useRef(null)
  const dispatch = useDispatch()
  const { status, message: msg } = useSelector((state) => state.user)
  const [message, setMessage] = useState(msg)
  const { resetPasswordStatus } = status
  const [email, onChangeEmail, setEmail] = useInput('')
  const { showResetPasswordModal } = useSelector((state) => state.user.modals)

  const onClearEmail = useCallback(() => {
    setEmail('')
  }, [])
  const onCloseResetPassword = useCallback(() => {
    if (showResetPasswordModal) {
      dispatch({ type: TYPES.CLOSE_RESET_PASSWORD_MODAL })
    }
  }, [showResetPasswordModal])
  const onResetPassword = useCallback(() => {
    if (!email) {
      setMessage('이메일을 입력하세요')
      return
    }
    dispatch({ type: TYPES.RESET_PASSWORD_REQUEST, data: email })
  }, [email])
  const onLeaveFocusLogin = useCallback((e) => {
    if (!resetBox.current) return
    if (!resetBox.current.contains(e.target)) {
      onCloseResetPassword()
    }
  }, [])
  const resetPassword = useDebounce(onResetPassword, 300)
  const close = useDebounce(onCloseResetPassword, 300)
  const getMessageStyle = useMemo(() => {
    const style = {
      marginBottom: '1rem',
      alignItems: 'center'
    }
    if (resetPasswordStatus === STATUS.LOADING) return style
    if (!email || resetPasswordStatus !== STATUS.DONE) {
      style.color = 'var(--redColor)'
      return style
    }
    style.color = 'var(--greenColor)'
    return style
  }, [resetPasswordStatus])
  const imageBackground = useMemo(() => {
    return {
      borderRadius: '50%',
      width: '130px',
      height: '130px',
      display: 'grid',
      placeItems: 'center',
      backgroundColor: 'var(--greyColor4)'
    }
  }, [])
  useEffect(() => {
    setMessage(msg)
  }, [resetPasswordStatus, msg])
  useEffect(() => {
    setMessage('')
  }, [email])
  return (
    <Backdrop onClick={onLeaveFocusLogin}>
      <Modal ref={resetBox}>
        <Cover>
          <div style={imageBackground}>
            <Image src="/whale.png" width="110" height="110" />
          </div>
        </Cover>
        <Container>
          <Close onClick={close}>✖</Close>
          <LoginForm>
            <h4>비밀번호변경</h4>
            <GridBox
              style={{
                gridTemplateColumns: '1fr',
                gridGap: '0.2rem'
              }}
            >
              <Input
                isBorder
                value={email}
                onChange={onChangeEmail}
                type="email"
                name="email"
                style={{ width: 'auto', padding: '0.3rem 0' }}
                autoComplete="off"
                placeholder="이메일을 입력하세요"
                onClear={onClearEmail}
                required
                isClear
              />
              {message && <span style={getMessageStyle}>{message}</span>}
              <Button
                width100
                borderRadius="5px"
                onClick={resetPassword}
                style={{
                  backgroundColor: 'var(--everGreenColor)',
                  fontSize: '1rem',
                  padding: '0.5rem 1rem',
                  color: 'var(--whiteColor)'
                }}
                type="button"
              >
                비밀번호변경
              </Button>
            </GridBox>
          </LoginForm>
        </Container>
      </Modal>
    </Backdrop>
  )
}

export default ResetPassword
