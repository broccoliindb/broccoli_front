import React, { useCallback, useMemo } from 'react'
import styled, { keyframes } from 'styled-components'
import Link from 'next/link'
import { faHeart, faReply } from '@fortawesome/free-solid-svg-icons'
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'
import PropTypes from 'prop-types'
import Avatar from './controls/Avatar'
import getLightTime from '../libs/CustomDate'
import FlexBox from './controls/FlexBox'
import { addIcon, CustomFAIcon } from '../libs/FontAwesome'
import Button from './controls/Button'
import useDebounce from '../hooks/useDebounce'

addIcon(faHeart)
addIcon(faReply)

const scale = keyframes`
  from {
    transform: scale(1)
  }
  to {
    transform: scale(1.01)
  }
`

const Container = styled.div`
  display: grid;
  font-family: 'Nanum Gothic';
  grid-template-rows: 1fr auto;
  box-shadow: rgb(0 0 0 / 20%) 0px 2px 10px;
  &:hover {
    box-shadow: rgb(0 0 0 / 70%) 0px 2px 10px;
    animation: ${scale} 0.05s linear forwards;
  }
  @media only screen and (max-width: 600px) {
    &:hover {
      box-shadow: none;
      animation: none;
    }
  }
`
const HashTagButton = styled(Button)`
  color: var(--whiteColor);
  background-color: var(--everGreenColor);
  margin: 0 0.3rem 0.3rem 0;
  padding: 0.2rem;
  border-radius: 5px;
  width: max-content;
`
const User = styled.div`
  background-color: var(--greyColor3);
  padding: 0.5rem;
  display: grid;
  grid-template-columns: auto 1fr;
  grid-gap: 0.5rem;
  &:hover {
    cursor: pointer;
    background-color: var(--greyColor4);
    font-style: italic;
  }
  @media only screen and (max-width: 600px) {
    &:hover {
      background-color: transparent;
    }
  }
`
const Contents = styled.div`
  display: grid;
  height: 100%;
  grid-template-rows: 1fr auto;
  padding: 0.5rem;
  @media only screen and (max-width: 600px) {
    &:hover {
      background-color: transparent;
    }
  }
`
const Thumbnail = styled.div`
  background: url(${(props) => props.url});
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center;
  max-width: 100%;
  height: 10rem;
  display: grid;
  margin-bottom: 1rem;
`

const Anchor = styled.a`
  cursor: pointer;
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: ${(props) => (props.isThumbnail ? 'auto 1fr' : '1fr')};
  padding: 1rem 0;
`
const Paragraph = styled.p`
  padding: 0;
  line-height: 1.3rem;
  overflow: hidden;
  word-break: break-all;
  max-height: ${(props) => (props.isThumbnail ? '5rem' : '25rem')};
  display: -webkit-box;
  -webkit-line-clamp: ${(props) => (props.isThumbnail ? 4 : 13)};
  -webkit-box-orient: vertical;
  @media only screen and (max-width: 600px) {
    -webkit-line-clamp: 4;
  }
`
const Title = styled.h2`
  word-break: break-all;
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
`
const ContentsSummary = styled.div`
  display: grid;
  grid-template-rows: auto auto 1fr;
`
const FlexBoxWrapper = styled(FlexBox)`
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
`
const PostMeta = styled.div`
  display: grid;
  grid-template-columns: auto auto 1fr;
  grid-gap: 1rem;
`
const MetaInfo = styled.div`
  display: grid;
  grid-template-columns: auto auto 1fr;
  grid-gap: 0.5rem;
`

const PostSummary = ({ post, isTemp, isMyDocs }) => {
  const {
    title,
    tempTitle,
    nickname,
    updatedat,
    content,
    tempContent,
    postkey,
    thumbnail,
    avatar,
    defaultavatar,
    likeCount,
    commentCount,
    hashtags
  } = post
  const router = useRouter()
  const { query } = router
  const { user } = query
  const me = useSelector((state) => state.user.me)
  const showWriter = useMemo(() => {
    if (!isMyDocs || !me) return true
    if (me.nickname !== nickname) return true
    return false
  }, [isMyDocs, me, nickname])
  const getTitle = useCallback(() => {
    if (isTemp) return <Title>{tempTitle}</Title>
    return <Title>{title}</Title>
  }, [post])
  const getThumbnail = useCallback(() => {
    if (!thumbnail) return
    return <Thumbnail url={thumbnail} />
  }, [post])
  const getUpdatedAt = useCallback(() => {
    return (
      <div style={{ marginBottom: '1rem' }}>
        <span>최종수정: </span>
        {getLightTime(updatedat)}
      </div>
    )
  }, [post])
  const getContentsEclipse = useCallback(() => {
    const getContent = (rawContent) => {
      if (!rawContent) return
      const tagRemoved = rawContent.replace(
        /<(\/)?([a-zA-Z0-9]*)(\s[a-zA-Z0-9-_]*=[^>]*)?(\s)*(\/)?>/g,
        ''
      )
      return <Paragraph isThumbnail={!!thumbnail}>{tagRemoved}</Paragraph>
    }
    if (isTemp) {
      return getContent(tempContent)
    }
    return getContent(content)
  }, [content, tempContent, thumbnail])
  const getLinkHref = useCallback(() => {
    if (isTemp) {
      return `/post?key=${postkey}`
    }
    return `/@${nickname}/${postkey}`
  }, [])
  const onMoveToWriter = useCallback(() => {
    router.push(`/@${nickname}`)
  }, [nickname])
  const onHashClick = useCallback((e) => {
    if (!e.target.innerText) return
    if (user) {
      router.push(
        `/${user}/hash?value=${encodeURIComponent(e.target.innerText)}`
      )
      return
    }
    router.push(`/hash?value=${encodeURIComponent(e.target.innerText)}`)
  }, [])
  const hashClick = useDebounce(onHashClick, 300)
  const moveToWriter = useDebounce(onMoveToWriter, 300)
  return (
    <Container>
      <Contents>
        <Link href={getLinkHref()}>
          <Anchor isThumbnail={!!thumbnail}>
            {getThumbnail()}
            <ContentsSummary>
              {getTitle()}
              {getUpdatedAt()}
              {getContentsEclipse()}
            </ContentsSummary>
          </Anchor>
        </Link>
        <FlexBoxWrapper>
          {hashtags &&
            hashtags.map((hashtag) => (
              <HashTagButton onClick={hashClick} key={hashtag.id}>
                {hashtag.tag}
              </HashTagButton>
            ))}
        </FlexBoxWrapper>
        <PostMeta>
          <MetaInfo>
            <CustomFAIcon icon={faHeart} color="var(--redColor)" />
            <span>좋아요</span>
            <span>{likeCount}</span>
          </MetaInfo>
          <MetaInfo>
            <CustomFAIcon icon={faReply} color="var(--everGreenColor)" />
            <span>댓글수</span>
            <span>{commentCount}</span>
          </MetaInfo>
        </PostMeta>
      </Contents>
      <>
        {showWriter && (
          <User onClick={moveToWriter}>
            <Avatar width={25} height={25} imgPath={avatar || defaultavatar} />
            <span style={{ alignSelf: 'center' }}>{nickname}</span>
          </User>
        )}
      </>
    </Container>
  )
}

PostSummary.propTypes = {
  isMyDocs: PropTypes.bool,
  isTemp: PropTypes.bool,
  post: PropTypes.shape({
    title: PropTypes.string,
    tempTitle: PropTypes.string,
    nickname: PropTypes.string,
    updatedat: PropTypes.string,
    content: PropTypes.string,
    tempContent: PropTypes.string,
    postkey: PropTypes.string,
    thumbnail: PropTypes.string,
    avatar: PropTypes.string,
    defaultavatar: PropTypes.string,
    likeCount: PropTypes.number,
    commentCount: PropTypes.number,
    hashtags: PropTypes.array
  }).isRequired
}

PostSummary.defaultProps = {
  isMyDocs: false,
  isTemp: false
}

export default PostSummary
