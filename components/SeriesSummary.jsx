import React, { useCallback } from 'react'
import styled, { keyframes } from 'styled-components'
import PropTypes from 'prop-types'
import getLightTime from '../libs/CustomDate'

const scale = keyframes`
  from {
    transform: scale(1)
  }
  to {
    transform: scale(1.01)
  }
`

const Container = styled.div`
  display: grid;
  font-family: 'Nanum Gothic';
  grid-template-rows: 1fr auto;
  box-shadow: rgb(0 0 0 / 20%) 0px 2px 10px;
  &:hover {
    box-shadow: rgb(0 0 0 / 70%) 0px 2px 10px;
    animation: ${scale} 0.05s linear forwards;
  }
  @media only screen and (max-width: 600px) {
    &:hover {
      box-shadow: none;
      animation: none;
    }
  }
`
const Contents = styled.div`
  display: grid;
  height: 100%;
  grid-template-rows: 1fr auto;
  padding: 0.5rem;
  &:hover {
    cursor: pointer;
  }
  @media only screen and (max-width: 600px) {
    &:hover {
      background-color: transparent;
    }
  }
`
const Thumbnail = styled.div`
  background: url(${(props) => props.url});
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center;
  max-width: 100%;
  height: 10rem;
  display: grid;
  margin-bottom: 1rem;
`

const Anchor = styled.a`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 'auto 1fr';
  padding: 1rem 0 0 0;
`
const Title = styled.h2`
  word-break: break-all;
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
`
const Meta = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 1rem;
`

const SeriesSummary = ({ item }) => {
  const { title, updatedat, thumbnail, seriesCount } = item
  const getUpdatedAt = useCallback(() => {
    return (
      <div style={{ marginBottom: '1rem' }}>
        <span>마지막 업데이트: </span>
        {getLightTime(updatedat)}
      </div>
    )
  }, [])
  return (
    <Container>
      <Contents>
        <Anchor>
          <Thumbnail url={thumbnail || '/nocontent.png'} />
          <Title>{title}</Title>
        </Anchor>
        <Meta>
          <span>{`${seriesCount}개의 포스트`}</span>
          <span>{getUpdatedAt()}</span>
        </Meta>
      </Contents>
    </Container>
  )
}

SeriesSummary.propTypes = {
  item: PropTypes.shape({
    title: PropTypes.string,
    updatedat: PropTypes.string,
    thumbnail: PropTypes.string,
    seriesCount: PropTypes.number
  }).isRequired
}

export default SeriesSummary
