import React, { useCallback, useState, useMemo } from 'react'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import useDebounce from '../hooks/useDebounce'
import { TYPES } from '../reducers/rootReducer'
import ComboBox from './controls/ComboBox'
import Avatar from './controls/Avatar'

const Container = styled.div`
  padding-right: 1rem;
  position: relative;
`

const RoundButton = styled.button`
  border-radius: 15px;
  border: none;
  padding: 0.3rem 0.7rem;
  background-color: var(--darkblackColor);
  color: var(--whiteColor);
  height: 2.5rem;
`

const MENU_TYPE = {
  MY_DOC: 'MY_DOC',
  NEW_POST: 'NEW_POST',
  TEMP_POSTS: 'TEMP_POSTS',
  LIKES: 'LIKES',
  SETTINGS: 'SETTINS',
  LOG_OUT: 'LOG_OUT'
}
const MyMenu = () => {
  const dispatch = useDispatch()
  const router = useRouter()
  const { asPath } = router
  const {
    isLogin,
    modals: { showLoginModal },
    me
  } = useSelector((state) => state.user)
  const nickname = me?.nickname
  const avatar = me?.avatar || me?.defaultavatar
  const comboBoxItems = useMemo(
    () => [
      {
        displayName: '내 문서',
        value: MENU_TYPE.MY_DOC,
        link: `/@${nickname}`
      },
      { displayName: '새 글 작성', value: MENU_TYPE.NEW_POST, link: '/post' },
      {
        displayName: '임시글',
        value: MENU_TYPE.TEMP_POSTS,
        link: '/temps'
      },
      { displayName: '좋아요', value: MENU_TYPE.LIKES, link: '/posts/likes' },
      { displayName: '설정', value: MENU_TYPE.LIKES, link: '/settings' },
      { displayName: '로그아웃', value: MENU_TYPE.LOG_OUT }
    ],
    [nickname]
  )
  const [isUserMenuOpen, setIsUserMenuOpen] = useState(false)
  const selectedMenu = useMemo(() => {
    const item = comboBoxItems.find((i) => {
      return i.link === decodeURIComponent(asPath)
    })
    return item
  }, [asPath])
  const [menuItem, setMenuItem] = useState(selectedMenu)

  const onClickToLogin = useCallback(() => {
    if (!showLoginModal) {
      dispatch({
        type: TYPES.SHOW_LOGIN_MODAL
      })
    }
  }, [])
  const login = useDebounce(onClickToLogin, 300)
  const onSelectedItem = useCallback(
    (item) => () => {
      if (item.value === MENU_TYPE.LOG_OUT) {
        dispatch({ type: TYPES.LOGOUT_REQUEST })
        return
      }
      setMenuItem(item)
      setIsUserMenuOpen(false)
      router.push(decodeURIComponent(item.link))
    },
    []
  )
  if (!isLogin && !me) {
    return (
      <Container>
        <RoundButton onClick={login}>로그인</RoundButton>
      </Container>
    )
  }
  return (
    <Container>
      <ComboBox
        itemsSource={comboBoxItems}
        selectedItem={menuItem}
        onSelectedItem={onSelectedItem}
        setIsShowList={setIsUserMenuOpen}
        isShowList={isUserMenuOpen}
      >
        <Avatar width={32} height={32} imgPath={avatar} />
      </ComboBox>
    </Container>
  )
}

export default MyMenu
