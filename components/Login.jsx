import React, { useCallback, useState, useEffect, useMemo, useRef } from 'react'
import styled from 'styled-components'
import Image from 'next/image'
import { generateFromString } from 'generate-avatar'
import { useDispatch, useSelector } from 'react-redux'
import {
  faGithub,
  faGoogle,
  faFacebook
} from '@fortawesome/free-brands-svg-icons'
import { useRouter } from 'next/router'
import useInput from '../hooks/useInput'
import Close from './controls/Close'
import Button from './controls/Button'
import Input from './controls/Input'
import GridBox from './controls/GridBox'
import useDebounce from '../hooks/useDebounce'
import { STATUS, TYPES } from '../reducers/rootReducer'
import { addIcon, CustomFAIcon } from '../libs/FontAwesome'
import getImage, { UPLOAD_TYPE } from '../libs/uploadImage'

addIcon(faGithub)
addIcon(faGoogle)
addIcon(faFacebook)

const Backdrop = styled.div`
  position: fixed;
  display: grid;
  place-items: center;
  background-color: var(--whiteBaseTransparnetBg);
  width: 100vw;
  height: 100%;
  left: 0;
  top: 0;
  bottom: 0;
  z-index: 10;
`
const Container = styled.div`
  background-color: var(--whiteColor);
  width: 20rem;
  color: var(--greyColor6);
  font-size: 1rem;
  display: grid;
  grid-template-rows: auto 1fr auto;
  padding: 1rem;
  @media only screen and (max-width: 600px) {
    width: 100vw;
    height: 100vh;
  }
`
const Modal = styled.div`
  display: grid;
  grid-template-columns: minmax(150px, 15%) 1fr;
  height: auto;
  box-shadow: rgb(0 0 0 / 20%) 0px 2px 10px;
  background-color: var(--everGreenColor);
`
const Cover = styled.div`
  color: var(--whiteColor);
  width: '100%';
  height: '100%';
  margin: auto;
  @media only screen and (max-width: 600px) {
    display: none;
    visibility: collapse;
  }
`
const LoginBox = styled.div`
  margin-bottom: 5rem;
  @media only screen and (max-width: 600px) {
    margin-bottom: 0;
  }
`
const LoginForm = styled.form`
  margin-bottom: 1rem;
`
const Signup = styled.div`
  width: max-content;
  margin-left: auto;
  margin-right: 0;
  font-size: 1rem;
  button {
    &:hover {
      text-decoration: underline;
    }
  }
`
const SNSLogins = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  margin-bottom: 1rem;
  button {
    border: none;
    width: max-content;
    margin-left: auto;
    margin-right: auto;
    border-radius: 15px;
    width: 3rem;
    height: 3rem;
    svg {
      font-size: 1.3rem;
      color: white;
    }
  }
`
const GridBoxWrapper = styled(GridBox)`
  margin: 1rem 0;
  grid-gap: 0.3rem;
`
const SUBJECT = {
  LOGIN: '로그인',
  SIGNUP: '회원가입'
}
const Login = () => {
  const loginBox = useRef(null)
  const emailInputRef = useRef(null)
  const router = useRouter()
  const { asPath } = router
  const dispatch = useDispatch()
  const { status, message: msg } = useSelector((state) => state.user)
  const [message, setMessage] = useState('')
  const [email, onChangeEmail, setEmail] = useInput('')
  const [password, onChangePassword, setPassword] = useInput('')
  const [password2, onChangePassword2, setPassword2] = useInput('')
  const [isLoginForm, setLoginForm] = useState(true)
  const [subject, setSubject] = useState(SUBJECT.LOGIN)
  const { showLoginModal } = useSelector((state) => state.user.modals)
  const { me } = useSelector((state) => state.user)
  const visitCount = me?.visitCount
  const setReturnToAfterLogin = useCallback(() => {
    sessionStorage.setItem('returnTo', asPath)
  }, [asPath])
  const onClearEmail = useCallback(() => {
    setEmail('')
  }, [])
  const onClearPassword = useCallback(() => {
    setPassword('')
  }, [])
  const onClearPassword2 = useCallback(() => {
    setPassword2('')
  }, [])
  const onClickSignup = useCallback(
    async (e) => {
      try {
        e.preventDefault()
        if (email.length > 30) {
          setMessage('이메일은 30자이내로 해주세요')
          return
        }
        if (password !== password2) {
          setMessage('비밀번호가 서로 다릅니다.')
          return
        }
        const passwordRegex = new RegExp(
          '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})'
        )
        if (!password.match(passwordRegex)) {
          setMessage(
            '비밀번호는 최소 한 개의 대문자,소문자,특수문자,숫자를 포함한 8자리 이상의 수 여야합니다. '
          )
          return
        }
        const avatarSvg = generateFromString(email)
        const svgBlob = new Blob([avatarSvg], { type: 'image/svg+xml' })
        if (!svgBlob) {
          setMessage('랜덤아바타 생성에 문제가 생겼습니다. 다시 시도해보세요.')
          return
        }
        const image = await getImage(svgBlob, UPLOAD_TYPE.AVATAR)
        dispatch({
          type: TYPES.SIGNUP_REQUEST,
          data: { email, password, defaultavatar: image.data.src }
        })
      } catch (err) {
        if (err) {
          console.error(err)
        }
      }
    },
    [email, password, password2]
  )
  const onClickEmailLogin = useCallback(
    (e) => {
      e.preventDefault()
      dispatch({ type: TYPES.LOGIN_REQUEST, data: { email, password } })
    },
    [email, password]
  )
  const onGithubLogin = useCallback(() => {
    setReturnToAfterLogin()
    window.open(`${process.env.API_URL}/auth/github`, '_self')
  }, [])
  const onGoogleLogin = useCallback(() => {
    setReturnToAfterLogin()
    window.open(`${process.env.API_URL}/auth/google`, '_self')
  }, [])
  const onKakaoLogin = useCallback(() => {
    setReturnToAfterLogin()
    window.open(`${process.env.API_URL}/auth/kakao`, '_self')
  }, [])
  const onFacebookLogin = useCallback(() => {
    setReturnToAfterLogin()
    window.open(`${process.env.API_URL}/auth/facebook`, '_self')
  }, [])
  const onCloseLogin = useCallback(() => {
    if (showLoginModal) {
      dispatch({ type: TYPES.CLOSE_LOGIN_MODAL })
    }
  }, [showLoginModal])
  const onResetPassword = useCallback(() => {
    dispatch({ type: TYPES.SHOW_RESET_PASSWORD_MODAL })
  }, [])
  const onClickSignupLogin = useCallback(
    (hasAccount) => () => {
      setLoginForm((prev) => !prev)
      if (hasAccount) {
        setSubject(SUBJECT.SIGNUP)
        setEmail('')
        setPassword('')
      } else {
        setSubject(SUBJECT.LOGIN)
        setEmail('')
        setPassword('')
        setPassword2('')
      }
    },
    []
  )
  const githubLogin = useDebounce(onGithubLogin, 300)
  const googleLogin = useDebounce(onGoogleLogin, 300)
  const kakaoLogin = useDebounce(onKakaoLogin, 300)
  const facebookLogin = useDebounce(onFacebookLogin, 300)
  const close = useDebounce(onCloseLogin, 300)
  const resetPassword = useDebounce(onResetPassword, 300)
  const imageBackground = useMemo(() => {
    return {
      borderRadius: '50%',
      width: '130px',
      height: '130px',
      display: 'grid',
      placeItems: 'center',
      backgroundColor: 'var(--greyColor4)'
    }
  }, [])
  const onLeaveFocusLogin = useCallback((e) => {
    if (!loginBox.current) return
    if (!loginBox.current.contains(e.target)) {
      onCloseLogin()
    }
  }, [])
  useEffect(() => {
    setMessage('')
  }, [password, password2, email])
  useEffect(() => {
    if (status) {
      const { signupStatus, loginStatus } = status
      if (signupStatus === STATUS.ERROR) {
        setMessage(msg)
      }
      if (loginStatus === STATUS.ERROR) {
        setMessage(msg)
      }
      if (loginStatus === STATUS.DONE) {
        if (visitCount === 1) {
          router.push('/settings')
        }
      }
    }
  }, [status])
  useEffect(() => {
    if (emailInputRef.current) {
      emailInputRef.current.focus()
    }
  }, [isLoginForm])
  useEffect(() => {
    setMessage('')
  }, [])
  return (
    <Backdrop onClick={onLeaveFocusLogin}>
      <Modal ref={loginBox}>
        <Cover>
          <div style={imageBackground}>
            <Image src="/whale.png" width="110" height="110" />
          </div>
        </Cover>
        <Container>
          <Close onClick={close}>✖</Close>
          <LoginBox>
            <h4>{subject}</h4>
            <h5>{`이메일로 ${subject}`}</h5>
            {isLoginForm && (
              <LoginForm onSubmit={onClickEmailLogin}>
                <GridBox
                  style={{
                    gridTemplateRows: '2rem 2rem auto',
                    gridTemplateColumns: '1fr',
                    gridGap: '0.2rem'
                  }}
                >
                  <Input
                    isBorder
                    ref={emailInputRef}
                    value={email}
                    onChange={onChangeEmail}
                    type="email"
                    name="email"
                    autoComplete="off"
                    placeholder="이메일을 입력하세요"
                    onClear={onClearEmail}
                    required
                    isClear
                    style={{ padding: '0.3rem 0' }}
                  />
                  <Input
                    isBorder
                    value={password}
                    onChange={onChangePassword}
                    type="password"
                    name="password"
                    autoComplete="off"
                    placeholder="비밀번호를 입력하세요"
                    onClear={onClearPassword}
                    required
                    isClear
                    style={{ padding: '0.3rem 0' }}
                  />
                  {message && (
                    <span
                      style={{ color: 'var(--redColor)', marginBottom: '1rem' }}
                    >
                      {message}
                    </span>
                  )}
                  <GridBoxWrapper>
                    <Button
                      width100
                      isBorder
                      borderRadius="5px"
                      onClick={resetPassword}
                      style={{
                        fontSize: '1rem',
                        padding: '0.5rem 1rem',
                        color: 'var(--everGreenColor)'
                      }}
                      type="button"
                    >
                      비밀번호변경
                    </Button>
                    <Button
                      width100
                      borderRadius="5px"
                      style={{
                        fontSize: '1rem',
                        padding: '0.5rem 1rem',
                        backgroundColor: 'var(--everGreenColor)',
                        color: 'var(--whiteColor)'
                      }}
                      type="submit"
                    >
                      로그인
                    </Button>
                  </GridBoxWrapper>
                </GridBox>
              </LoginForm>
            )}
            {!isLoginForm && (
              <LoginForm onSubmit={onClickSignup}>
                <GridBox
                  style={{
                    gridTemplateRows: '2rem 2rem 2rem auto',
                    gridTemplateColumns: '1fr',
                    gridGap: '0.2rem'
                  }}
                >
                  <Input
                    isBorder
                    ref={emailInputRef}
                    value={email}
                    onChange={onChangeEmail}
                    type="email"
                    autoComplete="off"
                    placeholder="이메일을 입력하세요"
                    onClear={onClearEmail}
                    required
                    isClear
                    style={{ padding: '0.3rem 0' }}
                  />
                  <Input
                    isBorder
                    value={password}
                    onChange={onChangePassword}
                    type="password"
                    autoComplete="off"
                    placeholder="비밀번호를 입력하세요"
                    onClear={onClearPassword}
                    required
                    isClear
                    style={{ padding: '0.3rem 0' }}
                  />
                  <Input
                    isBorder
                    value={password2}
                    onChange={onChangePassword2}
                    type="password"
                    autoComplete="off"
                    placeholder="비밀번호확인"
                    onClear={onClearPassword2}
                    required
                    isClear
                    style={{ padding: '0.3rem 0' }}
                  />
                  {message && (
                    <span
                      style={{ color: 'var(--redColor)', marginBottom: '1rem' }}
                    >
                      {message}
                    </span>
                  )}
                  <Button
                    type="submit"
                    isBorder
                    borderRadius="5px"
                    style={{
                      justifySelf: 'end',
                      fontSize: '1rem',
                      padding: '0.5rem 1rem',
                      backgroundColor: 'var(--everGreenColor)',
                      color: 'var(--whiteColor)'
                    }}
                  >
                    회원가입
                  </Button>
                </GridBox>
              </LoginForm>
            )}

            <h5>{`소셜 계정으로 ${subject}`}</h5>
            <SNSLogins>
              <button
                type="button"
                style={{ backgroundColor: 'var(--blackColor)' }}
                onClick={githubLogin}
              >
                <CustomFAIcon icon={faGithub} />
              </button>
              <button
                type="button"
                style={{ backgroundColor: 'var(--redColor)' }}
                onClick={googleLogin}
              >
                <CustomFAIcon icon={faGoogle} />
              </button>
              <button
                type="button"
                style={{ backgroundColor: 'var(--facebookBg)' }}
                onClick={facebookLogin}
              >
                <CustomFAIcon icon={faFacebook} />
              </button>
              <button
                type="button"
                style={{ backgroundColor: 'var(--yellowColor)' }}
                onClick={kakaoLogin}
              >
                <Image src="/kakao.svg" width="35" height="35" />
              </button>
            </SNSLogins>
          </LoginBox>
          <Signup>
            {isLoginForm && '아직 회원이 아니신가요?'}
            {!isLoginForm && '계정이 이미 있으신가요?'}
            <button
              type="button"
              onClick={onClickSignupLogin(isLoginForm)}
              style={{
                backgroundColor: 'white',
                color: 'var(--everGreenColor)',
                border: 'none',
                cursor: 'pointer',
                fontSize: '1.2rem'
              }}
            >
              {isLoginForm ? SUBJECT.SIGNUP : SUBJECT.LOGIN}
            </button>
          </Signup>
        </Container>
      </Modal>
    </Backdrop>
  )
}

export default Login
