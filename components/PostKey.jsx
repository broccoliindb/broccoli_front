import React, { useCallback, useRef, useEffect, useState, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { faLock, faGlobeAsia } from '@fortawesome/free-solid-svg-icons'
import Viewer from './libs/Viewer'
import { STATUS, TYPES } from '../reducers/rootReducer'
import '@toast-ui/editor/dist/toastui-editor-viewer.css'
import Layout from './Layout'
import GridBox from './controls/GridBox'
import getLightTime from '../libs/CustomDate'
import LikeButton from './controls/LikeButton'
import Button from './controls/Button'
import Comment from './Comment'
import Reply from './Reply'
import useInput from '../hooks/useInput'
import FlexBox from './controls/FlexBox'
import SeriesListOnPost from './SeriesListOnPost'
import { addIcon, CustomFAIcon } from '../libs/FontAwesome'
import MessageBox from './controls/MessageBox'
import useDebounce from '../hooks/useDebounce'

addIcon(faLock)
addIcon(faGlobeAsia)

const Container = styled.div`
  padding: 1rem;
`
const NotFoundContent = styled.div`
  place-items: center;
  font-size: 3rem;
  line-height: 3rem;
  a {
    h2 {
      &:hover {
        color: var(--everGreenColor);
      }
    }
  }
  @media only screen and (max-width: 600px) {
    font-size: 2rem;
  }
`
const ContentHeader = styled(GridBox)`
  grid-template-columns: auto 1fr auto;
  grid-template-areas: 'author date .';
  grid-gap: 0.5rem;
  margin-bottom: 1rem;
  div,
  span {
    align-self: center;
    justify-self: start;
  }
  span.author {
    grid-area: author;
    font-weight: bold;
    font-size: 1.2rem;
    justify-self: start;
  }
  span.date {
    grid-area: date;
    color: var(--greyColor5);
  }
  @media only screen and (max-width: 600px) {
    grid-template-columns: 1fr auto;
    grid-template-rows: 1fr 1fr;
    grid-template-areas: 'author author' 'date .';
  }
`
const GridBoxHeader = styled(GridBox)`
  grid-gap: 0.5rem;
  grid-template-columns: auto 1fr;
  color: var(--greyColor5);
`
const ButtonGridBox = styled(GridBox)`
  grid-gap: 0.3rem;
  grid-template-columns: 1fr auto;
  button {
    color: var(--everGreenColor);
    justify-self: end;
    padding: 0.5rem;
    margin: 0.1rem;
  }
  @media only screen and (max-width: 600px) {
    button {
      width: max-content;
    }
  }
`
const HashTagButton = styled(Button)`
  color: var(--whiteColor);
  background-color: var(--everGreenColor);
  margin: 0 0.3rem 0.3rem 0;
  padding: 0.2rem;
  border-radius: 5px;
  width: max-content;
`

const FlexBoxWrapper = styled(FlexBox)`
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
`
const CommentContainer = styled.div`
  display: grid;
  grid-gap: 0.5rem;
  margin-bottom: 1rem;
`
const PostKey = () => {
  const {
    status: postStatus,
    mainPost,
    postsOnSeries: postMetaList
  } = useSelector((state) => state.post)
  const { removePostStatus, toastStatus, getPostStatus } = postStatus
  const [showMessageBoxForReply, setShowMessageBoxForReply] = useState(false)
  const [showMessageBoxForPost, setShowMessageBoxForPost] = useState(false)
  const [viewer, setViewer] = useState(null)
  const { comments } = useSelector((state) => state.comment)
  const { me, siteOwner } = useSelector((state) => state.user)
  const { showLoginModal } = useSelector((state) => state.user.modals)
  const {
    Liker,
    Hashtags,
    User,
    id,
    scope,
    isUse,
    updatedAt,
    series,
    title,
    content: mainContent,
    markdown: mainMarkdown
  } = mainPost
  const loginId = me?.id
  const isPicked = Liker.length > 0 && Liker.find((i) => i.id === loginId)
  const [isLiked, setIsLiked] = useState(isPicked)
  const [deleteCommentId, setDeleteCommentId] = useState(null)
  const [deleteCommentChilds, setDeleteCommentChilds] = useState(0)
  const [content, onChangeContent, setContent] = useInput('')
  const dispatch = useDispatch()
  const ref = useRef(null)
  const router = useRouter()
  const { postKey, user } = router.query
  const owner = useMemo(() => {
    if (siteOwner) return siteOwner
    if (me) return me
    return {
      nickname: '',
      avatar: '',
      defaultavatar: '',
      blogname: ''
    }
  }, [siteOwner, me])
  const onModifyPost = useCallback(() => {
    router.push(`/post?key=${postKey}`)
  }, [])
  const onToggleLike = useCallback(() => {
    if (!me && !showLoginModal) {
      dispatch({ type: TYPES.SHOW_LOGIN_MODAL })
      return
    }
    if (Liker.find((i) => i.id === loginId)) {
      dispatch({ type: TYPES.UNLIKE_POST_REQUREST, data: id })
    } else {
      dispatch({ type: TYPES.LIKE_POST_REQUREST, data: id })
    }
  }, [mainPost, showLoginModal, me])
  const checkLoginUserIsAuthor = useCallback(() => {
    const loginUserId = loginId
    const authorId = User?.id
    return loginUserId && authorId && loginUserId === authorId
  }, [mainPost, me])
  const onLoad = useCallback((instance) => {
    ref.current = instance
    setViewer(ref.current.viewerInst)
  }, [])
  const onSaveComment = useCallback(() => {
    if (!content) return
    dispatch({
      type: TYPES.ADD_COMMENT_REQUEST,
      data: { postId: id, content, upperId: null }
    })
    setContent('')
  }, [content])
  const getIcon = useCallback(() => {
    if (scope === 'A') {
      return faGlobeAsia
    }
    return faLock
  }, [mainPost])
  const onLoadRemoveMessageBox = useCallback(
    (commentId, childLength) => () => {
      setDeleteCommentId(commentId)
      setDeleteCommentChilds(childLength)
      setShowMessageBoxForReply(true)
    },
    []
  )
  const onCancelDeleteReply = useCallback(() => {
    setShowMessageBoxForReply(false)
  }, [])
  const onDeleteReply = useCallback(() => {
    dispatch({
      type: TYPES.REMOVE_COMMENT_REQUEST,
      data: { id: deleteCommentId, childLength: deleteCommentChilds }
    })
    setDeleteCommentId(null)
    setDeleteCommentChilds(0)
    setShowMessageBoxForReply(false)
  }, [deleteCommentId, deleteCommentChilds])
  const onLoadDeletePost = useCallback(() => {
    setShowMessageBoxForPost(true)
  }, [])
  const onCancelDeletePost = useCallback(() => {
    setShowMessageBoxForPost(false)
  }, [])
  const onDeletePost = useCallback(() => {
    dispatch({ type: TYPES.REMOVE_POST_REQUEST, data: postKey })
    setShowMessageBoxForPost(false)
  }, [postKey])
  const onHashClick = useCallback((e) => {
    if (!e.target.innerText) return
    if (user) {
      router.push(
        `/${user}/hash?value=${encodeURIComponent(e.target.innerText)}`
      )
      return
    }
    router.push(`/hash?value=${encodeURIComponent(e.target.innerText)}`)
  }, [])
  const hashClick = useDebounce(onHashClick, 300)
  const modifyPost = useDebounce(onModifyPost, 300)
  const deletePost = useDebounce(onDeletePost, 300)
  const deleteReply = useDebounce(onDeleteReply, 300)
  useEffect(() => {
    if (removePostStatus === STATUS.DONE && toastStatus === STATUS.DONE) {
      if (user) {
        router.replace(`/${user}`)
      } else {
        router.replace('/')
      }
    }
  }, [postStatus])
  useEffect(() => {
    if (Liker.find((i) => i.id === loginId)) {
      setIsLiked(true)
    } else {
      setIsLiked(false)
    }
    if (!ref.current) return
    if (viewer) {
      viewer.setMarkdown(mainMarkdown)
    }
  }, [mainPost, isLiked, me])
  return (
    <Layout siteOwner={owner}>
      <Container>
        <>
          {!isUse && getPostStatus === STATUS.DONE && (
            <NotFoundContent>
              <h1>찾으시는 포스트가 없습니다.😭😭</h1>
              <Link href="/">
                <a>
                  <h2>🏠홈으로 돌아가기</h2>
                </a>
              </Link>
            </NotFoundContent>
          )}
          {isUse && (
            <>
              <div>
                <div>
                  {checkLoginUserIsAuthor() && (
                    <ButtonGridBox>
                      <Button borderRadius="10px" onClick={modifyPost}>
                        수정
                      </Button>
                      <Button
                        borderRadius="10px"
                        style={{
                          color: 'var(--everGreenColor)'
                        }}
                        onClick={onLoadDeletePost}
                      >
                        삭제
                      </Button>
                    </ButtonGridBox>
                  )}
                </div>
                <ContentHeader>
                  <span className="author">{User?.nickname}</span>
                  <GridBoxHeader className="date">
                    <span>{`마지막 수정 : ${getLightTime(updatedAt)}`}</span>
                    <span>
                      <CustomFAIcon icon={getIcon()} />
                    </span>
                  </GridBoxHeader>
                  <LikeButton
                    likeCount={Liker.length || 0}
                    isLiked={isLiked}
                    onClick={onToggleLike}
                  />
                </ContentHeader>
                {Hashtags && (
                  <FlexBoxWrapper>
                    {Hashtags.map((hashtag) => (
                      <HashTagButton onClick={hashClick} key={hashtag.id}>
                        {hashtag.tag}
                      </HashTagButton>
                    ))}
                  </FlexBoxWrapper>
                )}
                {postMetaList && postMetaList.length > 0 && (
                  <SeriesListOnPost
                    postMetaList={postMetaList}
                    seriesName={series}
                  />
                )}

                <>
                  <h1>{title}</h1>
                  {id > -1 && (
                    <Viewer initialValue={mainContent} onLoad={onLoad} />
                  )}
                </>
              </div>
              <div>
                <CommentContainer>
                  <Comment
                    isLoggedIn={!!me}
                    content={content}
                    onChangeContent={onChangeContent}
                    onSaveComment={onSaveComment}
                  />
                  {comments && comments.length > 0 && (
                    <>
                      {comments.map((comment) => (
                        <Reply
                          key={comment.id}
                          id={comment.id}
                          lvl={comment.lvl}
                          postId={id}
                          upperId={comment.id}
                          writer={comment.nickname}
                          writerId={comment.userId}
                          avatar={comment.avatar || comment.defaultavatar}
                          content={comment.content}
                          childComments={comment.childComments}
                          writtenDate={comment.updatedAt}
                          isLoggedIn={!!me}
                          onLoadRemoveMessageBox={onLoadRemoveMessageBox}
                          onDeleteReply={onDeleteReply}
                        />
                      ))}
                    </>
                  )}
                </CommentContainer>
              </div>
            </>
          )}
        </>
        {showMessageBoxForReply && (
          <MessageBox
            onCancel={onCancelDeleteReply}
            onOK={deleteReply}
            title="댓글삭제"
            message="댓글을 삭제하시겠습니까?"
          />
        )}
        {showMessageBoxForPost && (
          <MessageBox
            onCancel={onCancelDeletePost}
            onOK={deletePost}
            title="포스트삭제"
            message="포스트를 삭제하시겠습니까?"
          />
        )}
      </Container>
    </Layout>
  )
}

export default PostKey
