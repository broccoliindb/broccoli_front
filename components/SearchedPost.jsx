import React, { useCallback } from 'react'
import styled, { keyframes } from 'styled-components'
import Link from 'next/link'
import { useSelector } from 'react-redux'
import PropTypes from 'prop-types'
import Avatar from './controls/Avatar'
import getLightTime from '../libs/CustomDate'

const scale = keyframes`
  from {
    transform: scale(1)
  }
  to {
    transform: scale(1.01)
  }
`

const Container = styled.div`
  display: grid;
  font-family: 'Nanum Gothic';
  grid-template-rows: 1fr auto;
  box-shadow: rgb(0 0 0 / 20%) 0px 2px 10px;
  &:hover {
    box-shadow: rgb(0 0 0 / 70%) 0px 2px 10px;
    animation: ${scale} 0.05s linear forwards;
  }
  @media only screen and (max-width: 600px) {
    &:hover {
      box-shadow: none;
      animation: none;
    }
  }
`
const User = styled.div`
  background-color: var(--whiteColor);
  padding: 0.5rem;
  display: grid;
  grid-template-columns: auto 1fr;
  grid-gap: 0.5rem;
`
const Contents = styled.div`
  display: grid;
  height: 100%;
  grid-template-rows: 1fr auto;
  padding: 0.5rem;
  @media only screen and (max-width: 600px) {
    &:hover {
      background-color: transparent;
    }
  }
`
const Thumbnail = styled.div`
  background: url(${(props) => props.url});
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center;
  max-width: 100%;
  height: 10rem;
  display: grid;
  margin-bottom: 1rem;
`

const Anchor = styled.a`
  cursor: pointer;
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: ${(props) => (props.isThumbnail ? 'auto 1fr' : '1fr')};
  padding: 1rem 0;
`
const Paragraph = styled.p`
  padding: 0;
  line-height: 1.3rem;
  overflow: hidden;
  word-break: break-all;
  max-height: ${(props) => (props.isThumbnail ? '5rem' : '25rem')};
  display: -webkit-box;
  -webkit-line-clamp: ${(props) => (props.isThumbnail ? 4 : 13)};
  -webkit-box-orient: vertical;
  @media only screen and (max-width: 600px) {
    -webkit-line-clamp: 4;
  }
`
const Title = styled.h2`
  word-break: break-all;
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
`
const ContentsSummary = styled.div`
  display: grid;
  grid-template-rows: auto auto 1fr;
`
const SearchedPost = ({ post, isMyDocs, termTokens }) => {
  const {
    title,
    updatedat,
    content,
    postkey,
    thumbnail,
    userId,
    avatar,
    nickname
  } = post
  const tokens = termTokens.map((i) => i.token)
  const reg = new RegExp(`${tokens.join('|')}`, 'gi')
  const replaceWithSpan = useCallback((match) => {
    return `<span class='term'>${match}</span>`
  }, [])
  const createdMarkUp = (words) => {
    const changed = words.replace(reg, replaceWithSpan)
    const index = changed.indexOf('<span')
    const newMarkUp =
      index !== -1 && index >= 200
        ? `...${changed.substring(index - 200)}`
        : changed
    return {
      __html: newMarkUp
    }
  }
  const me = useSelector((state) => state.user.me)
  const getTitle = useCallback(() => {
    return <Title dangerouslySetInnerHTML={createdMarkUp(title)} />
  }, [])
  const getThumbnail = useCallback(() => {
    if (!thumbnail) return
    return <Thumbnail url={thumbnail} />
  }, [])
  const getUpdatedAt = useCallback(() => {
    return (
      <div style={{ marginBottom: '1rem' }}>
        <span>최종수정: </span>
        {getLightTime(updatedat)}
      </div>
    )
  }, [])
  const getContentsEclipse = useCallback(() => {
    const getContent = (rawContent) => {
      if (!rawContent) return
      const tagRemoved = rawContent.replace(
        /<(\/)?([a-zA-Z0-9]*)(\s[a-zA-Z0-9-_]*=[^>]*)?(\s)*(\/)?>/g,
        ''
      )
      return (
        <Paragraph
          isThumbnail={!!thumbnail}
          dangerouslySetInnerHTML={createdMarkUp(tagRemoved)}
        />
      )
    }
    return getContent(content)
  }, [content, thumbnail])
  const getWriterInfo = useCallback(() => {
    if (isMyDocs && me && me.nickname === nickname) {
      return null
    }
    return (
      <User>
        <Avatar width={25} height={25} imgPath={avatar} />
        <span style={{ alignSelf: 'center' }}>{nickname}</span>
      </User>
    )
  }, [me])
  const getLinkHref = useCallback(() => {
    return `/post/${postkey}?authorId=${userId}`
  }, [userId])
  return (
    <Container>
      <Contents>
        <Link href={getLinkHref()}>
          <Anchor isThumbnail={!!thumbnail}>
            {getThumbnail()}
            <ContentsSummary>
              {getTitle()}
              {getUpdatedAt()}
              {getContentsEclipse()}
            </ContentsSummary>
          </Anchor>
        </Link>
      </Contents>
      {getWriterInfo()}
    </Container>
  )
}

SearchedPost.propTypes = {
  isMyDocs: PropTypes.bool,
  termTokens: PropTypes.array,
  post: PropTypes.shape({
    title: PropTypes.string,
    nickname: PropTypes.string,
    updatedat: PropTypes.string,
    content: PropTypes.string,
    postkey: PropTypes.string,
    thumbnail: PropTypes.string,
    avatar: PropTypes.string,
    userId: PropTypes.number
  }).isRequired
}

SearchedPost.defaultProps = {
  isMyDocs: false,
  termTokens: []
}

export default SearchedPost
