import React, { useEffect, useMemo, useState, useCallback } from 'react'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import Tabs from './controls/Tabs'

const Container = styled.div``

const BlogTabs = () => {
  const router = useRouter()
  const { query, asPath } = router
  const user = query?.user
  const items = useMemo(
    () => [
      { displayName: '글', link: `/${user}` },
      {
        displayName: '시리즈',
        link: `/${user}/series`
      },
      {
        displayName: '소개',
        link: `/${user}/about`
      },
      {
        displayName: '토이프로젝트',
        link: `/${user}/toy`
      }
    ],
    [user]
  )
  const [tabItems, setTabItems] = useState(items)
  const selected = items.find((i) => i.link === decodeURIComponent(asPath))
  const [selectedItem, setSelectedItem] = useState(selected)
  const onSelectedItem = useCallback(
    (item) => () => {
      setSelectedItem(item)
    },
    []
  )
  useEffect(() => {
    setTabItems(items)
  }, [items])
  return (
    <Container>
      <Tabs
        itemsSource={tabItems}
        selectedItem={selected}
        onSelectedItem={onSelectedItem}
      />
    </Container>
  )
}

export default BlogTabs
