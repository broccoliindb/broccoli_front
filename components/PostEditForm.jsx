import React, { useCallback, useState, useRef, useEffect } from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'
import styled from 'styled-components'
import { useDispatch, useSelector } from 'react-redux'
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import Editor from './libs/Editor'
import Input from './controls/Input'
import Button from './controls/Button'
import GridBox from './controls/GridBox'
import FlexBox from './controls/FlexBox'
import { TYPES, STATUS } from '../reducers/rootReducer'
import { addIcon, CustomFAIcon } from '../libs/FontAwesome'
import useDebounce from '../hooks/useDebounce'
import PublishPost from './PublishPost'
import getImage, { UPLOAD_TYPE } from '../libs/uploadImage'
import 'codemirror/lib/codemirror.css'
import '@toast-ui/editor/dist/toastui-editor.css'
import '@toast-ui/editor/dist/toastui-editor-viewer.css'

addIcon(faArrowLeft)

const Container = styled.div`
  display: grid;
  grid-template-rows: auto auto 1fr auto;
  height: 100vh;
`
const Exit = styled.button`
  border: none;
  cursor: pointer;
  border-radius: 15px;
  background-color: var(--whiteColor);
  padding: 0.5rem;
  font-size: 1.3rem;
  &:hover {
    background-color: var(--greyColor3);
  }
  @media only screen and (max-width: 600px) {
    font-size: 1rem;
  }
`
const ButtonGridBox = styled(GridBox)`
  margin: 1rem 0;
  grid-gap: 0.3rem;
  padding: 0 1rem;
  grid-template-columns: 1fr auto auto;
`
const FlexBoxWrapper = styled(FlexBox)`
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  line-height: 1.5rem;
  padding: 0 1rem;
`
const HashTagButton = styled(Button)`
  color: var(--whiteColor);
  background-color: var(--everGreenColor);
  padding: 0.2rem;
  margin-right: 0.2rem;
  border-radius: 5px;
  width: max-content;
`
const getTempKey = () => {
  return Date.now()
}
const PostEditForm = () => {
  const dispatch = useDispatch()
  const router = useRouter()
  const [isLoaded, setLoaded] = useState(false)
  const { query } = router
  const [tag, setTag] = useState('')
  const [deletedTags, setDeletedTags] = useState([])
  const key = query?.key
  const { status: postStatus, mainPost } = useSelector((state) => state.post)
  const { me } = useSelector((state) => state.user)
  const [isSavingPost, setIsSavingPost] = useState(false)
  const nickname = me?.nickname
  const { addPostStatus, tempSaveStatus, getTempPostStatus, getPostStatus } =
    postStatus
  const {
    title: mainTitle,
    markdown: mainMarkdown,
    content: mainContent,
    tempTitle,
    tempMarkdown,
    tempContent,
    Hashtags,
    isTemp,
    isUse,
    thumbnail,
    id: mainId
  } = mainPost
  const [titleValue, setTitleValue] = useState(
    (key && ((isUse && mainTitle) || (!isUse && isTemp && tempTitle))) || ''
  )
  const [htmlValue, setHtml] = useState(
    (key && ((isUse && mainContent) || (!isUse && isTemp && tempContent))) || ''
  )
  const [markdownValue, setMarkdown] = useState(
    (key && ((isUse && mainMarkdown) || (!isUse && isTemp && tempMarkdown))) ||
      ''
  )
  const [thumbnailImage, setThumbnailImage] = useState(thumbnail || '')
  const [tags, setTags] = useState((key && Hashtags) || [])
  const ref = useRef(null)
  const interval = useRef()
  const checkValidation = useCallback((titleVal, mdVal) => {
    if (titleVal && mdVal) return true
    if (!titleVal) {
      dispatch({
        type: TYPES.TEMP_SAVE_POST_REJECTED,
        error: { message: '타이틀이 없습니다.' }
      })
    } else if (!mdVal) {
      dispatch({
        type: TYPES.TEMP_SAVE_POST_REJECTED,
        error: { message: '본문이 없습니다.' }
      })
    }
    return false
  }, [])
  const onCancelSavePost = useCallback(() => {
    setIsSavingPost(false)
  }, [])
  const onClickSavePost = useCallback(() => {
    if (!checkValidation(titleValue, markdownValue)) return
    if (!ref.current) return
    const editor = ref.current.getInstance()
    setHtml(editor.getHtml())
    setIsSavingPost(true)
  }, [titleValue, htmlValue, markdownValue])
  const onChangeTitle = useCallback((e) => {
    setTitleValue(() => e.target.value)
  }, [])
  const onChangeEditValue = useCallback((htmlVal, mdVal) => {
    setMarkdown(() => mdVal)
  }, [])
  const onClickTempSavePost = useCallback(() => {
    if (!ref.current) return
    const editor = ref.current.getInstance()
    if (!checkValidation(titleValue, markdownValue)) return
    setHtml(editor.getHtml())
    dispatch({
      type: TYPES.TEMP_SAVE_POST_REQUEST,
      data: {
        id: mainId,
        title: titleValue,
        content: editor.getHtml(),
        markdown: markdownValue,
        tags,
        deletedTags
      }
    })
  }, [titleValue, htmlValue, markdownValue, tags, deletedTags])

  const onLoad = useCallback(
    (instance) => {
      ref.current = instance
      setLoaded(true)
    },
    [htmlValue, markdownValue]
  )
  const onHashClick = useCallback(
    (item) => () => {
      const index = tags.findIndex((i) => i === item)
      setTags((prev) => {
        const copyPrev = [...prev]
        const deleted = copyPrev.splice(index, 1)
        if (deleted.id !== -1) {
          setDeletedTags((p) => [...p, ...deleted])
        }
        return [...copyPrev]
      })
    },
    [tags]
  )
  const onTagChange = useCallback((e) => {
    e.preventDefault()
    setTag(e.target.value)
  }, [])
  const onTagKeyDown = useCallback(
    (e) => {
      if (e.code === 'Enter' && tag) {
        if (tag.length > 20) {
          dispatch({
            type: TYPES.ADD_HASHTAG_REJECTED,
            error: { message: '태그는 20자 이하로 작성해주세요' }
          })
          setTag('')
          return
        }
        setTags((prev) => {
          if (prev.find((i) => i.tag === tag)) return prev
          return [...prev, { key: getTempKey(), id: -1, tag }]
        })
        setTag('')
      }
      if (tags.length > 0 && e.code === 'Backspace') {
        if (tag.length === 0) {
          setTags((prev) => {
            const copyPrev = [...prev]
            const deleted = copyPrev.splice(-1, 1)
            if (deleted.id !== -1) {
              setDeletedTags((p) => [...p, ...deleted])
            }
            return [...copyPrev]
          })
        }
      }
    },
    [tag, tags, deletedTags]
  )
  const savePost = useDebounce(onClickSavePost, 300)
  const tempSavePost = useDebounce(onClickTempSavePost, 300)
  const uploadImage = useCallback(async (blob) => {
    try {
      if (!blob) return
      const image = await getImage(blob, UPLOAD_TYPE.POST)
      setThumbnailImage(image.data.src)
      return image
    } catch (err) {
      if (err) {
        console.error(err)
      }
    }
  }, [])

  const getExitPath = useCallback(() => {
    if (!key) {
      return `/@${nickname}`
    }
    if (isTemp && !isUse) {
      return `/@${nickname}`
    }
    return `/@${nickname}/${key}`
  }, [key, mainPost])
  useEffect(() => {
    if (!nickname) {
      router.replace('/')
    }
  }, [nickname])
  useEffect(() => {
    if (!isLoaded) return
    if (isSavingPost) {
      clearInterval(interval.current)
      return
    }
    interval.current = setInterval(() => {
      onClickTempSavePost()
    }, 10000)
    return () => {
      clearInterval(interval.current)
    }
  }, [
    isLoaded,
    isSavingPost,
    titleValue,
    htmlValue,
    markdownValue,
    tags,
    deletedTags
  ])
  useEffect(() => {
    if (!ref.current) return
    const editor = ref.current.getInstance()
    setThumbnailImage(thumbnail)
    if (key && mainId !== -1 && tempSaveStatus !== STATUS.ERROR) {
      if (getTempPostStatus === STATUS.DONE || tempSaveStatus === STATUS.DONE) {
        setTitleValue(tempTitle)
        setMarkdown(tempMarkdown)
        setHtml(tempContent)
        setTags(Hashtags)
        editor.setHtml(tempContent)
        editor.setMarkdown(tempMarkdown)
      } else if (
        isTemp &&
        !isUse &&
        getPostStatus === STATUS.DONE &&
        tempSaveStatus !== STATUS.LOADING
      ) {
        setTitleValue(tempTitle)
        setMarkdown(tempMarkdown)
        setHtml(tempContent)
        setTags(Hashtags)
        editor.setHtml(tempContent)
        editor.setMarkdown(tempMarkdown)
      } else if (
        isUse &&
        getPostStatus === STATUS.DONE &&
        tempSaveStatus !== STATUS.LOADING
      ) {
        setTitleValue(mainTitle)
        setMarkdown(mainMarkdown)
        setHtml(mainContent)
        setTags(Hashtags)
        editor.setHtml(mainContent)
        editor.setMarkdown(mainMarkdown)
      }
    }
  }, [postStatus, key, mainPost, isLoaded])
  return (
    <Container>
      <Input
        style={{ padding: '1rem 0 0 1rem' }}
        value={titleValue}
        onChange={onChangeTitle}
        placeholder="제목을 입력하세요"
        isHeader
      />
      <FlexBoxWrapper>
        {tags.map((hashtag) => (
          <HashTagButton
            onClick={onHashClick(hashtag)}
            key={hashtag.key || hashtag.id}
          >
            <span>{hashtag.tag}</span>
            <span color="var(--redColor)"> ✗</span>
          </HashTagButton>
        ))}
        <Input
          value={tag}
          onChange={onTagChange}
          onKeyDown={onTagKeyDown}
          placeholder="태그를 입력하세요. 엔터키로 구분가능합니다."
        />
      </FlexBoxWrapper>
      <Editor
        placeholder="당신의 이야기를 적어보세요..."
        initialValue={markdownValue}
        previewStyle="vertical"
        initialEditType="markdown"
        height="100%"
        usageStatistics={false}
        useCommandShortcut
        onChange={onChangeEditValue}
        onLoad={onLoad}
        hooks={{
          addImageBlobHook: async (blob, callback) => {
            const file = await uploadImage(blob)
            callback(file.data.src, `${file.data.name}`)
            return false
          }
        }}
      />
      {isLoaded && (
        <ButtonGridBox>
          <Exit>
            <Link href={getExitPath()}>
              <a>
                <CustomFAIcon icon={faArrowLeft} />
                <span> 나가기</span>
              </a>
            </Link>
          </Exit>
          <Button
            isBorder
            style={{
              color: 'var(--everGreenColor)',
              borderRadius: '5px',
              padding: '0.5rem'
            }}
            onClick={tempSavePost}
            type="button"
          >
            임시저장
          </Button>
          <Button
            type="button"
            onClick={savePost}
            style={{
              backgroundColor: 'var(--everGreenColor)',
              color: 'var(--whiteColor)',
              borderRadius: '5px',
              padding: '0.5rem'
            }}
          >
            저장
          </Button>
        </ButtonGridBox>
      )}
      {isSavingPost && addPostStatus !== STATUS.DONE && (
        <PublishPost
          prevPost={mainPost}
          titleValue={titleValue}
          htmlValue={htmlValue}
          markdownValue={markdownValue}
          thumbnail={thumbnailImage}
          tags={tags}
          deletedTags={deletedTags}
          onCancel={onCancelSavePost}
        />
      )}
    </Container>
  )
}

export default PostEditForm
