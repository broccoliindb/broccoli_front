import React, { useCallback, useMemo, useState } from 'react'
import styled from 'styled-components'
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
  faBookOpen,
  faCaretLeft,
  faCaretRight
} from '@fortawesome/free-solid-svg-icons'
import PropTypes from 'prop-types'
import { addIcon, CustomFAIcon } from '../libs/FontAwesome'
import Button from './controls/Button'
import GridBox from './controls/GridBox'

addIcon(faBookOpen)
addIcon(faCaretRight)
addIcon(faCaretLeft)

const Container = styled.div`
  position: relative;
  display: grid;
  grid-template-rows: auto 1fr;
  grid-gap: 1rem;
  padding: 1rem;
  margin-bottom: 1rem;
  background-color: var(--greyColor2);
`
const CustomFAIconWapperBook = styled(CustomFAIcon)`
  position: absolute;
  top: 1rem;
  right: 1rem;
  font-size: 3rem;
  color: var(--everGreenColor);
`
const Options = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  line-height: 1.4rem;
`
const GridBoxWrapper = styled(GridBox)`
  width: max-content;
  grid-template-columns: auto auto auto;
  grid-gap: 0.5rem;
`
const ButtonWrapper = styled(Button)`
  background-color: transparent;
  color: var(--greyColor5);
  width: max-content;
`
const Arrow = styled(Button)`
  display: grid;
  place-items: center;
  font-weight: bold;
  width: 1.4rem;
  height: 1.4rem;
  border-radius: 50%;
  font-size: 1.4rem;
  line-height: 1.4rem;
  opacity: ${(props) => (props.disable ? '0.8' : '1')};
  background-color: var(--greyColor3);
  cursor: ${(props) => (props.disable ? 'inherit' : 'pointer')};
  color: ${(props) =>
    props.disable ? 'var(--whiteColor)' : 'var(--everGreenColor)'};
  &:hover {
    opacity: ${(props) => (props.disable ? '0.8' : '1')};
    background-color: ${(props) =>
      props.disable ? 'var(--greyColor3)' : 'var(--everGreenColor)'};
    color: ${(props) =>
      props.disable ? 'var(--whiteColor)' : 'var(--whiteColor)'};
  }
`
const Item = styled.li`
  margin: 0 0 0.8rem 0;
  text-decoration: ${(props) => (props.selected ? 'underline' : 'none')};
  text-underline-position: under;
  font-weight: ${(props) => (props.selected ? 'bold' : 'normal')};
  color: ${(props) =>
    props.selected ? 'var(--everGreenColor)' : 'var(--blackColor)'};
`
const SeriesListOnPost = ({ postMetaList, seriesName }) => {
  const router = useRouter()
  const [isShowSeriesList, setIsShowSeriesList] = useState(false)
  const { user, postKey, authorId } = router.query
  const selectedIndex = postMetaList.findIndex((i) => i.postKey === postKey)
  const POSITION = useMemo(() => {
    return {
      LEFT: 'LEFT',
      RIGHT: 'RIGHT'
    }
  }, [])
  const getItemIndex = useCallback(() => {
    return selectedIndex + 1
  }, [postKey])
  const getDisable = useCallback(
    (position) => {
      if (position === POSITION.LEFT) {
        return selectedIndex === 0
      }
      return selectedIndex === postMetaList.length - 1
    },
    [postKey]
  )
  const onPostMove = useCallback(
    (position) => () => {
      if (!getDisable(position)) {
        let leftPath = null
        let rightPath = null
        if (position === POSITION.LEFT) {
          leftPath = user
            ? `/${user}/${postMetaList[selectedIndex - 1].postKey}`
            : `/post/${
                postMetaList[selectedIndex - 1].postKey
              }?authorId=${authorId}`
          return router.push(leftPath)
        }
        rightPath = user
          ? `/${user}/${postMetaList[selectedIndex + 1].postKey}`
          : `/post/${
              postMetaList[selectedIndex + 1].postKey
            }?authorId=${authorId}`
        return router.push(rightPath)
      }
    },
    [postKey]
  )
  const showSeriesList = useCallback(() => {
    setIsShowSeriesList((prev) => !prev)
  }, [])
  return (
    <Container>
      <CustomFAIconWapperBook icon={faBookOpen} />
      <h3 style={{ marginBottom: '0.5rem' }}>{seriesName}</h3>
      {isShowSeriesList && (
        <ul>
          {postMetaList.map((item, index) => {
            const linkPath = user
              ? `/${user}/${item.postKey}`
              : `/post/${item.postKey}?authorId=${authorId}`
            return (
              <Link key={item.postId} href={linkPath}>
                <a>
                  <Item selected={item.postKey === postKey} key={item.postId}>
                    <span>{`${index + 1}. `}</span>
                    {item.postTitle}
                  </Item>
                </a>
              </Link>
            )
          })}
        </ul>
      )}
      <Options>
        <ButtonWrapper onClick={showSeriesList}>
          <>
            <span>{isShowSeriesList ? '▴' : '▾'}</span>
            <span>{isShowSeriesList ? '숨기기' : '목록보기'}</span>
          </>
        </ButtonWrapper>
        <GridBoxWrapper>
          <span
            style={{
              color: 'var(--everGreenColor)',
              fontWeight: 'bold',
              alignSelf: 'center'
            }}
          >
            {`${getItemIndex()}/${postMetaList.length}`}
          </span>
          <Arrow
            onClick={onPostMove(POSITION.LEFT)}
            disable={getDisable(POSITION.LEFT)}
          >
            ‹
          </Arrow>
          <Arrow
            onClick={onPostMove(POSITION.RIGHT)}
            disable={getDisable(POSITION.RIGHT)}
          >
            ›
          </Arrow>
        </GridBoxWrapper>
      </Options>
    </Container>
  )
}

SeriesListOnPost.propTypes = {
  seriesName: PropTypes.string.isRequired,
  postMetaList: PropTypes.arrayOf(
    PropTypes.shape({
      postId: PropTypes.number,
      postTitle: PropTypes.string,
      postKey: PropTypes.string,
      seriesId: PropTypes.number,
      seriesName: PropTypes.string
    })
  ).isRequired
}

export default SeriesListOnPost
