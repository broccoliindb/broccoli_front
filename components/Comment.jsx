import React, { useEffect, useState, useCallback } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import { TYPES } from '../reducers/rootReducer'
import useDebounce from '../hooks/useDebounce'
import Button from './controls/Button'
import GridBox from './controls/GridBox'

const Container = styled.div`
  display: grid;
  grid-template-rows: 1fr;
`
const Content = styled.textarea`
  width: 100%;
  margin-bottom: 0.5rem;
  border: none;
  box-shadow: rgb(0 0 0 / 5%) 0px 0px 4px;
  border: 1px solid var(--greyColor3);
`
const GridBoxWrapper = styled(GridBox)`
  grid-gap: 0.3rem;
  grid-template-columns: 1fr auto;
`
const ButtonWrapper = styled(Button)`
  background-color: var(--everGreenColor);
  border-radius: 5px;
  color: var(--whiteColor);
  justify-self: end;
  font-size: ${(props) => (props.lvl ? '1rem' : '1.5rem')};
  padding: ${(props) => (props.lvl ? '0.5rem' : '1rem')};
  @media only screen and (max-width: 600px) {
    width: max-content;
  }
`

const Comment = ({
  isUpdate,
  lvl,
  content,
  onCancelComment,
  onChangeContent,
  onSaveComment,
  isLoggedIn
}) => {
  const dispatch = useDispatch()
  const { showLoginModal } = useSelector((state) => state.user.modals)
  const [placeHolderValue, setPlaceHolderValue] = useState(
    isLoggedIn ? '댓글을 작성하세요.' : '로그인을 해주세요'
  )

  const onClickToLogin = useCallback(() => {
    if (!isLoggedIn && !showLoginModal) {
      dispatch({ type: TYPES.SHOW_LOGIN_MODAL })
    }
  }, [isLoggedIn, showLoginModal])
  const login = useDebounce(onClickToLogin, 300)
  useEffect(() => {
    if (isLoggedIn) {
      setPlaceHolderValue('댓글을 작성하세요.')
    } else {
      setPlaceHolderValue('로그인을 해주세요')
    }
  }, [isLoggedIn])
  const cancelComment = useDebounce(onCancelComment, 300)
  const saveComment = useDebounce(onSaveComment, 300)
  return (
    <Container>
      <Content
        placeholder={placeHolderValue}
        value={content}
        onChange={onChangeContent}
        rows={3}
        onClick={login}
      />
      <GridBoxWrapper>
        {isUpdate && (
          <ButtonWrapper lvl={lvl} onClick={cancelComment}>
            취소
          </ButtonWrapper>
        )}
        <ButtonWrapper lvl={lvl} onClick={saveComment}>
          {isUpdate ? '댓글 수정' : '댓글 작성'}
        </ButtonWrapper>
      </GridBoxWrapper>
    </Container>
  )
}

Comment.propTypes = {
  isUpdate: PropTypes.bool,
  lvl: PropTypes.number,
  content: PropTypes.string,
  onCancelComment: PropTypes.func,
  onChangeContent: PropTypes.func.isRequired,
  onSaveComment: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool
}

Comment.defaultProps = {
  isUpdate: false,
  lvl: 0,
  content: '',
  onCancelComment: () => {},
  isLoggedIn: false
}

export default Comment
