import React from 'react'
import PropTypes from 'prop-types'
import { Viewer } from '@toast-ui/react-editor'

const WrappedViewer = (props) => {
  const { forwardedRef } = props
  return <Viewer {...props} ref={forwardedRef} />
}

WrappedViewer.propTypes = {
  forwardedRef: PropTypes.func.isRequired
}

export default WrappedViewer
