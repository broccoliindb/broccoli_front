import React from 'react'
import PropTypes from 'prop-types'
import { Editor } from '@toast-ui/react-editor'

const WrappedEditor = (props) => {
  const { forwardedRef } = props
  return <Editor {...props} ref={forwardedRef} />
}

WrappedEditor.propTypes = {
  forwardedRef: PropTypes.func.isRequired
}

export default WrappedEditor
