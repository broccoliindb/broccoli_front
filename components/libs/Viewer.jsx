import React, { useCallback } from 'react'
import dynamic from 'next/dynamic'
import PropTypes from 'prop-types'

const Viewer = dynamic(() => import('./WrappedViewer'), { ssr: false })

const ViewerWithForwardedRef = React.forwardRef((props, ref) => {
  return <Viewer {...props} forwardedRef={ref} />
})

const Index = (props) => {
  const viewerRef = useCallback((node) => {
    if (node) {
      props.onLoad(node)
      return node
    }
  }, [])
  return <ViewerWithForwardedRef {...props} ref={viewerRef} />
}

Index.propTypes = {
  onLoad: PropTypes.func.isRequired
}

export default Index
