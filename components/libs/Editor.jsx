import React, { useCallback, useState } from 'react'
import dynamic from 'next/dynamic'
import PropTypes from 'prop-types'

const Editor = dynamic(() => import('./WrappedEditor'), { ssr: false })

const EditorWithForwardedRef = React.forwardRef((props, ref) => (
  <Editor {...props} forwardedRef={ref} />
))

const Index = (props) => {
  const [ref, setRef] = useState(null)
  const editorRef = useCallback((node) => {
    if (node) {
      props.onLoad(node)
      setRef(node)
      return node
    }
  }, [])
  const onChange = useCallback(() => {
    if (!ref) return
    const instance = ref.getInstance()
    props.onChange(instance.getHtml(), instance.getMarkdown())
  }, [props, ref])
  return (
    <EditorWithForwardedRef {...props} ref={editorRef} onChange={onChange} />
  )
}

Index.propTypes = {
  onChange: PropTypes.func.isRequired,
  onLoad: PropTypes.func.isRequired
}

export default Index
