import React, {
  useState,
  useCallback,
  useRef,
  useLayoutEffect,
  useEffect
} from 'react'
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import Header from './Header'
import Login from './Login'
import Toast from './controls/Toast'
import ResetPassword from './ResetPassword'
import Sidebar from './Sidebar'
import { REDUCER, STATUS } from '../reducers/rootReducer'
import useThrottle from '../hooks/useThrottle'
import useDebounce from '../hooks/useDebounce'
import Loading from './controls/Loading'

const Container = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: repeat(24, 1fr);
  height: 100vh;
  overflow-y: auto;
  ::-webkit-scrollbar {
    width: 5px;
    visibility: ${(props) => (props.isHeaderShow ? 'visible' : 'hidden')};
  }
`
const Main = styled.main`
  grid-column: 3 / span 20;
  margin-top: 4.5rem;
  @media only screen and (max-width: 600px) {
    grid-column: 1 / -1;
  }
  @media only screen and (min-width: 1200px) {
    grid-column: ${(props) => (props.isHome ? '1/-1' : '6 / span 14;')};
  }
`

const Layout = ({ children, siteOwner }) => {
  const [prevY, setPrevY] = useState(0)
  const [isHeaderShow, setIsHeaderShow] = useState(true)
  const router = useRouter()
  const scrollRef = useRef(null)
  const [toastMessage, setToastMessage] = useState('')
  const [toastType, setToastType] = useState(null)
  const [toastToward, setToastToward] = useState(null)
  const [isReturningTo, setIsReturningTo] = useState(false)
  const showLoginModal = useSelector(
    (state) => state.user.modals.showLoginModal
  )
  const showResetPasswordModal = useSelector(
    (state) => state.user.modals.showResetPasswordModal
  )
  const { pathname, query } = router
  const { isLoggedSuccess } = query

  const { message: userMessage, status: userStatus } = useSelector(
    (state) => state.user
  )
  const { message: postMessage, status: postStatus } = useSelector(
    (state) => state.post
  )
  const { resetPasswordStatus, toastStatus: userToastStatus } = userStatus
  const {
    removePostStatus,
    getPostsStatus,
    toastStatus: postToastStatus
  } = postStatus
  const handleScroll = useCallback(
    (e) => {
      const diff = e.target.scrollTop - prevY
      if (diff > 0) {
        setIsHeaderShow(false)
      } else if (diff < 0) {
        setIsHeaderShow(true)
      }
      setPrevY(e.target.scrollTop)
    },
    [prevY]
  )
  const stopScroll = useCallback((e) => {
    if (e.target.scrollTop === 0) {
      setIsHeaderShow(true)
    } else {
      setIsHeaderShow(false)
    }
  }, [])
  const throttleScroll = useThrottle(handleScroll, 300)
  const debounceScroll = useDebounce(stopScroll, 1500)
  const scrollDetectHandler = useCallback(
    (...e) => {
      throttleScroll(...e)
      debounceScroll(...e)
    },
    [prevY]
  )
  useEffect(() => {
    if (pathname === '/' && isLoggedSuccess) {
      const returnTo = sessionStorage.getItem('returnTo')
      if (returnTo && returnTo !== '/') {
        setToastToward(REDUCER.USER)
        setToastType(STATUS.DONE)
        setToastMessage('이전 페이지로 이동합니다.')
        setIsReturningTo(true)
      } else if (returnTo !== '/') {
        sessionStorage.removeItem('returnTo')
      }
    }
  }, [isLoggedSuccess, pathname])
  useEffect(() => {
    if (removePostStatus === STATUS.DONE) {
      setToastToward(REDUCER.POST)
      setToastType(STATUS.DONE)
      setToastMessage(postMessage)
    }
    // 로그인사용자가 없을때 홈으로 이동한다는 메세지.
    if (getPostsStatus === STATUS.DONE && postMessage) {
      setToastToward(REDUCER.POST)
      setToastType(STATUS.DONE)
      setToastMessage(postMessage)
    }
    if (resetPasswordStatus === STATUS.DONE) {
      setToastToward(REDUCER.USER)
      setToastMessage(userMessage)
      setToastType(STATUS.DONE)
    }
  }, [
    removePostStatus,
    getPostsStatus,
    resetPasswordStatus,
    userMessage,
    postMessage
  ])
  useEffect(() => {
    if (userToastStatus === STATUS.DONE || postToastStatus === STATUS.DONE) {
      setToastMessage('')
      setToastType(null)
      setToastToward(null)
      if (isReturningTo) {
        const returnTo = sessionStorage.getItem('returnTo')
        if (returnTo) {
          sessionStorage.removeItem('returnTo')
          setIsReturningTo(false)
          router.replace(returnTo)
        }
      }
    }
  }, [userToastStatus, postToastStatus, isReturningTo])
  useLayoutEffect(() => {
    if (scrollRef.current) {
      scrollRef.current.addEventListener('scroll', scrollDetectHandler)
    }
    return () => {
      if (!scrollRef.current) return
      scrollRef.current.removeEventListener('scroll', scrollDetectHandler)
    }
  }, [prevY])

  return (
    <Container isHeaderShow={isHeaderShow} ref={scrollRef}>
      <Header isHeaderShow={isHeaderShow} siteOwner={siteOwner} />
      <Sidebar isMyDocs={pathname === '/[user]'} />
      <Main isHome={pathname === '/'} isMyDocs={pathname === '/[user]'}>
        {children}
      </Main>
      {showLoginModal && <Login />}
      {showResetPasswordModal && <ResetPassword />}
      {toastMessage && (
        <Toast
          toastToward={toastToward}
          wait={3000}
          type={toastType}
          message={toastMessage}
        />
      )}
      {isReturningTo && userToastStatus === STATUS.LOADING && (
        <Loading content="이전페이지로 돌아갑니다." />
      )}
    </Container>
  )
}

Layout.propTypes = {
  children: PropTypes.element.isRequired,
  siteOwner: PropTypes.shape({
    nickname: PropTypes.string,
    avatar: PropTypes.string,
    defaultavatar: PropTypes.string,
    blogname: PropTypes.string
  })
}

Layout.defaultProps = {
  siteOwner: PropTypes.shape({
    nickname: '',
    avatar: '',
    defaultavatar: '',
    blogname: ''
  })
}

export default Layout
