import axios from 'axios'

export const UPLOAD_TYPE = {
  POST: 'POST',
  AVATAR: 'AVATAR'
}

const getImage = (blob, type) => {
  const formData = new FormData()
  formData.append('image', blob)
  axios.defaults.baseURL = process.env.API_URL
  axios.defaults.withCredentials = true
  if (type === UPLOAD_TYPE.POST) {
    return axios.post('/upload/image', formData, {
      headers: { 'Content-type': 'multipart/form-data' }
    })
  }
  if (type === UPLOAD_TYPE.AVATAR) {
    return axios.post('/upload/avatar', formData, {
      headers: { 'Content-type': 'multipart/form-data' }
    })
  }
}

export default getImage
