import { createStore, applyMiddleware, compose } from 'redux'
import { createWrapper } from 'next-redux-wrapper'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import rootReducer from '../reducers/rootReducer'
import rootSaga from '../sagas'

const makeStore = () => {
  const sagaMiddleWare = createSagaMiddleware()
  const middlewares = [sagaMiddleWare]
  const enhancer =
    process.env.NODE_ENV === 'production'
      ? compose(applyMiddleware(...middlewares))
      : composeWithDevTools(applyMiddleware(...middlewares))
  const store = createStore(rootReducer, enhancer)
  store.sagaTask = sagaMiddleWare.run(rootSaga)
  return store
}

const wrapper = createWrapper(makeStore, {
  debug: process.env.NODE_ENV !== 'production'
})

export default wrapper
